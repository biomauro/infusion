import argparse
import re
import sys

if __name__ == "__main__":

    defaultFilter = r'[.HG]+'


    descriptionText = "The script cleans up all not canonical chromosomes from GTF files, using a regular expression."
    
    parser = argparse.ArgumentParser(description = descriptionText,formatter_class=argparse.RawDescriptionHelpFormatter)
    
    parser.add_argument("gtfFile", help="Annotations files in GTF format. stdout if empty.")

    parser.add_argument("-o", action="store", default="", dest="outFile",
        help="Path to output file")

    parser.add_argument("-filter", action="store", default=defaultFilter, dest="filter",
        help="Regular expression to filter out chromosome names.\nDefault: " + defaultFilter)


    args = parser.parse_args()

    gtfFile = open(args.gtfFile)
    filter = args.filter
    outFileName = args.outFile

    outStream = open(outFileName,"w") if len(outFileName) > 0 else sys.stdout

    for line in gtfFile:
        if line.startswith("#") or len(line.strip()) == 0:
            outStream.write(line)
        else:
            items = line.split()
            if not re.search(filter,items[0]):
                outStream.write(line)




