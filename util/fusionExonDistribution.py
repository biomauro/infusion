import argparse
import sys
import numpy as np
import matplotlib.pyplot as pyplot


def parseExonNumber(t_record):
    
    exons = []
    transcripts = t_record.split(";")

    #TODO: what about intron?

    for t in transcripts:
        t_items = t.split(":")
        #print t_items
        exons.append ( int(t_items[1]) )

    return exons
        


MAX_EXON_INDEX = 50


if __name__ == "__main__":

    descriptionText = "The script analyzes the exon distribution of fusions"
    
    parser = argparse.ArgumentParser(description = descriptionText,formatter_class=argparse.RawDescriptionHelpFormatter)
    
    hist_data = []

    parser.add_argument("fusionsFile", help="Detailed output of InFusion tool")

    parser.add_argument("-o", action="store", default="", dest="outFile", help="Path to output file")

    args = parser.parse_args()

    fusionsFile = open(args.fusionsFile)
    
    output = sys.stdout if len(args.outFile) == 0 else open(args.outFile, "w")
    
    num_processed = 0

    for line in fusionsFile:
        if line.startswith("#") or len(line.strip()) == 0:
            continue
        
        items = line.split()
        
        if len(items) < 27:
            continue

        exon_nums_1 = parseExonNumber(items[21])
        hist_data += exon_nums_1       

        exon_nums_2 = parseExonNumber(items[27])
        hist_data += exon_nums_2

        num_processed += 1
    
    print "Processed %d fusions" % num_processed

        
    pyplot.hist( hist_data, MAX_EXON_INDEX )
    pyplot.xlabel("Exon number")
    pyplot.ylabel("Breakpoint count")
    pyplot.title("Fusion breakpoint location in exons (%d events)" % num_processed )


    pyplot.savefig("exon_histogram.svg")

    
