__author__ = 'okonechnikov@mpiib-berlin.mpg.de'

import os
import sys
import run_comparison

dirpath = os.path.dirname(__file__)
sys.path.append( os.path.join(dirpath, "..") )
from fusion import *
import argparse


def filterResultsBasedOnPresence(totalResults, minPresence):

    newResults = []

    for f in totalResults:
        if len(f.tools) >= minPresence:
            newResults.append(f)


    return newResults


def computePresence(totalResults):
    presence = {}

    for f in totalResults:
        key = ":".join(sorted(f.tools))
        if key in presence:
            presence[key] += 1
        else:
            presence[key] = 1

    return presence

def generateRCodeForVenn(sampleNames, presence):
    
    code = "venn.data <- c("
    for key,occur in presence.iteritems():
        item_name = ""
        for name in sampleNames:
            item_name += "1" if name in key else "0"
        code += "\"%s\" = %d," % (item_name,occur)
    code = code[:-1] + ")" 
    code += "\n"
    
    return code

def compareSamples(args, samples, minPresence, drawPlot):

    totalResults = []
    sampleNames = []

    for sample in samples:
        sampleName = sample[0]
        sampleResults = sample[1]

        sampleNames.append( sampleName )

        for f in sampleResults:
            run_comparison.addFusionToPool(f, totalResults, sampleName, create=True)


    totalResults = filterResultsBasedOnPresence(totalResults, minPresence)

    data, fusionNames = run_comparison.createResultTable(totalResults, sampleNames)


    outDir = args.reportDir

    if not outDir:
        outDir = os.path.abspath(args.config).replace(".txt","") + "_results"

    if not os.path.exists(outDir):
        os.mkdir(outDir)


    resultsFile = open(os.path.join(outDir, "common_fusions.txt"), "w")

    writeFilteredResults(totalResults, resultsFile)


    reportPath = os.path.join(outDir, "report.txt")

    report = open(reportPath, "w")

    report.write("DETECTED FUSIONS\n\n")

    report.write("Fusion\t" + "\t".join(sampleNames) + "\n")

    run_comparison.writeResultsTableToFile(report, data, fusionNames)

    presence =  computePresence(totalResults)

    report.write("\nFUSION PRESENCE\n\n")
    for key,occur in presence.iteritems():
        report.write( key + " -> " + str(occur) + "\n")

    if args.rCode:
        if len(sampleNames) <= 4:
            r_code = generateRCodeForVenn(sampleNames, presence)
            report.write("\nVENN DIAGRAM (in R)\n")
            report.write( r_code )
        else:
            sys.stderr.write("Too many samples for Venn diagram. Skipping...\n")

    if drawPlot:
        plotPath = os.path.join(outDir, "comparison_plot.png")
        run_comparison.plotFusionResults(data, sampleNames, fusionNames, plotPath, "png")


def loadSamplesInfo(config):
    samples = []
    for line in open(config):
        items = line.split()
        samples.append( [ items[0], items[1] ] )

    return samples


if __name__ == "__main__":

    descriptionText = "The script compares fusions in different samples detected by the same tool. \
    It outputs how many fusions are common in samples and all common fusions based on threshold."

    parser = argparse.ArgumentParser(description = descriptionText,formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument("config", help="File describing the samples. Each line should contain sample name and path to fusions detected from sample.")
    parser.add_argument("--out", default="", dest="reportDir", help="Directory with detailed report")
    parser.add_argument("--tool", default="infusion", dest="toolId", help="Fusion file format. Default: infusion")
    parser.add_argument("-p", default=1, type=int, dest="minPresence", help="Fusion has to be present in these number of samples")
    parser.add_argument("--no-plot", action="store_true", default=False, dest="noPlot", help="Do not draw the graphics plot [compatiblity issue]")
    parser.add_argument("--r-venn", action="store_true", default=False, dest="rCode", help="Generate R code to create Venn diagram for fusion presence across samples. Only works when number of samples <= 4" )

    args = parser.parse_args()

    results = []

    tool = run_comparison.createTool(args.toolId)

    samples = loadSamplesInfo(args.config)

    if len(samples) < 2:
        sys.stderr.write( "At least 2 samples have to be provided!\n" )
        exit(42)

    for sampleName, sampleFile in samples:
        sys.stderr.write( "Loading %s results\n" % tool.name )
        tool.expectedFusionsOutput = sampleFile
        fusions = tool.getFusionsOutput()
        results.append (  [sampleName, fusions] )

    compareSamples(args, results, args.minPresence, not args.noPlot)
