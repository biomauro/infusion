__author__ = 'okonechnikov@mpiib-berlin.mpg.de'

import re
import subprocess
import time
import os
import sys
import numpy as np
import matplotlib.pyplot as plt
import compare_with_design

dirpath = os.path.dirname(__file__)
sys.path.append( os.path.join(dirpath, "..") )
from fusion import *
import argparse


def parsePerfReport( path):
        
        elapsedTime  = "00:00:00"
        peakMemory = 0

        #print "Parsing perf report for ", self.name
        if not os.path.exists(path):
            #sys.stderr.write("Failed to open performance report %s" % path) 
            return elapsedTime, peakMemory 
        
        for line in open(path):
            if "Elapsed (wall clock)" in line:
                elapsedTime = line.strip().split("m:ss):")[1]
            elif "Maximum resident set" in line:
                peakMemory = int(line.split(":")[1])
        
        return elapsedTime, peakMemory



class FusionTool:
    def __init__(self,toolId, name):
        self.id = toolId
        self.name = name
        self.expectedFusionsOutput = ""
        self.runCmd = ""
        self.skipRun = False
        self.elapsedTime = "00:00:00"
        self.peakMemory = 0

    def loadOptions(self, options):
        self.expectedFusionsOutput = options["fusions_output"]
        if "run_cmd" in options:
            self.runCmd = options["run_cmd"]
        else:
            self.skipRun = True
        if "skip" in options:
            self.skipRun = options["skip"] == '1'

        if "name" in options:
            self.name = options["name"]

        if "perf_report" in options:
            self.elapsedTime, self.peakMemory = parsePerfReport(options["perf_report"])
    
    def run(self):
        self.log = open(self.name + ".run.log", "w")
        self.log.write( "%s:\n%s\n\n" % (time.asctime(), self.runCmd) )
        args = self.runCmd.split()

        print "Running ", self.name

        child_process = subprocess.Popen( args, stdin=subprocess.PIPE,
        stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True,env=os.environ )

        stdout_str, stderr_str = child_process.communicate()

        return_code = child_process.returncode

        self.log.write("[stdout]\n\n")
        self.log.write(stdout_str)

        self.log.write("\n[stderr]\n\n")
        self.log.write(stderr_str)

        self.log.write("\n")
        self.log.write( "%s:\nFinished\n\n" % (time.asctime()) )


        if return_code:
            print stdout_str
            print stderr_str
            assert 0


def fixPrefixedChromosome(chrom):
    if chrom == "chrM":
        return "MT"
    else:
        return chrom.replace("chr","")


class TophatFusion(FusionTool):
    def __init__(self):
        FusionTool.__init__(self, "tophat", "TophatFusion")

    def loadOptions(self, options):
        FusionTool.loadOptions(self,options)
        self.sample_name = options["sample_name"]

        if "tophat_log" in options:
            self.parseTophatLog( options["tophat_log"] )

    def parseTophatLog(self, path):
        if not os.path.exists(path):
            sys.stderr.write("Failed to open tophat log: %s")
            return 
        
        for line in open(path):
            if "Run complete:" in line:
                self.elapsedTime = line.strip().split()[4]
            


    def getFusionsOutput(self):


        results = []
        fusionsFile = open(self.expectedFusionsOutput)
        idx = 1



        for line in fusionsFile:
            f = Fusion(str(idx))
            items = line.split()

            assert len(items) > 3

            if items[0] != self.sample_name:
                continue

            f.genes1 = [items[1]]
            f.i1.ref = fixPrefixedChromosome(items[2])

            f.i1.bpos = int(items[3])

            f.genes2 = [items[4]]
            f.i2.ref = fixPrefixedChromosome(items[5])
            f.i2.bpos = int(items[6])

            #f.ref1 = items[3]
            #f.ref2 = refnames[1]
            #f.pos1 = int(items[1])
            #f.pos2 = int(items[2])

            #assert len(items[3]) == 2

            #f.strand1 = FORWARD_STRAND if items[3][0] == 'f' else REVERSE_STRAND
            #f.strand2 = FORWARD_STRAND if items[3][1] == 'f' else REVERSE_STRAND

            f.numSpans = int(items[7])
            f.numPaired = int(items[8])

            results.append(f)
            idx += 1

        return results

class SoapFuse(FusionTool):
    def __init__(self):
        FusionTool.__init__(self, "soapfuse", "SOAPFuse")

    def loadOptions(self, options):
        FusionTool.loadOptions(self,options)

    def getFusionsOutput(self):


        results = []
        fusionsFile = open(self.expectedFusionsOutput)
        idx = 1

        for line in fusionsFile:
            f = Fusion(str(idx))
            items = line.split()
            
            if line.startswith("up_gene"):
                continue

            assert len(items) >= 12

            f.genes1 = [items[0]]
            f.i1.ref = fixPrefixedChromosome(items[1])

            f.i1.bpos = int(items[3])

            f.genes2 = [items[5]]
            f.i2.ref = fixPrefixedChromosome(items[6])
            f.i2.bpos = int(items[8])

            #f.ref1 = items[3]
            #f.ref2 = refnames[1]
            #f.pos1 = int(items[1])
            #f.pos2 = int(items[2])

            #assert len(items[3]) == 2

            #f.strand1 = FORWARD_STRAND if items[3][0] == 'f' else REVERSE_STRAND
            #f.strand2 = FORWARD_STRAND if items[3][1] == 'f' else REVERSE_STRAND

            f.numSpans = int(items[11])
            f.numPaired = int(items[10])

            results.append(f)
            idx += 1

        return results


class FusionMap(FusionTool):
    def __init__(self):
        FusionTool.__init__(self, "fusionMap", "FusionMap")


    def getFusionsOutput(self):

        results = []
        fusionsFile = open(self.expectedFusionsOutput)

        for line in fusionsFile:
            if line.startswith("FusionID"):
                continue

            items = line.split()

            assert len(items) >= 9
            f = Fusion(items[0])

            f.ref1 = items[5]
            f.pos1 = int(items[6])
            f.ref2 = items[7]
            f.pos2 = int(items[8])

            f.genes1 = [items[9]]
            f.genes2 = [items[13]]

            f.strand1 = FORWARD_STRAND if items[4][0] == '+' else REVERSE_STRAND
            f.strand2 = FORWARD_STRAND if items[4][1] == '+' else REVERSE_STRAND

            f.numSpans = int(items[2]) + int(items[3])
            f.numPaired = 0

            results.append(f)

        return results

class ChimeraScan(FusionTool):
    def __init__(self):
        FusionTool.__init__(self, "chimerascan", "ChimeraScan")


    def getFusionsOutput(self):

        results = []
        fusionsFile = open(self.expectedFusionsOutput)

        for line in fusionsFile:
            if line.startswith("#"):
                continue

            items = line.split()

            f = Fusion(items[6])

            f.i1.ref = fixPrefixedChromosome(items[0])
            start1 = int(items[1]) + 1
            end1 = int(items[2]) + 1
            strand1 = items[8]
            f.i1.bpos = end1 if strand1 == '+' else start1

            f.i2.ref = fixPrefixedChromosome(items[3])
            start2 = int(items[4]) + 1
            end2 = int(items[5]) + 1
            strand2 = items[9]
            f.i2.bpos = start2 if strand2 == '+' else end2

            f.genes1 = items[12].split(",")
            f.genes2 = items[13].split(",")

            f.numSpans = int(items[7])

            results.append(f)

        return results




class Defuse(FusionTool):
    def __init__(self):
        FusionTool.__init__(self, "defuse", "deFuse")


    def getFusionsOutput(self):

        results = []

        fusionsFile = open(self.expectedFusionsOutput)

        for line in fusionsFile:
            if line.startswith("cluster_id"):
                continue

            items = line.split()

            f = Fusion(items[0])

            f.i1.ref = items[24]
            f.i1.bpos = int(items[37])
            f.i2.ref = items[25]
            f.i2.bpos = int(items[38])

            f.genes1 = [ items[30] ]
            f.genes2 = [ items[31] ]

            f.numSpans = int(items[2])
            f.numPaired = int(items[56])

            results.append(f)

        return results

class FusionCatcher(FusionTool):
    def __init__(self):
        FusionTool.__init__(self, "fusioncatcher", "FusionCatcher")

    def getFusionsOutput(self):

        results = []

        break_pairs = set()

        fusionsFile = open(self.expectedFusionsOutput)

        f_id = 1

        for line in fusionsFile:
            if line.startswith("Gene_1_symbol"):
                continue

            items = line.split("\t")

            f = Fusion(f_id)

            f.genes1 = [ items[0] ]
            f.genes2 = [ items[1] ]

            pos1_props = items[8].split(":")
            f.i1.ref = pos1_props[0]
            f.i1.bpos = int(pos1_props[1])

            pos2_props = items[9].split(":")
            f.i2.ref = pos2_props[0]
            f.i2.bpos = int(pos2_props[1])

            break_pair = (f.i1.bpos, f.i2.bpos)

            if break_pair in break_pairs:
                continue
            else:
                break_pairs.add(break_pair)

            f.numSpans = int(items[5])
            f.numPaired = int(items[4])

            results.append(f)

            f_id += 1

        return results


class InFusion(FusionTool):
    def __init__(self):
        FusionTool.__init__(self, "infusion", "InFusion")


    def getFusionsOutput(self):

        def parseLocation(locationStr):
            items = locationStr[1:-1].split(",")
            return  int(items[0]), int(items[1])

        results = []

        fusionsFile = open(self.expectedFusionsOutput)

        for line in fusionsFile:
            if line.startswith("#") or len(line.strip()) == 0:
                continue
            items = line.split()
            assert len(items) >= 8

            f= Fusion(items[0])

            f.i1.ref = items[1]


            f.i1.bpos = int(items[2])

            f.i1.startPos, f.i1.endPos = parseLocation(items[3])

            f.i2.ref = items[4]
            f.i2.bpos = int(items[5])
            f.i2.startPos, f.i2.endPos = parseLocation(items[6])

            f.numSpans = int(items[7])
            f.numPaired = int(items[8])

            f.genes1 = items[9].split(";")
            f.genes2 = items[10].split(";")

            results.append(f)


        return results


def createTool(toolId):
    if toolId == "tophat":
        return TophatFusion()
    elif toolId == "infusion":
        return InFusion()
    elif toolId == "fusionMap":
        return FusionMap()
    elif toolId == "defuse":
        return Defuse()
    elif toolId == "chimerascan":
        return ChimeraScan()
    elif toolId == "soapfuse":
        return SoapFuse()
    elif toolId == "fusioncatcher":
        return FusionCatcher()
    else:
        assert 0
        return None


def parseConfigurationFile(filename):

    toolSet = []

    cfgFile = open(filename)
    dirpath = os.path.dirname(filename)

    toolOptions = {}
    common = {}

    blockName = ""

    for line in cfgFile:
        if len(line.strip()) == 0 or line.startswith("#"):
            continue
        elif line.startswith('</'):
            if len(toolOptions) > 0 and len(blockName) > 0:
                #sys.stderr.write( "Parsed configuration block: %s\n" % blockName )
                #sys.stderr.write( "%s\n" % str(toolOptions) )
                if blockName == "common":
                    common = toolOptions
                else:
                    tool = createTool(blockName)
                    path = toolOptions["fusions_output"]
                    if not path.startswith("/"):
                        #path is relative to config dir
                        path = os.path.join(dirpath,path)
                        toolOptions["fusions_output"] = path

                    tool.loadOptions(toolOptions)
                    toolSet.append(tool)
        elif line.startswith("<"):
            toolOptions.clear()
            blockName = re.findall(r'<([\s\w-]+)>', line)[0]
        else:
            items = line.strip().split('=')
            if len(items) == 2:
                toolOptions[items[0]] = items[1]
            else:
                sys.stderr.write( "Bad config line: %s\n" % line )

    return toolSet,common

def breakpointsMatch(f1, f2, threshold):

    dist1 =  abs(f1.i1.bpos - f2.i1.bpos)
    dist2 = abs(f1.i2.bpos - f2.i2.bpos)
    return ( f1.i1.ref == f2.i1.ref and f1.i2.ref == f2.i2.ref and
            dist1 <= threshold and
            dist2 <= threshold )

def breakpointsCrossMatch(f1, f2, threshold):
    return (f1.i1.ref == f2.i2.ref and f1.i2.ref == f2.i1.ref and
            abs(f1.i1.bpos - f2.i2.bpos) <= threshold and
            abs(f1.i2.bpos - f2.i1.bpos) <= threshold )

def hasCoordinates(f):
    return f.i1.bpos != -1 and f.i2.bpos != -1

def intersect(geneList1, geneList2):
    return len(set(geneList1) & set(geneList2)) > 0


def genesMatch(f1,f2):
    return  ( intersect(f1.genes1, f2.genes1) and intersect(f1.genes2, f2.genes2) ) or\
            ( intersect(f1.genes1, f2.genes2) and intersect(f1.genes2, f2.genes1) )

def genesMatchInRightDirection(f1, f2):
    return intersect(f1.genes1, f2.genes1) and intersect(f1.genes2, f2.genes2)

def fusionsMatch(f1, f2):
    if hasCoordinates(f1) and hasCoordinates(f2):
        return breakpointsMatch(f1, f2, 20) or breakpointsCrossMatch(f1, f2, 20)
    else:
        return genesMatch(f1,f2)

#TODO: set max position to detect fusion as an option

def compareFusions(f1, f2, dist_data):
    if hasCoordinates(f1) and hasCoordinates(f2):
        if breakpointsMatch(f1, f2, 20):
            dist_data.append( f1.i1.bpos - f2.i1.bpos )
            dist_data.append( f1.i2.bpos - f2.i2.bpos )
            return True
        elif breakpointsCrossMatch(f1, f2, 20):
            dist_data.append( f1.i1.bpos - f2.i2.bpos  )
            dist_data.append( f1.i2.bpos - f2.i1.bpos  )
            return True
        else:
            return False
    else:
        return genesMatch(f1,f2)


def addFusionToPool(newFusion, pool, toolName, create):
    bp_pos_precision = []
    for fusion in pool:
        #if breakpointsMatch(newFusion, fusion, 10) or breakpointsCrossMatch(newFusion, fusion, 10):
        if compareFusions(newFusion, fusion, bp_pos_precision):
            fusion.tools.add(toolName)
            fusion.scores[toolName] = newFusion.numSpans + newFusion.numPaired
            fusion.bp_precision[toolName] = bp_pos_precision

            #if bp_pos_precision[0] > 0 or bp_pos_precision[1] > 0:
            #    print "The fusion %s bp position discovered by %s has a bias" % (fusion.name, toolName )

            fusion.struct_correct[toolName] = genesMatchInRightDirection(newFusion, fusion)
            return True

    if create:
        newFusion.tools = set([toolName])
        newFusion.scores[toolName] = newFusion.numSpans + newFusion.numPaired
        pool.append(newFusion)
        return True

    return False

def loadExpectedFusions(expectedResultsPath):

    fusions = []

    if expectedResultsPath:
        fusions = compare_with_design.parseDesignedList(expectedResultsPath)
        for f in fusions:
            f.tools = set()

    return fusions

class FusionToolCounts:
    def __init__(self, toolNames):
        self.counts = {}
        for toolName in toolNames:
            self.counts[toolName] = 0


    def incCount(self, toolName):
        self.counts[toolName] += 1


def createResultTable(fusions, toolNames):

    data = np.zeros(shape=(len(toolNames), len(fusions)))

    fusionNames = []

    fusionCount = 0
    validCounts = FusionToolCounts(toolNames)
    for f in fusions:
        if f.name:
            fusionNames.append(f.name)
        else:
            fusionNames.append( "%s-%s" % (f.genes1[0], f.genes2[0]) )
        toolCount = 0
        for toolName in toolNames:
            if toolName in f.tools:
                validCounts.incCount(toolName)
                data[toolCount, fusionCount] = 1
            else:
                data[toolCount, fusionCount] = 0
            toolCount += 1
        fusionCount += 1

    return data,fusionNames

def detectCommonFusions(fusions):

    commonFusions = []

    for f in fusions:
        if len(f.tools) > 1:
            commonFusions.append(f)

    return commonFusions




def plotFusionResults(plotData, toolNames, fusionNames, plotPath, imageFormat ):

    fig, ax = plt.subplots()
    ax.pcolormesh( plotData, cmap=plt.cm.Blues, alpha=0.8  )

    # want a more natural, table-like display
    ax.xaxis.set_tick_params(size=0)
    ax.invert_yaxis()
    ax.xaxis.tick_top()

    # put the major ticks at the middle of each cell
    ax.set_xticks(np.arange(plotData.shape[1])+0.5, minor=False)
    ax.set_yticks(np.arange(plotData.shape[0])+0.5, minor=False)

    #ax.get_xticks().set_visible(False)

    ax.grid(False)

    if len(fusionNames) < 60:

        bold_labels = [] # already know
        italic_labels = [] # in both cell lines

        for i in range(len(fusionNames)):
            fName =  fusionNames[i]
            if "*" in fName:
                bold_labels.append(i)
                fusionNames[i] = fName.replace("*","")
            if "'" in fName:
                italic_labels.append(i)
                fusionNames[i] = fName.replace("'","")


            fusionNames[i] = fusionNames[i].replace("_"," ")

        plt.xticks(rotation=90)
        plt.subplots_adjust(top=0.5)
        ax.set_xticklabels(fusionNames, minor=False)

        labels = ax.xaxis.get_ticklabels()
        for idx in bold_labels:
            labels[idx].set_weight('bold')

        for idx in italic_labels:
            labels[idx].set_style('italic')

    else:
        ax.set_xticklabels([], minor=False)

    ax.set_yticklabels(toolNames, minor=False)

    #plt.show()
    fig.set_size_inches(20,5)
    #print plotData.shape
    plt.savefig(plotPath, format=imageFormat)


def writeResultsTableToFile(reportFile, data, fusionNames):
    for i in range(len(fusionNames)):
        scores = [fusionNames[i]]
        for j in range(data.shape[0]):
            scores.append(str(data[j,i]))
        reportFile.write( "\t".join(scores) + "\n")


def selectExpectedFusions(pathToExpected, toolName, toolResults):

    results = []

    expectedFusions = loadExpectedFusions(pathToExpected)

    for f in toolResults:
        if addFusionToPool(f, expectedFusions, toolName, create=False):
            results.append(f)


    return results


def createComparisonSummary(pathToExpected, results):

    otherResults = []

    expectedFusions = loadExpectedFusions(pathToExpected)

    toolNames = []

    for result in results:
        toolName = result[0]
        toolResults = result[1]

        toolNames.append( toolName )

        for f in toolResults:
            if not addFusionToPool(f, expectedFusions, toolName, create=False):
                addFusionToPool(f, otherResults, toolName, create=True)


    summary = {}

    for toolName in toolNames:
        summary[toolName] = [0,0, len(expectedFusions)]

    for toolName in toolNames:
        for f in expectedFusions:
            if toolName in f.tools:
                summary[toolName][0] += 1

        for f in otherResults:
            if toolName in f.tools:
                summary[toolName][1] += 1

    return summary,expectedFusions


def writeToolStats(report, toolNames, expectedFusions, otherResults):

    # vector [TOTAL_DETECTED, TRUE_POSITIVE, FALSE_NEGATIVE, CORRECT_STRUCTURE]
    tool_stats = {}

    for name in toolNames:
        tool_stats[name] = [0,0,0,0]

    for f in expectedFusions:
        for name in toolNames:
            if name in f.tools:
                tool_stats[name][0] += 1
                tool_stats[name][1] += 1
                tool_stats[name][3] += 1 if f.struct_correct[name] else 0
            else:
                tool_stats[name][2] += 1

    for f in otherResults:
        for name in f.tools:
            tool_stats[name][0] += 1

    report.write("\nRESULTS TABLE\n")
    report.write("tool\tTrue\tTotal\tPrecision\tRecall\tF-measure\tCorrect structure\n")

    for toolName in tool_stats:
        stats = tool_stats[toolName] 
        sp = stats[1] / float( stats[0]) if stats[0] > 0 else 0
        sn = stats[1] / float( len(expectedFusions) )
        fmeasure =  ( 2* sp*sn ) / (sp + sn)  if (sp + sn) > 0 else 0
        correct_struct = stats[3] / float(stats[0]) if stats[0] > 0 else 0


        report.write("%s\t%d/%d\t%d\t%f\t%f\t%f\t%.2f\n" %
                     (toolName, stats[1], len(expectedFusions), stats[0], sp, sn, fmeasure, correct_struct))


    # write LATEX
    report.write("\nLATEX RESULTS\n")

    report.write("\\begin{tabular} {|c|c|c|}\n")
    report.write("\hline\n")
    report.write("Method & True & Total & Precision & Recall \\\\\n")
    report.write("\hline\n")
    for toolName in tool_stats:
        stats = tool_stats[toolName]
        sp = stats[1] / float( stats[0]) if stats[0] > 0 else 0
        sn = stats[1] / float( len(expectedFusions) )
        report.write("%s & %d & %d & %.2f & %.2f \\\\\n" %  (toolName, stats[1], stats[0], sp, sn))
        report.write("\hline\n")
    report.write("\\end{tabular}\n")


def plot2Datasets(tool_datasets, x_idx, idxs, x_label, titles, path):

    fig = plt.figure(figsize=(12, 5), dpi=100,)
    fig.subplots_adjust(hspace=1.5)

    #fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(12, 5), dpi=100)
    #fig.tight_layout()


    axes = []
    axes.append( plt.subplot(121) )
    axes.append(  plt.subplot(122) )

    fig.tight_layout(pad=3.5)

    colors = getColorsList()

    for i in range(len(idxs)):
        y_idx = idxs[i]
        ax = axes[i]
        index = 0
        for tool_name in tool_datasets:
            x = []
            y = []

            #print "Drawing for", tool_name
            dataset = tool_datasets[tool_name]
            for d in dataset:
                x_val = d[x_idx]
                y_val = d[y_idx]
                #if normalize:
                #    sum = x_val + y_val
                #    x_val = x_val / float(sum) if sum > 0 else 0
                #    y_val  = y_val / float(sum) if sum > 0 else 0

                x.append( x_val )
                y.append( y_val )

            ax.plot(x, y, 'k', label=tool_name, color = colors[index])
            index += 1

        # Now add the legend with some customizations.
        legend = ax.legend(loc='lower right', shadow=True)

        # The frame is matplotlib.patches.Rectangle instance surrounding the legend.
        frame  = legend.get_frame()
        frame.set_facecolor('0.90')

        # Set the fontsize
        for label in legend.get_texts():
            label.set_fontsize('large')

        for label in legend.get_lines():
            label.set_linewidth(1.5)  # the legend line width

        ax.set_xlabel(x_label)
        #plt.ylabel(labels[i])
        ax.set_title(titles[i])
        ax.grid()

    plt.savefig(path)




def plotToolData(tool_datasets, x_idx, y_idx, x_label, y_label, title, snPath, normalize = False):

    fig, ax = plt.subplots()

    colors = getColorsList()

    index = 0

    for tool_name in tool_datasets:
        x = []
        y = []

        #print "Drawing for", tool_name
        dataset = tool_datasets[tool_name]
        for d in dataset:
            x_val = d[x_idx]
            y_val = d[y_idx]
            if normalize:
                sum = x_val + y_val
                x_val = x_val / float(sum) if sum > 0 else 0
                y_val  = y_val / float(sum) if sum > 0 else 0

            x.append( x_val )
            y.append( y_val )
        ax.plot(x, y, 'k', label=tool_name, color=colors[index] )
        index += 1

    # Now add the legend with some customizations.
    legend = ax.legend(loc='lower right', shadow=True)

    # The frame is matplotlib.patches.Rectangle instance surrounding the legend.
    frame  = legend.get_frame()
    frame.set_facecolor('0.90')

    # Set the fontsize
    for label in legend.get_texts():
        label.set_fontsize('large')

    for label in legend.get_lines():
        label.set_linewidth(1.5)  # the legend line width

    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.title(title)

    plt.savefig(snPath)


def getFusionsBelowThreshold(threshold, expectedFusions):

    fusions = []

    for f in expectedFusions:
        if f.numSupportersByDesign <= threshold:
            fusions.append(f)

    return fusions


def drawGraphs(expectedFusions, otherResults, toolNames, outDir, numSimulatedAvailable):

    max_num_supporters = 160 # Has to be dynamic?

    tool_datasets = {}
    for name in toolNames:
        tool_datasets[name] = []

    graphDataPath  = os.path.join(outDir, "plot_data.txt")
    graphDataFile  = open(graphDataPath, "w")

    print toolNames

    for score_threshold in range(3,max_num_supporters+1):

        expectedFusionsBelowThreshold = expectedFusions

        if numSimulatedAvailable:
            expectedFusionsBelowThreshold = getFusionsBelowThreshold(score_threshold, expectedFusions)

        num_simulated = len(expectedFusionsBelowThreshold)
        if num_simulated == 0:
            continue

        if score_threshold == max_num_supporters:
            #assert num_simulated == len(expectedFusions)
            print "Num supporters: ",num_simulated, ", num expected: ", len(expectedFusions)

        for tool in toolNames:

            tp = 0
            fp = 0
            total = 0

            for f in expectedFusionsBelowThreshold:

                score = f.scores[tool] if tool in f.scores else 0
                passThreshold = score <= score_threshold

                if passThreshold and tool in f.tools:
                    tp += 1
                    total += 1

            for f in otherResults:
                score = f.scores[tool] if tool in f.scores else 0
                passThreshold = score <= score_threshold

                if tool in f.tools and passThreshold:
                    fp += 1
                    total += 1

            recall = tp / float(num_simulated)
            precision = tp / float(total) if total > 0 else 0
            fmeasure = ( 2* recall*precision ) / (recall + precision)  if (recall + precision) > 0 else 0

            tool_datasets[tool].append( [score_threshold, tp, fp, total, recall, precision, fmeasure] )


    for tool_name, dataset in tool_datasets.iteritems():
        graphDataFile.write(">%s\n" % tool_name)
        for (score, tp, fp, total, recall, precision, fmeasure)  in dataset:
            graphDataFile.write( "%d\t%d\t%d\t%d\t%f\t%f\t%f\n" % (score, tp, fp, total, recall, precision, fmeasure) )



    snPath = os.path.join(outDir, "recall.png")
    plotToolData(tool_datasets, 0, 4, "Number of supporting reads", "Recall","Recall comparison", snPath)

    fdrPath = os.path.join(outDir, "precision.png")
    plotToolData(tool_datasets, 0, 5,  "Number of supporting reads", "Precision", "Precision comparison", fdrPath)

    bothPlotsPath = os.path.join(outDir, "recall_and_precision.png")
    plot2Datasets(tool_datasets, 0, [4,5],  "Number of supporting reads", ["Recall", "Precision"], bothPlotsPath)


    pvPath = os.path.join(outDir, "pv.png")
    plotToolData(tool_datasets, 4, 5,  "Recall", "Precision", "PV-curve", pvPath)

    fmPath = os.path.join(outDir, "fmeasure.png")
    plotToolData(tool_datasets, 0, 6,  "Number of supporting reads", "F-measure", "F-measure comparison", fmPath)

    #tprPath = os.path.join(outDir, "tpr.png")
    #plotToolData(tool_datasets, 1, 2,  "False positive rate", "True positive rate", "TPR comparison", tprPath, True)


def addNumSupportersByDesign(expectedFusions, numSupportersPath):

    for f in expectedFusions:
        f.numSupportersDesigned = 0

    numFusionSupporters = 0

    for line in open(numSupportersPath):
        if line.startswith("#") or len(line.strip()) == 0:
            continue
        items = line.split()
        fusion_name = items[0]
        num_supporters = int(items[-1])
        for f in expectedFusions:
            if f.name == fusion_name:
                f.numSupportersByDesign = num_supporters
                numFusionSupporters += 1
                break

    if numFusionSupporters != len(expectedFusions):
        sys.stderr.write("ERROR! Number of fusions in supporters file %s doesn't "
                         "pass total number of expected fusions.\nPlease check this file carefully." % (numSupportersPath) )
        sys.exit(-1)



def saveBpPrecision(expectedFusions, toolNames, outDir, imageFormat):

    outReportPath = os.path.join(outDir, "bp_precision.txt")

    outFile = open(outReportPath, "w")

    datasets = []


    # why only common?
    bp_precision = {}

    numCommonBps = 0

    for f in expectedFusions:
        if len(f.bp_precision) == len(toolNames):
            numCommonBps += 1

        for tool_name, bpp in f.bp_precision.iteritems():
            bpp_data = bp_precision.get(tool_name, [])
            bpp_data.append(abs(bpp[0])) # distance between valid and reported position for bp1
            bpp_data.append(abs(bpp[1])) # distance between valid and reported position for bp2
            bp_precision[tool_name ] = bpp_data

    #print "Number of common fusions detected by all tools: ", numCommonBps / 2


    for tool_name in toolNames:
        bpp_data = bp_precision[tool_name]
        hist = {}
        datasets.append( bpp_data )
        correct_dist = 0
        for dist in bpp_data:
            count = hist.get(dist, 0)
            hist[dist] = count + 1
            if dist == 0:
                correct_dist += 1
            #data_str.append( str(dist))
        mean_correct_dist = float(correct_dist) / len(bpp_data)
        outFile.write(">%s %d %f %f %f\n"  %
                      (tool_name, len(bpp_data) / 2, mean_correct_dist, np.mean(bpp_data), np.std(bpp_data)) )
        for val, count in hist.iteritems():
            outFile.write("%d:%d\n" % (val,count) )

    bins = []
    for i in range(-11,11,1):
        bins.append( i + 0.5)



    bins = range(0,20)

    #bins = [-5.5,-4.5,-3.5,-2.5,-1.5,-0.5,0.5,1.5,2.5,3.5,4.5,5.5]
    #colors = getColorsList()

    #print bins

    #print toolNames
    #print hist_data
    for toolName in toolNames:
        fig, ax = plt.subplots()

        bpp_data = bp_precision[toolName]

        n, bins, patches = ax.hist(bpp_data, bins=bins,  normed=True, histtype='bar', fill=True, rwidth=0.8,
                                       label=toolName)

        legend = ax.legend(loc='upper left', shadow=True)

        plt.title("Breakpoint precision ( N=%d, mean=%.1f, median=%.1f) " % (len(bpp_data) / 2, np.mean(bpp_data), np.median(bpp_data)) )
        plt.xlabel("Breakpoint deviation")

        plt.savefig(os.path.join(outDir, "bp_precision_%s.svg" % toolName),format=imageFormat)



def compareResults(args, results, toolSet, warnings):

    expectedFusions = []
    commonFusions = []
    otherResults = []


    fusionsBlock = []
    if len(args.fusionsBlock) > 0:
        items = args.fusionsBlock.split(":")
        assert(len(items) == 2)
        fusionsBlock = [ int(items[0]), int(items[1])]

    if len(args.expectedFusions) > 0:
        expectedFusions = loadExpectedFusions(args.expectedFusions)
    else:
        print "No validated known fusions are provided."


    if args.designedSupportersPath:
        addNumSupportersByDesign(expectedFusions, args.designedSupportersPath)

    toolNames = []
    bpPrecision = {}

    for result in results:
        toolName = result[0]
        toolResults = result[1]

        toolNames.append( toolName )

        toolBpPrecision = []

        for f in toolResults:
            foundFusion = False
            if len(expectedFusions) > 0:
                foundFusion  =  addFusionToPool(f, expectedFusions, toolName, create=False)

            if not foundFusion:
                addFusionToPool(f, otherResults, toolName, create=True)

        bpPrecision[toolName] = toolBpPrecision


    if len(fusionsBlock) > 0:
        expectedFusions = expectedFusions[fusionsBlock[0] : fusionsBlock[1]]

    validData = []
    validFusionNames = []

    commonData = []
    commonFusionNames = []

    if len(expectedFusions) > 0:
        validData, validFusionNames = createResultTable(expectedFusions, toolNames)
    else:
        commonFusions = detectCommonFusions(otherResults)
        commonData, commonFusionNames = createResultTable(commonFusions, toolNames)

    data, fusionNames = createResultTable(otherResults, toolNames)

    outDir = args.reportDir

    if not outDir:
        outDir = os.path.abspath(args.config).replace(".cfg","") + "_results"

    if not os.path.exists(outDir):
        os.mkdir(outDir)

    if args.drawROC:
        drawGraphs(expectedFusions, otherResults, toolNames, outDir, len(args.designedSupportersPath) > 0)

    reportPath = os.path.join(outDir, "report.txt")

    report = open(reportPath, "w")

    if len(expectedFusions) > 0:
        report.write("DETECTED VALID FUSIONS\n")
        report.write("Fusion\t" + "\t".join(toolNames) + "\n")
        writeResultsTableToFile(report, validData, validFusionNames)
        report.write("\n")

    if len(expectedFusions) > 0:
        report.write("OTHER DETECTED FUSIONS\n")
    else:
        report.write("DETECTED FUSIONS\n")


    report.write("Fusion\t" + "\t".join(toolNames) + "\n")

    if  len(fusionNames) > 0:
        writeResultsTableToFile(report, data, fusionNames)

    if len(commonFusions) > 0:
        report.write("\nCOMMON DETECTED FUSIONS\n")
        report.write("Fusion\t" + "\t".join(toolNames) + "\n")
        writeResultsTableToFile(report, commonData, commonFusionNames)

    if len(warnings) > 0:
        report.write("\nWARNINGS\n")
        for line in warnings:
            report.write(line)

    if len(expectedFusions) > 0:
        writeToolStats(report, toolNames, expectedFusions, otherResults)

    performanceReport = ""
    for tool in toolSet:
        if tool.peakMemory > 0:
            performanceReport += "%s\t%s\t%s\n" % ( tool.name, tool.elapsedTime, tool.peakMemory )

    if performanceReport:
        print "Writing performance report"
        report.write("\nPERFORMANCE\n")
        report.write("Tool name\tTime(h:mm:ss)\tPeak memory (Mb)\n")
        report.write(performanceReport)

    if len(expectedFusions) > 0:
        print "Creating a plot including detected validated fusions"
        validatedPlotPath = os.path.join(outDir, "validated_plot.%s" % args.imageFormat)
        plotFusionResults(validData,toolNames,validFusionNames, validatedPlotPath, args.imageFormat)

    if len(fusionNames) > 0:
        print "Creating a plot including all detected fusions"
        otherFusionsPlotPath = os.path.join(outDir, "results_plot.%s" % args.imageFormat)
        plotFusionResults(data,toolNames,fusionNames, otherFusionsPlotPath, args.imageFormat)

    if len(commonFusions) > 0:
        print "Creating a plot including common detected fusions"
        commonFusionsPlotPath = os.path.join(outDir, "common_plot.%s" % args.imageFormat)
        plotFusionResults(commonData,toolNames, commonFusionNames, commonFusionsPlotPath, args.imageFormat)

    if args.analyzeBpPrecision:
        print "Computing fusion breakpoint precision"
        saveBpPrecision(expectedFusions, toolNames, outDir, args.imageFormat)




class ComparisonResult:
    def __init__(self,toolName):
        self.toolName = toolName
        self.numTP = 0
        self.numFP = 0
        self.numFN = 0
        self.fusionScores = []
        self.falsePostives = []

class FusionScore:
    def __init__(self, gene1, gene2):
        self.name = "%s-%s" % (gene1,gene2)
        self.count = 0
        self.val = 0

def plotComparisonValues(plotPath, results, fusionNames):


    # calc num Fusions

    numFusions = len(fusionNames)

    plotData = np.zeros(shape=(len(results), numFusions))

    i = 0

    for toolName in results:
        toolData = results[toolName]
        for j in range(numFusions):
            plotData[i][j] = 0 if toolData[j] == '-' else 1
        i += 1

    #print X

    fig, ax = plt.subplots()
    heatmap = ax.pcolor( plotData, cmap=plt.cm.Blues, alpha=0.8, edgecolors='k'  )

    ax.invert_yaxis()
    ax.xaxis.tick_top()

    # put the major ticks at the middle of each cell
    ax.set_xticks(np.arange(plotData.shape[1])+0.5, minor=False)
    ax.set_yticks(np.arange(plotData.shape[0])+0.5, minor=False)

    #ax.get_xticks().set_visible(False)

    # want a more natural, table-like display
    #ax.invert_yaxis()
    ax.xaxis.set_tick_params(size=0)

    ax.grid(False)
    plt.xticks(rotation=90)
    plt.subplots_adjust(top=0.5)

    ax.set_xticklabels(fusionNames, minor=False)
    k = 0
    for label in ax.xaxis.get_ticklabels():
        if k % 2 == 0:
        # label is a Text instance
            label.set_color('red')
            label.set_rotation(45)
            label.set_fontsize(16)
        k += 1

    ax.set_yticklabels(results.keys(), minor=False)

    #plt.show()
    fig.set_size_inches(20,5)
    plt.savefig(plotPath)






def compareResultsWithDesign(args, predictedFusions, designedFusions):

    compRes = []

    bitsets = {}
    reals = [None]*len(designedFusions)

    for fusionSet in predictedFusions:
        r = ComparisonResult(fusionSet[0])
        totalPositiveCount = 0

        bitsets[r.toolName] = ['-'] * len(designedFusions)
        idx = 0

        for df in designedFusions:
            score = FusionScore(df.genes1, df.genes2)

            reals[idx] = score.name

            for pf in fusionSet[1]:
                if (df.genes1 in pf.genes1 and df.genes2 in pf.genes2) or \
                        (df.genes1 in pf.genes2 and df.genes2 in pf.genes1):
                    score.count += 1
                    score.val += pf.numSpans + pf.numPaired
                    pf.detected = True
            if score.count > 0:
                r.numTP += 1
                totalPositiveCount += score.count
                bitsets[r.toolName][idx] = 'X'

            r.fusionScores.append(score)
            idx += 1

        r.numFN = bitsets[r.toolName].count('-')

        for pf in fusionSet[1]:
            if not hasattr(pf, "detected"):
                r.numFP += 1
                r.falsePostives.append( "%s-%s" % (pf.genes1, pf.genes2 ) )

        compRes.append( r )

    outDir = args.reportDir

    if not outDir:
        outDir = os.path.abspath(args.config).replace(".cfg","") + "_results"

    if not os.path.exists(outDir):
        os.mkdir(outDir)

    reportFile = os.path.join(outDir, "report.txt")
    tableFile = os.path.join(outDir, "results_table.txt")

    report = open(reportFile, "w")
    results = open(tableFile, "w")

    report.write("Reals:\n")

    for i in range(0, len(reals)):
        report.write("%d %s\n" % (i + 1, reals[i]) )

    report.write("\n")

    OFFSET_SIZE = 12
    for toolName in bitsets:
        spacer = " "*( OFFSET_SIZE - len(toolName) )
        report.write("%s%s" % (toolName,spacer) )
        for c in bitsets[toolName]:
            report.write(c)
        report.write("\n")

    report.write("\n")

    results.write( "#Tool name\tTP\tFN\tFP\tPPR\tAC\n" )
    for r in compRes:
        ppr =  float(r.numTP)/(r.numFP + r.numTP)
        acc = float(r.numTP)/(len(designedFusions))
        results.write( "%s\t%d\t%d\t%d\t%f\t%f\n" % (r.toolName, r.numTP, r.numFN, r.numFP, ppr, acc ) )
        report.write("[%s]\n" % r.toolName)
        report.write("\n[Detected fusions]\n")
        for f in r.fusionScores:
            report.write("%s\n" % f.name)
        report.write("\n[False positives]\n")
        for f in r.falsePostives:
            report.write("%s\n" % f)
        report.write("\n")

    plotPath = os.path.join(outDir, "plot.png")

    fusionNames = []

    for df in designedFusions:
        score = FusionScore(df.genes1, df.genes2)
        fusionNames.append(score.name)

    plotComparisonValues(plotPath, bitsets, fusionNames )

if __name__ == "__main__":

    descriptionText = "The script compares results of tools for fusion detection"

    parser = argparse.ArgumentParser(description = descriptionText,formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument("config", help="Configuration file")
    parser.add_argument("--skip-run",action="store_true", default=False, dest="skipRunStep", help="Force skipping running tools")
    parser.add_argument("--threshold",action="store", type=int, default=2, dest="threshold", help="Threshold distance for breakpoint position")
    parser.add_argument("--out", default="", dest="reportDir", help="Directory with detailed report")
    parser.add_argument("-e", default="", dest="expectedFusions", help="Fusions expected in the experiment (validated list)")
    parser.add_argument("--segment", default="", dest="fusionsBlock", help="Check only validation of a segment of valid fusions. Example: 1,20 (first 20) ")
    parser.add_argument("--roc", action="store_true", default=False, dest="drawROC", help="Draw ROC-curve based on total fusion score")
    parser.add_argument("--bp-precision", action="store_true", default=False, dest="analyzeBpPrecision", help="Analyze breakpoint precision")
    parser.add_argument("--format", action="store", default="svg", dest="imageFormat", help="Output image format, default is SVG")

    args = parser.parse_args()
    args.designFile = ""

    toolSet,common = parseConfigurationFile(args.config)

    results = []

    warnings = []

    for tool in toolSet:
        if not (args.skipRunStep or tool.skipRun):
            tool.run()

        if os.path.exists(tool.expectedFusionsOutput):
            sys.stderr.write( "Loading %s results\n" % tool.name )
            fusions = tool.getFusionsOutput()
            results.append (  [tool.name, fusions ] )
        else:
            warn = "No fusions detected by %s\n" % tool.name
            sys.stderr.write("WARNING! " + warn )
            warnings.append(warn)
            results.append ( [tool.name, [] ] )

    if not args.expectedFusions:
        args.expectedFusions = common.get("validated", "")

    if  not args.fusionsBlock:
        args.fusionsBlock = common.get("segment", "")


    if args.expectedFusions and not args.expectedFusions.startswith("/"):
        args.expectedFusions = os.path.join(os.path.dirname(args.config), args.expectedFusions)

    args.designedSupportersPath = common.get("designed_supporters", "")
    if args.designedSupportersPath:
        if not args.designedSupportersPath.startswith("/"):
            args.designedSupportersPath = os.path.join(os.path.dirname(args.config), args.designedSupportersPath)

    compareResults(args, results, toolSet, warnings)

