import random
import sys
import argparse
from Bio import SeqIO
from Bio.Seq import Seq
import HTSeq


def idsContainGiven(givenId, transcriptIds):
    for tId in transcriptIds:
        if givenId.find(tId) != -1:
            return True

    return False

if __name__ == "__main__":
    
    descriptionText = "The script extracts a random subset of transcript from a GTF file."

    parser = argparse.ArgumentParser(description = descriptionText,formatter_class=argparse.RawDescriptionHelpFormatter)
    
    parser.add_argument("-i", action="store", required="true", dest="inputGtf",
        help="Input GTF file")

    parser.add_argument("-o", action="store", default="-", dest="outputGtf",
                        help="OutputGtf file")


    parser.add_argument("--attr-id", action="store", dest="attrId", default="transcript_id",
                        help="Attribute to select the transcript. Default is \"transcript_id\"")

    parser.add_argument("-s", action="store", type=int, default=42, dest="seed", help="Random seed")

    parser.add_argument("-n", action="store", required="true", type=int, dest="sampleSize", help="Subset size")

    args = parser.parse_args()
    
    print args
    

    # parse GTF file

    gtf_file = HTSeq.GFF_Reader( args.inputGtf )

    transcripts = {}

    for feature in gtf_file:
        if feature.type == 'exon' or feature.type == 'CDS':
            transcriptName = feature.attr[ args.attrId ]
            tr_features = transcripts.get(transcriptName, [] )
            tr_features.append(feature)
            transcripts[transcriptName] = tr_features


    sys.stderr.write(  "Loaded %d transcripts\n" % len(transcripts) )

    seq = range(0, len(transcripts))
    random.seed(args.seed)
    sample = sorted(random.sample(seq, args.sampleSize))

    #print sample

    k = 0

    out_file = sys.stdout if args.outputGtf == "-" else open(args.outputGtf, "w")

    for t_name in transcripts:
        if k in sample:
            features = transcripts[t_name]
            for feature in features:
                out_file.write( feature.get_gff_line() )

        k += 1


