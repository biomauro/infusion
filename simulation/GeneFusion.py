#TODO: is this outdate stuff and has to be removed?

import re

def parseFusion(line):

    pair = line.split(";")
    #print pair[0]
    lf =  re.findall(r'([\.\-\w\d]+)\((\d+),(\d+)\)', pair[0])[0]
    #print lf
    lf_exons = ( lf[0], int(lf[1]),int(lf[2]))
    rf =  re.findall(r'([\.\-\w\d]+)\((\d+),(\d+)\)', pair[1])[0]
    #print rf
    rf_exons = ( rf[0], int(rf[1]), int(rf[2]) )

    fusion = ( lf_exons, rf_exons)

    return fusion

def writeAnnotationWithFusions(fusions, fusionGeneNames, geneFileName, subset, outFile):


    geneFile = open(geneFileName)
    fusionGenes = {}
    output = open(outFile, "w")

    for line in geneFile:
        items = line.split("\t")
        geneName = items[3]

        if geneName in fusionGeneNames:
            #print "Saving %s ..." % geneName
            fusionGenes[geneName] = (items[0], int(items[1]), items[10], items[11], items[5])
        else:
            #print "Skipping %s ..." % geneName
            if geneName in subset:
                output.write(line)

    count = 0
    for pair in fusions:

        count+=1
        fusionName=  "FUSION%03d" % count
        print "Creating fusion ", fusionName

        for fusion in pair:
            fusionGene = fusionGenes[fusion[0]]
            coords = fusionGene[3].split(",")[ fusion[1] - 1 : fusion[2]  ]
            #print coords
            strand = fusionGene[4]
            sizes = fusionGene[2].split(",")[ fusion[1] - 1 : fusion[2]  ]

            fusionStart = fusionGene[1]
            fusionEnd = fusionStart + int(coords[-1]) + int(sizes[-1])

            fusionSizes = ",".join( [str(s) for s in sizes] ) + ","
            fusionCoords = ",".join( [str(c) for c in  coords]) + ","
            output.write("%s\t%d\t%d\t%s\t0\t%s\t0\t0\t0\t%d\t%s\t%s\n" %
                         (fusionGene[0], fusionStart, fusionEnd, fusionName, strand, len(coords),
                          fusionSizes, fusionCoords) )


