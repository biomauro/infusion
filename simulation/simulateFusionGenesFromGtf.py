import sys
import HTSeq
import argparse
import random
import simlib

BREAK_INSIDE_EXON = "break_inside_exon"
BREAK_EXON_BORDER = "break_exon_border"
BREAK_INTRON = "break_intron"

class IntergenicRegion(HTSeq.GenomicInterval):
    def __init__(self, name, chrom, start_pos, end_pos):
        super(IntergenicRegion, self).__init__(chrom, start_pos, end_pos, ".")
        self.name = name


def loadIntergenicRegions(path_to_igr, min_region_size):

    regions = []

    for line in open(path_to_igr):
        if line.startswith("#") or len(line.strip()) == 0:
            continue

        items = line.strip().split()

        assert len(items) == 4, "Name (4th column) field missing in intergenic annotations"

        region = IntergenicRegion(items[3], items[0], int(items[1]) + 1, int(items[2]))

        if region.length < min_region_size:
            continue

        regions.append(region)

    return regions


def loadGeneAnnotations(inputGtf, supported_biotypes = ("protein_coding") ):

    transcripts = {}

    gtf_file = HTSeq.GFF_Reader( inputGtf )

    for feature in gtf_file:
        biotype = feature.attr["gene_biotype"]
        if supported_biotypes and not biotype in supported_biotypes:
            continue

        if feature.type == 'exon':
            transcriptName = feature.attr[ "transcript_name" ]
            tr_features = transcripts.get(transcriptName, [] )
            if tr_features:
                saved_feature = tr_features[0]
                if saved_feature.attr["transcript_id"] != feature.attr["transcript_id"]:
                    #print "BAD GUY: %s" % transcriptName
                    continue
            tr_features.append(feature)
            transcripts[transcriptName] = tr_features

    return transcripts


def getBreakCoordsInsideDownstreamIntron(strand, exon, next_exon  ):
    if strand == '+':
        exon_start = exon.iv.start + 1
        next_start = next_exon.iv.start
        exon_end = random.randint(exon.iv.end + 1, next_start - 1)
    else:
        assert strand == '-'
        exon_end = exon.iv.end + 1
        prev_end = next_exon.iv.end
        assert prev_end < exon.iv.start
        #print "Choosing intronic breakpoint between %d and %d. exon_id = %d" % (prev_end + 1, feature.iv.start - 1, exon_id)
        exon_start = random.randint(prev_end + 1, exon.iv.start - 1)

    return exon_start, exon_end


def getBreakCoordsInsideExon(upstream, strand, exon_start, exon_end):

    if upstream:
        if strand == '+':
            exon_end = random.randint(exon_start + 1, exon_end - 1)
        else:
            assert strand == '-'
            exon_start = random.randint(exon_start + 1, exon_end - 1)
    else:
        if strand == '+':
            exon_start = random.randint(exon_start + 1, exon_end - 1)
        else:
            assert strand == '-'
            exon_end = random.randint(exon_start + 1, exon_end - 1)

    return exon_start, exon_end




def constructFusionPart(transcript_exons, first_exon_id, last_exon_id, index, break_type ):

    fe = transcript_exons[0]
    fusionPart = simlib.GenePart(fe.attr["gene_name"], fe.attr["transcript_name"], fe.iv.chrom, fe.iv.strand)

    num_exons = len(transcript_exons)

    exon_starts = []
    exon_ends = []
    exon_ids = []

    for i,feature in enumerate(transcript_exons):
        exon_id = int(feature.attr["exon_number"]) - 1
        if exon_id >= first_exon_id and exon_id <= last_exon_id:
            exon_start = feature.iv.start + 1
            exon_end = feature.iv.end
            if (index == 0 and exon_id == last_exon_id) or (index == 1 and exon_id == first_exon_id):
                if break_type == BREAK_INTRON and index == 0:
                    exon_start, exon_end = getBreakCoordsInsideDownstreamIntron(fusionPart.strand, feature, transcript_exons[i+1])
                elif break_type == BREAK_INSIDE_EXON:
                    exon_start, exon_end = getBreakCoordsInsideExon(index == 0, fusionPart.strand, exon_start, exon_end)

            exon_starts.append(exon_start)
            exon_ends.append(exon_end)

            if feature.iv.strand == '-':
                exon_id = num_exons - exon_id - 1
            exon_ids.append(exon_id)


    fusionPart.exonIndexes = sorted(exon_ids)
    fusionPart.exonStarts = sorted(exon_starts)
    fusionPart.exonEnds = sorted(exon_ends)
    fusionPart.compLength()

    fusionPart.features.append(break_type)

    return fusionPart


def pickFusionPartner(index, transcripts, break_type = BREAK_EXON_BORDER):

    transcript_name = random.choice(transcripts.keys())
    transcript_exons = transcripts[transcript_name]

    if index == 0:
        first_exon_id = 0
        if break_type == BREAK_INTRON:
            last_exon_id = random.randint(0,len(transcript_exons) - 2)
        elif break_type == BREAK_INSIDE_EXON:
            last_exon_id = random.randint(1,len(transcript_exons) - 1)
        else:
            last_exon_id = random.randint(0, len(transcript_exons) - 1 )
    else:
        if break_type == BREAK_INSIDE_EXON:
            first_exon_id = random.randint(0, len(transcript_exons) - 2)
        else:
            first_exon_id = random.randint(0, len(transcript_exons) - 1 )
        last_exon_id = len(transcript_exons) - 1

    fusionPart = constructFusionPart(transcript_exons, first_exon_id, last_exon_id, index, break_type)

    return fusionPart

def outputFusionGenePartner(outFile, gene, fusion_id ):

    outFile.write( "%s\t%s\t%s\t" % (fusion_id, gene.geneName, gene.transcriptName ) )
    outFile.write( "%s\t%s\t%d\t%d\t" % (gene.seqName, gene.strand, gene.getNumExons(), len(gene) ) )

    outFile.write( "%s\t" % ",".join ( map( str, gene.exonIndexes ) ) )
    outFile.write( "%s\t" % ",".join ( map( str, gene.exonStarts ) ) )
    outFile.write( "%s\t" % ",".join ( map( str, gene.exonEnds ) ) )

    outFile.write("hybrid\t%s" % ",".join(gene.features))

    outFile.write("\n")

def outputFusionGene(outFile, gene1, gene2):

    fusion_id = "%s-%s" % (gene1.transcriptName, gene2.transcriptName)

    outputFusionGenePartner(outFile, gene1, fusion_id )
    outputFusionGenePartner(outFile, gene2, fusion_id )


def pickFusionPartnerFromIntergenicRegion(regions, min_size, max_size):

    region_idx = random.randint(0,len(regions) - 1)
    region = regions[region_idx]

    region_strand = random.choice( ('+','-') )

    fusionPart = simlib.GenePart(region.name, region.name, region.chrom, region_strand)

    start_pos = region.start + random.randint(1, region.length - max_size - 1)
    end_pos = random.randint( start_pos + min_size, start_pos + max_size - 1)

    fusionPart.exonStarts = [ start_pos + 1]
    fusionPart.exonEnds = [ end_pos ]
    fusionPart.exonIndexes = [ 0 ]
    fusionPart.compLength()
    fusionPart.features.append("intergenic")

    return fusionPart


def filterTranscriptsBasedOnExonNumber(transcripts, num_exons):
    new_transcripts = {}

    for t_name in transcripts:
        exons = transcripts[t_name]
        if len(exons) >= num_exons:
            new_transcripts[t_name] = exons

    return new_transcripts


def pickFusionPartnerIsoforms(index, transcripts, numIsoforms):

    res = []
    transcript_name = random.choice(transcripts.keys())
    transcript_exons = transcripts[transcript_name]

    if index == 0:
        first_exon_id = 0
        last_exon_ids = random.sample( range(0, len(transcript_exons) - 1), numIsoforms )
        for last_exon_id in last_exon_ids:
            fusionPart = constructFusionPart(transcript_exons, first_exon_id, last_exon_id, 0, BREAK_EXON_BORDER)
            fusionPart.features.append("isoform")
            res.append(fusionPart)

    else:
        first_exon_ids = random.sample( range(0, len(transcript_exons) - 1), numIsoforms )
        last_exon_id = len(transcript_exons) - 1
        for first_exon_id in first_exon_ids:
            fusionPart = constructFusionPart(transcript_exons, first_exon_id, last_exon_id, 1, BREAK_EXON_BORDER)
            fusionPart.features.append( "isoform" )
            res.append(fusionPart)

    return res



def simulateIsoforms(outputFile, numIsoforms):

    fusions = []
    ig_idx = random.randint(0,1)

    if ig_idx == 0:
        isoforms = pickFusionPartnerIsoforms(0, transcripts, numIsoforms )
        gene2 = pickFusionPartner(1, transcripts)
        for gene1 in isoforms:
            fusions.append( (gene1, gene2) )
    else:
        gene1 = pickFusionPartner(0, transcripts)
        isoforms = pickFusionPartnerIsoforms(1, transcripts, numIsoforms )
        for gene2 in isoforms:
            fusions.append( (gene1, gene2) )

    for i, (gene1,gene2) in enumerate(fusions):
        fusion_id = "%s-%s-iso%d" % (gene1.transcriptName, gene2.transcriptName,i+1)
        outputFusionGenePartner(outputFile, gene1, fusion_id )
        outputFusionGenePartner(outputFile, gene2, fusion_id )

if __name__ == "__main__":
    
    descriptionText = "The script simulates gene fusions from GTF file based on given parameters."
    
    parser = argparse.ArgumentParser(description = descriptionText,formatter_class=argparse.RawDescriptionHelpFormatter)
    
    parser.add_argument("-g" , action="store", dest="inputGtf",  required="true", help="Input file in GTF format")

    parser.add_argument("-o", action="store", dest="output", default="-", help="Output fusions file. Default is std out")

    parser.add_argument("--intergenic", action="store", default="", dest="intergenicRegions",
                        help="Path to intergenic regions. Required to create fusions together with intergenic regions")

    parser.add_argument("--num-isoforms", action="store", default=0, type=int, dest="numIsoforms",
                        help="Simulate this number of isoforms for each fusion gene.")

    parser.add_argument("--break-type", action="store", type=int, default=0,  dest="breakType",
                        help="Fusion break type. Possible values: 0 - %s, 1 - %s, 2 - %s"
                             % (BREAK_EXON_BORDER, BREAK_INSIDE_EXON, BREAK_INTRON) )

    parser.add_argument("--max-ig-exon-size", action="store", type=int, default=500, dest="max_igr_size",
                        help="Maximum size of an intergenic \"exon\". Default is 500.")

    parser.add_argument("--min-ig-exon-size", action="store", type=int, default=300, dest="min_igr_size",
                    help="Maximum size of an intergenic \"exon\". Default is 300.")


    parser.add_argument("-n", action="store", type=int, required="true", dest="numGenes", help="Number of genes to simulate")

    parser.add_argument("-s", action="store", type=int, default=42, dest="seed", help="Random seed")

    args = parser.parse_args()

    random.seed(args.seed)

    transcripts = loadGeneAnnotations(args.inputGtf)

    outputFile = sys.stdout if args.output == "-" else open(args.output, "w")

    if args.intergenicRegions:
        igr_regions = loadIntergenicRegions(args.intergenicRegions, args.max_igr_size)

    break_type = BREAK_EXON_BORDER

    if args.breakType == 0:
        break_type = BREAK_EXON_BORDER
    elif args.breakType == 1:
        break_type = BREAK_INSIDE_EXON
    elif args.breakType == 2:
        break_type = BREAK_INTRON
    else:
        assert 0, "Unknown break type %d. Possible types are 0(%s), 1(%s) or 2(%s)" \
                  % (args.breakType,BREAK_EXON_BORDER, BREAK_INSIDE_EXON, BREAK_INTRON)

    if break_type == BREAK_INTRON or break_type == BREAK_INSIDE_EXON:
        transcripts = filterTranscriptsBasedOnExonNumber(transcripts, 2)
    elif args.numIsoforms > 0:
        transcripts = filterTranscriptsBasedOnExonNumber(transcripts, args.numIsoforms + 1)

    for i in range(args.numGenes):
        if args.numIsoforms > 0:
            simulateIsoforms(outputFile, args.numIsoforms)
        elif args.intergenicRegions:
            ig_idx = random.randint(0,1)
            if ig_idx == 0:
                gene1 = pickFusionPartnerFromIntergenicRegion(igr_regions, args.min_igr_size, args.max_igr_size )
                gene2 = pickFusionPartner(1, transcripts)
            else:
                gene1 = pickFusionPartner(0, transcripts)
                gene2 = pickFusionPartnerFromIntergenicRegion(igr_regions, args.min_igr_size, args.max_igr_size )
            outputFusionGene(outputFile, gene1, gene2)

        else:
            gene1 = pickFusionPartner(0, transcripts, break_type)
            gene2 = pickFusionPartner(1, transcripts, break_type)
            outputFusionGene(outputFile, gene1, gene2)










