import re
import sys
import argparse
import os.path
from Bio import SeqIO
from Bio.Seq import Seq

def getRegions( seqName, rStart, startStr, lengthStr ):
    regions = []
    
    starts = startStr.split(",")
    lens = lengthStr.split(",")

    for i,s in enumerate(starts[0:-1]):
        startPos = rStart + int(s) 
        endPos = startPos + int(lens[i])
        regions.append ( (seqName, startPos, endPos) )

    return regions


if __name__ == "__main__":
    
    descriptionText = "The script converts UCSC BED file to GTF format."
    
    parser = argparse.ArgumentParser(description = descriptionText,formatter_class=argparse.RawDescriptionHelpFormatter)
    
    parser.add_argument("-b", action="store", required="true", dest="bedFile", help="Input file with list of genes in UCSC Bed format")
    parser.add_argument("-o", action="store", dest="outFile", default="", help="Output file. Default is output.fa")
    
    args = parser.parse_args()
    
    print args
    
    bedFileName = args.bedFile
    outFileName = args.outFile
    
    
    outFileName = bedFileName.replace(".bed", ".gtf")

    outFile = open(outFileName, "w")

    # parse BED file

    features = {}

    for line in open(bedFileName):
        items = line.strip().split("\t")
        regions = []
        rStart = int(items[1])
        seqName = items[0]
        regions = getRegions( seqName, rStart, items[11], items[10])
        
        transcriptId = items[3]
        strand = items[5]
        
        for r in regions:
            geneId = "gene_" + transcriptId
            record = "%s\tucsc_knonwnGene\texon\t%d\t%d\t0.000\t%s\t." % (seqName, r[1] + 1, r[2],strand)  
            record += "\tgene_id \"%s\"; transcript_id \"%s\";" % (geneId, transcriptId)
            record += "\n"

            outFile.write(record)



      



