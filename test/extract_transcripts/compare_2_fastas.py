#!/usr/bin/python

import argparse
from Bio import SeqIO
from Bio.Seq import Seq


if __name__ == "__main__":

    descriptionText = "Script compares 2 multiFASTA files"

    parser = argparse.ArgumentParser(description = descriptionText,formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument("fastaFile1", help="First multifasta")
    parser.add_argument( "fastaFile2",  help="Second multifasta ")

    args = parser.parse_args()

    print args

    fastaFileName1 = args.fastaFile1
    fastaFileName2 = args.fastaFile2


    # load & save sequences

    seqData1 = SeqIO.to_dict(SeqIO.parse(fastaFileName1, "fasta"))
    seqData2 = SeqIO.to_dict(SeqIO.parse(fastaFileName2, "fasta"))

    for seqName in seqData1:


        if not seqName in seqData2:
            print "%s from %s is not found in %s" % (seqName, fastaFileName1, fastaFileName2)
            exit(-1)

        seq_rec = Seq("")

        seq1 = seqData1[seqName].seq
        seq2 = seqData2[seqName].seq

        if str(seq1) != str(seq2):
            print "%s from %s doesn't match with the one from %s" % (seqName, fastaFileName1, fastaFileName2)
            print ">seq1\n%s\n>seq2\n%s\n" % (seq1, seq2)
            exit(-1)


    exit(0)
