#ifndef SAMUTILS_H
#define SAMUTILS_H

#include <seqan/bam_io.h>

using seqan::BamAlignmentRecord;

class SamUtils
{
public:
    static int getNumNonMatches(const BamAlignmentRecord& rec);
};

#endif // SAMUTILS_H
