#include "CreateFinalAlignmentTask.h"
#include "ChunkBamReader.h"
#include "PairedAlignmentsStore.h"
#include "LocalAlignmentStore.h"
#include "StringUtils.h"

using namespace std;
using namespace seqan;


bool CreateFinalAlignmentTask::init()
{

    cerr << "Loading fusion clusters..." << endl;

    if ( !Fusion::readFusionClusters(fusions, cfg.fusionsPath ) ) {
        cerr << "Failed to read fusions from " << cfg.fusionsPath << endl;
        return false;
    }


    for (size_t idx = 0; idx < fusions.size(); ++idx) {
        const Fusion& f = fusions.at(idx);
        const vector<BreakpointCandidatePtr>& bps = f.getBreakpoints();
        foreach_ (const BreakpointCandidatePtr& bp, bps) {
            fusionReadMap[bp->readName] = idx;
        }
    }


    if (cfg.annotationsPath.length() > 0) {
        cerr << "Loading transcript annotations..." << endl;

        if (! geneModels.loadTranscripts(cfg.annotationsPath)) {
            cerr << "Failed to load gene models from " << cfg.annotationsPath << endl;
            return false;
        }
    } else if (cfg.transcriptomicAlignmentsPath.size() > 0 ) {
        cerr << "ERROR! Annotations are not provided along with transcriptomic alignments!" << endl;
        return false;
    }

    return true;

}

int CreateFinalAlignmentTask::run()
{
    if (!init()) {
        return -1;
    }

    cerr << "Writing BAM header..." << endl;
    if (!writeHeader()) {
        return -1;
    }


    if (cfg.localAlignmentsPath.size() == 1) {

        if (!writeLocalAlignments(cfg.localAlignmentsPath[0], "")) {
            return -1;
        }

    } else if (cfg.localAlignmentsPath.size() == 2) {

        if (!writeLocalAlignments(cfg.localAlignmentsPath[0], "/1")) {
            return -1;
        }

        if (!writeLocalAlignments(cfg.localAlignmentsPath[1], "/2")) {
            return -1;
        }

    }

    if (!writeTranscriptomicAlignments()) {
        return -1;
    }

    return close(outBamStream);

}

bool CreateFinalAlignmentTask::writeHeader()
{
    BamStream bamStreamIn(cfg.headerBamPath.c_str(), BamStream::READ, BamStream::BAM);
    if (!isGood(bamStreamIn)) {
        cerr << "Failed to open input alignment file: " << cfg.headerBamPath << endl;
        return false;
    }

    open(outBamStream, cfg.outputPath.c_str(),BamStream::WRITE, cfg.useSamFormat ? BamStream::SAM : BamStream::BAM);
    if (!isGood(outBamStream)) {
        cerr << "Failed to create out file for  alignments: " << cfg.outputPath << endl;
        return false;
    }

    outBamStream.header = bamStreamIn.header;
    outBamStream._nameStore = bamStreamIn._nameStore;
    assert(length(bamStreamIn.header.records) > 0);
    assert(length(bamStreamIn._nameStore) > 0);

    for (size_t i = 0; i < length(outBamStream.header.records); ++i) {
        BamHeaderRecord& rec = outBamStream.header.records[i];
        if (rec.type == BAM_HEADER_PROGRAM) {
            String<Pair<CharString, CharString> > newTags;
            append(newTags, Pair<CharString,CharString>("ID","InFusion"));
            append(newTags, Pair<CharString,CharString>("VN", INFUSION_VERSION));
            rec.tags = newTags;
        }
    }

    return true;

}

static map<string,int> createRefIdMap(const StringSet<CharString>&   nameStore) {

    map<string,int> refIdMap;
    int len = length(nameStore);

    for (int i = 0; i < len; ++i) {
        string refName(toCString(nameStore[i]));
        refIdMap[refName] = i;
    }

    return refIdMap;
}


static vector<string> createTranscriptNameList(const StringSet<CharString>& transcriptNameSet ) {
    vector<string> transcriptNames;
    int numTranscripts = length(transcriptNameSet);

    for (int i = 0; i < numTranscripts; ++i) {
        transcriptNames.push_back(toCString(transcriptNameSet[i]));
    }

    return transcriptNames;
}


void CreateFinalAlignmentTask::convertToGenomic(seqan::BamAlignmentRecord& record, const TranscriptPtr& transcript, map<string,int>& refIdMap ) {

    /*if (record.qName == "SRR018268.15446335") {
        cerr << "HELLO!" << endl;
    }*/

    bool transcriptRC = !transcript->isPositiveStranded();
    int newStartPos = -1;

    String<CigarElement<> > newCigar;

    int numElem = length(record.cigar);
    int posInRef = record.beginPos;
    for (int i = 0; i < numElem; ++i) {
        CigarElement<>& elem = record.cigar[i];
        if (elem.operation == 'M' ) {
            vector<GenomicInterval> intervals = transcript->getGenomicIntervals(posInRef, elem.count);
            if (newStartPos == -1) {
                newStartPos = intervals.front().begin;
            }
            int numIntervals = intervals.size();
            for (int i = 0; i < numIntervals; ++i) {
                const GenomicInterval& iv = intervals[i];
                if (i > 0 ) {
                    const GenomicInterval& prevIv = intervals[i - 1];
                    CigarElement<> skip('N', iv.begin - prevIv.end - 1);
                    appendValue(newCigar, skip);
                }
                CigarElement<> match('M', iv.length() );
                appendValue(newCigar,match );
            }
            posInRef += elem.count;
        } else {
            if (elem.operation == 'D') {
                posInRef += elem.count;
            }
            appendValue(newCigar, elem);
        }
    }

    bool readRC = hasFlagRC(record);
    const string& refName = transcript->getRefName();
    int refId = refIdMap[refName];
    record.rID = refId;
    record.beginPos = newStartPos;
    record.cigar = newCigar;

    if (transcriptRC) {
        reverseComplement(record.seq);
        reverse(record.qual);
        record.flag = readRC ? record.flag & ~BAM_FLAG_RC : record.flag | BAM_FLAG_RC;
    }

}


bool CreateFinalAlignmentTask::writeTranscriptomicAlignments()
{

    if (cfg.transcriptomicAlignmentsPath.size() == 0) {
        return true;
    }


    const TranscriptSet& transcripts = geneModels.getTranscripts();
    ChunkBamReaderConfig readerConfig;
    // remove mate id?
    readerConfig.removeMateId = true;
    ChunkBamReader bamReader(cfg.transcriptomicAlignmentsPath.c_str(), readerConfig);

    if (!bamReader.init()) {
        cerr << "Failed to open BAM file: " << cfg.transcriptomicAlignmentsPath << endl;
        return false;
    }


    cerr << "Processing file " << cfg.transcriptomicAlignmentsPath << endl;

    vector<string> refNames;
    StringUtils::convertStringSetToVector(outBamStream._nameStore, refNames);

    if (refNames.size() == 0) {
        cerr << "References names are emtpy! Transcriptomic alignements are not processed." << endl;
        return false;
    }

    map<string,int> refIdMap= createRefIdMap( outBamStream._nameStore );
    vector<string> transcriptNames = createTranscriptNameList( bamReader.getRefNames() );

    while (!bamReader.isEof()) {

        vector<BamAlignmentRecord> records;
        if (!bamReader.loadNextChunk(records)) {
            cerr << "Error reading data from BAM file: " << cfg.transcriptomicAlignmentsPath << endl;
        }

        size_t numRecords = records.size();
        int recordCount = 0;
        for (size_t i = 0; i < numRecords; ++i) {

            BamAlignmentRecord& record =  records[i];
            if (hasFlagFirst(record) || hasFlagLast(record)) {
                // paired data
                record.flag = record.flag | BAM_FLAG_MULTIPLE;
            }
#ifdef DEBUG
            if (record.qName == "LRRC37B-010-DDX5-010.fa.fastq.000000254") {
            //if (record.qName == "TRPV1-004-NAT9-004.fa.fastq.000000273/1") {
                cerr << "FOUND!" << endl;
            }
#endif
            BamTagsDict tags(record.tags);
            uint idx = -1;
            bool genomicAln = findTagKey(idx, tags, CharString("ZG"));

            if (genomicAln) {
                const string& refName = transcriptNames.at (record.rID);
                record.rID = refIdMap.at(refName);

            } else {

                const string& transcriptName = transcriptNames.at( record.rID );

                TranscriptSet::const_iterator it = transcripts.find(transcriptName);
                if ( it  == transcripts.end()) {
                    //cerr << "Failed to find transcript sequence " << transcriptName << ", ignoring read " << record.qName << endl;
                    continue;
                }

                convertToGenomic(record, it->second, refIdMap);
            }

            auto iter = fusionReadMap.find(record.qName);

            if (iter != fusionReadMap.end()) {

                int fusionIdx = iter->second;
                const Fusion& f = fusions.at(fusionIdx);
                GenomicInterval iv(refNames[record.rID], record.beginPos, record.beginPos + getAlignmentLengthInRef(record));

                int fusionIntervalNumber = -1;
                if (f.getEncompassingInterval(0).intersects(iv)) {
                    fusionIntervalNumber = 0;
                } else if (f.getEncompassingInterval(1).intersects(iv)) {
                    fusionIntervalNumber = 1;
                } else {
                    continue;
                }

                setTagValue(tags, "XF", f.getIndex());
                setTagValue(tags, "XN", fusionIntervalNumber);
            }

            int res = writeRecord(outBamStream, record);
            if (res != 0) {
                cerr << "Failed to write a record " << record.qName << endl;
                return false;
            }
            recordCount++;

        }

#ifdef DEBUG
        cerr << "Written " << recordCount << " reads..." << endl;
#endif


    }


    return true;


}

bool CreateFinalAlignmentTask::writeLocalAlignments(const string& alignmentPath, const char* mateId)
{


    cerr << "Processing file " << alignmentPath << endl;
    ChunkBamReaderConfig readerConfig;
    // remove mate id?
    readerConfig.removeMateId = false;
    ChunkBamReader bamReader(alignmentPath.c_str(), readerConfig);

    if (!bamReader.init()) {
        cerr << "Failed to open BAM file: " << alignmentPath << endl;
        return false;
    }

    vector<string> refNames;
    StringUtils::convertStringSetToVector(bamReader.getRefNames(), refNames);

    int recordCount = 0;
    const int MAX_MISMATCHES = 2;

    while (!bamReader.isEof()) {

        vector<BamAlignmentRecord> records;
        if (!bamReader.loadNextChunk(records)) {
            cerr << "Error reading data from BAM file: " << alignmentPath << endl;
        }

        //TODO: what to do with rescued reads?

        size_t numRecords = records.size();
        for (size_t i = 0; i < numRecords; ++i) {

            BamAlignmentRecord& record =  records[i];
            //assert(hasFlagFirst(record) || hasFlagLast(record));

            /*if (record.qName == "SRR201779.1051866") {
                cerr << "FOUND!" << endl;
            }*/

            bool ok = false;
            int numMismatches = LocalAlignmentStore::getIntTagValue(record, "NM", ok);
            int score = LocalAlignmentStore::getIntTagValue(record, "AS", ok);
            // skip local alignments having more than N mismatches
            if (score > 0 && numMismatches > MAX_MISMATCHES ) {
                continue;
            }

            append(record.qName, mateId);
            auto iter = fusionReadMap.find(record.qName );

            if (iter == fusionReadMap.end()) {
                continue;
            }

            int fusionIdx = iter->second;
            const Fusion& f = fusions.at(fusionIdx);
            GenomicInterval iv(refNames[record.rID], record.beginPos, record.beginPos + getAlignmentLengthInRef(record));

            int fusionIntervalNumber = -1;
            if (f.getEncompassingInterval(0).intersects(iv)) {
                fusionIntervalNumber = 0;
            } else if (f.getEncompassingInterval(1).intersects(iv)) {
                fusionIntervalNumber = 1;
            } else {
                continue;
            }

            BamTagsDict tags(record.tags);
            setTagValue(tags, "XF", f.getIndex());
            setTagValue(tags, "XN", fusionIntervalNumber);

            int res = writeRecord(outBamStream, record);
            if (res != 0) {
                cerr << "Failed to write a record " << record.qName << endl;
                return false;
            }
            recordCount++;



        }

    }

#ifdef DEBUG
        cerr << "Written " << recordCount << " reads..." << endl;
#endif


    return true;
}


