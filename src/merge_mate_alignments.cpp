#include <iostream>
#include <algorithm>

#include <seqan/basic.h>
#include <seqan/sequence.h>
#include <seqan/file.h>

#include <seqan/stream.h>
#include <seqan/bam_io.h>
#include <seqan/arg_parse.h>
#include <seqan/modifier.h>

#include "ChunkBamReader.h"
#include "StringUtils.h"

using namespace seqan;
using namespace std;

#define PROGRAM_NAME                    "merge_mate_alignments"


struct MergeMatesConfig {
    CharString inFile1, inFile2, outFile;
    CharString genomicAln1, genomicAln2;
    bool useSamFormat;
    MergeMatesConfig() : useSamFormat(false) {}
};


struct BamStreamContext {
    BamStream stream;
    bool genomic;
    bool upstream;
};

void setupArgumentParser( ArgumentParser& parser )
{
    setVersion(parser, INFUSION_VERSION);

    addDescription(parser, "This tool is part of InFusion pipeline. It merges alignments of upstream\
                   and downstream mates with setting of appropariate flag");

    string usageLine;
    usageLine.append(PROGRAM_NAME).append(" [OPTIONS] UPSTREAM_BAM DOWNSTREAM_BAM");
    addUsageLine(parser, usageLine);

    addArgument(parser, ArgParseArgument(ArgParseArgument::INPUTFILE, "upstream_bam"));
    addArgument(parser, ArgParseArgument(ArgParseArgument::INPUTFILE, "downstream_bam"));

    addSection(parser, "Options");

    addOption(parser, ArgParseOption( "o", "output", "Path to output file.", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "g1", "genomic1", "Path to upstream genomic alignment", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "g2", "genomic2", "Path to downstream genomic alignment", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "s", "sam", "Path output in SAM format, default is BAM." ));


    addTextSection(parser, "References");
    addText(parser, "Konstantin Okonechnikov <okonechnikov@mpiib-berlin.mpg.de>");
}


/*static bool headerHasFirstRecord(const BamHeader& header) {
    for (size_t i = 0; i < length(header.records); ++i) {
        const BamHeaderRecord& rec = header.records[i];

        if (rec.type == BAM_HEADER_FIRST) {
            return true;
        }
    }

    return false;
}

static bool headerIncludesSequence(const BamHeader& header, const CharString& seqName ) {

    for (size_t i = 0; i < length(header.records); ++i) {
        const BamHeaderRecord& rec = header.records[i];
        if (rec.type == BAM_HEADER_REFERENCE) {
            CharString refName;
            getTagValue(refName, "SN", rec);

            if (refName == seqName) {
                return true;
            }

        }
    }

    return false;
}*/


void mergeHeaders(BamHeader& header, const BamHeader& headerToMerge, bool addFirstRecord, bool addSequences) {

    for (size_t i = 0; i < length(headerToMerge.records); ++i) {
        const BamHeaderRecord& rec = headerToMerge.records[i];
        if (rec.type == BAM_HEADER_FIRST && addFirstRecord) {
            if ( addFirstRecord ) {
                append(header.records,rec);
            }
        } else if (rec.type == BAM_HEADER_REFERENCE && addSequences) {
            CharString refName;
            getTagValue(refName, "SN", rec);
            append(header.records, rec );
            BamHeader::TSequenceInfo info;
            info.i1 = refName;
            info.i2 = length(header.sequenceInfos);
            append(header.sequenceInfos, info);

        }
    }

}

bool initInputStream(BamStream& bamInput, const CharString& inFile, BamStream& bamOutput ) {

    if (open(bamInput, toCString(inFile), BamStream::READ) != 0) {
        cerr << "Problem opening input BAM file " << inFile << endl;
        return false;
    }

    return true;
}



bool writeAlignmentsToOutput(BamStream& bamInput, bool upstream, bool genomic, BamStream& bamOutput, int refIdOffset ) {

    BamAlignmentRecord record;

    while (true)
    {

        if (atEnd(bamInput)) {
            break;
        } else {
            if (readRecord(record, bamInput) != 0)
            {
                std::cerr << "Could not read alignment from " << bamInput._filename << std::endl;
                return false;
            }

            if (genomic) {
                if (!ChunkBamReader::readCompletelytAligned(record)) {
                    continue;
                }

                // set tag that it is a genomic alignment

                CharString samTags;
                assignTagsBamToSam(samTags, record.tags);
                samTags += "\tZG:i:1";
                assignTagsSamToBam(record.tags, samTags);

            }

            StringUtils::addReadMateIdentifier(record.qName, upstream ? '1' : '2');


            if (upstream) {
                record.flag = record.flag | BAM_FLAG_FIRST;
            } else {
                 record.flag = record.flag | BAM_FLAG_LAST;
            }
            record.rID += refIdOffset;

            if (writeRecord(bamOutput, record) != 0) {
                std::cerr << "Could not write alignment!" << std::endl;
                return 1;
            }
        }


    }

    return true;

}


int performAnalysis(const MergeMatesConfig& cfg) {


    BamStream bamOutput(toCString(cfg.outFile), BamStream::WRITE, cfg.useSamFormat ? BamStream::SAM : BamStream::BAM);

    if (!isGood(bamOutput)) {
        cerr << "Problem with input BAM file " << cfg.outFile << endl;
        return false;
    }


    BamStream transInput1, transInput2, genomicInput1, genomicInput2;

    if ( !initInputStream(transInput1, cfg.inFile1, bamOutput) ) {
        return 1;
    }
    mergeHeaders(bamOutput.header, transInput1.header, true, true );


    if ( !initInputStream(transInput2, cfg.inFile2, bamOutput) ) {
        return 1;
    }

    int refIdOffset = length(bamOutput.header.sequenceInfos);

    if (length(cfg.genomicAln1) > 0 && length(cfg.genomicAln2) > 0) {

        if ( !initInputStream(genomicInput1, cfg.genomicAln1, bamOutput) ) {
            return 1;
        }
        mergeHeaders(bamOutput.header, genomicInput1.header, false, true);


        if ( !initInputStream(genomicInput2, cfg.genomicAln2, bamOutput) ) {
            return 1;
        }


    }

    // header is ready, start writing

    if (!writeAlignmentsToOutput(transInput1, true, false, bamOutput, 0)) {
        return 1;
    }

    if (!writeAlignmentsToOutput(transInput2, false, false, bamOutput, 0)) {
        return 1;
    }

    if (length(cfg.genomicAln1) > 0 && length(cfg.genomicAln2) > 0) {


        if (!writeAlignmentsToOutput(genomicInput1, true, true, bamOutput, refIdOffset)) {
            return 1;
        }

        if (!writeAlignmentsToOutput(genomicInput2, false, true, bamOutput, refIdOffset)) {
            return 1;
        }

    }

    /*string outHeaderPath(toCString(cfg.outFile));
    outHeaderPath += ".header";
    std::fstream headerOutStream(outHeaderPath, std::ios::binary | std::ios::out);
    if (!headerOutStream.good())
    {
        std::cerr << "ERROR: Could not open " << outHeaderPath << " for writing.\n";
        return 1;
    }

    if (write2(headerOutStream, bamOutput.header, bamOutput.bamIOContext, seqan::Sam()) != 0)
    {
        std::cerr << "ERROR: Could not write header to file " << outHeaderPath << "\n";
        return 1;
    }*/

    return 0;

}


int main(int argc, char const ** argv)
{

    ArgumentParser parser(PROGRAM_NAME);
    setupArgumentParser(parser);

    ArgumentParser::ParseResult res = parse(parser, argc, argv);
    if (res != ArgumentParser::PARSE_OK) {
        return 1;
    }

    MergeMatesConfig cfg;

    getArgumentValue(cfg.inFile1, parser, 0);
    getArgumentValue(cfg.inFile2, parser, 1);
    getOptionValue(cfg.outFile, parser, "o");
    getOptionValue(cfg.useSamFormat, parser, "s");
    getOptionValue(cfg.genomicAln1, parser, "g1");
    getOptionValue(cfg.genomicAln2, parser, "g2");

#ifdef DEBUG
    cerr << "Options" << endl;
    cerr << "Upstream transcriptomic BAM file: " << cfg.inFile1 << endl;
    cerr << "Downstream transcriptomic BAM file: " << cfg.inFile2 << endl;
    cerr << "Output BAM file: " << cfg.outFile << endl;

#endif

    return performAnalysis(cfg);
}



