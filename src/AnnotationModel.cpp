#include "AnnotationModel.h"

#include <fstream>
#include <seqan/file.h>
#include <seqan/sequence.h>
#include <seqan/store.h>
#include <seqan/gff_io.h>

#include "StringUtils.h"

using std::cout;
using std::endl;
using namespace seqan;

bool Transcript::init()
{

    sort(exons.begin(), exons.end(), FeatureCompare());

    startPos = exons.at(0)->startPos;
    seqName = exons.at(0)->seqName;
    positiveStranded = exons.at(0)->positiveStranded;

    length = 0;
    for (auto it = exons.begin(); it != exons.end(); ++it) {
        ExonPtr f = (*it);
        length += f->length;

        // All exons must have the same reference and strand

        if (seqName != f->seqName) {
            return false;
        }
        if (positiveStranded != f->positiveStranded) {
            return false;
        }
    }

    return true;
}

int Transcript::getEndPos() const
{
    return exons.back()->endPos();
}

const string& Transcript::getGeneName() const
{
    assert(gene);
    return gene->getName();
}

const string &Transcript::getGeneId() const
{
    assert(gene);
    return gene->getId();
}

const string &Transcript::getGeneBioType() const
{
    assert(gene);
    return gene->getBioType();
}

int Transcript::getGenomicPos(int localPos) const
{
    assert(localPos < length);

    int accumLength = 0;
    int startPos = -1;
    int strandSign = positiveStranded ? 1 : -1;

    if (positiveStranded) {
        for (size_t i = 0, sz = exons.size(); i < sz; ++i) {
            const FeaturePtr& exon = exons[i];
            startPos = exon->startPos;
            if (localPos < accumLength + exon->length) {
                break;
            }

            accumLength += exon->length;
        }
    } else {
        for (size_t i = exons.size() - 1; i >= 0; --i) {
            const FeaturePtr& exon = exons[i];
            startPos = exon->endPos();
            if (localPos < accumLength + exon->length) {
                break;
            }

            accumLength += exon->length;
        }
    }

    return startPos + strandSign*(localPos - accumLength);


}

int Transcript::genomicCoordToTranscriptomic(const string &refName, int pos) const
{
    if (refName != seqName) {
         return -1;
    }

    int offset = 0;
    foreach_ (ExonPtr exon, exons) {
        if (exon->contains(pos)) {
            return offset + (pos - exon->startPos);
        }
        offset += exon->length;
    }

    return -1;

}

bool Transcript::geneContainsGenomicPoint(int pos) const
{
    return ( gene->getStartPos() <= pos && pos <= gene->getEndPos() );
}

vector<GenomicInterval> Transcript::getGenomicIntervals(int localStart, int iLength) const
{
    vector<GenomicInterval> result;
    int localEnd = localStart + iLength - 1;

    if ( (localStart < 0) || (localEnd >= length) ) {
        return result;
    }

    int acumLen = 0;
    int firstExonIndex = -1;

    if (!positiveStranded) {
        // mirror the interval
        localStart =  length - localStart -1;
        localEnd = length - localEnd - 1;
        std::swap(localStart,localEnd);
    }

    for (size_t i = 0, count = exons.size(); i < count; ++i ) {

        const FeaturePtr& e = exons.at(i);

        if (firstExonIndex == -1 && GenomicInterval::contains(acumLen, e->length, localStart)) {
            firstExonIndex = i;
        }

        if (firstExonIndex != -1) {
            if (GenomicInterval::contains(acumLen, e->length, localEnd)) {
                GenomicInterval r;
                r.begin = firstExonIndex == i ? e->startPos + (localStart - acumLen) : e->startPos;
                r.end = e->startPos + localEnd - acumLen;
                result.push_back(r);
                break;
            } else {
                GenomicInterval r;
                r.begin =  firstExonIndex == i ? e->startPos + (localStart - acumLen) : e->startPos;
                r.end = e->endPos();
                result.push_back(r);
            }
        }

        acumLen += e->length;

    }

    return result;
}


AnnotationModel::AnnotationModel() : numRecords(0)
{
}

bool AnnotationModel::loadTranscripts(const string &fileName, bool useIdAsName)
{

    map<string, GenePtr> geneMap;

    seqan::GffStream gffIn(fileName.c_str());

    if (!isGood(gffIn)) {
        return false;
    }

    seqan::GffRecord record;

    while (!atEnd(gffIn))
    {

        string geneId, geneName, transcriptId, transcriptName, exonId, bioType;

        if (0 != readRecord(record, gffIn) ) {
            return false;
        }

        int numTags = length(record.tagValue);
        for (int i = 0; i < numTags; ++i) {
            if (record.tagName[i] == "gene_id" ) {
                geneId = toCString(record.tagValue[i]);
            } else if (record.tagName[i] == "gene_name" ) {
                geneName = toCString(record.tagValue[i]);
            } else if (record.tagName[i] == "transcript_id" ) {
                transcriptId = toCString(record.tagValue[i]);
            }else if (record.tagName[i] == "transcript_name" ) {
                transcriptName = toCString(record.tagValue[i]);
            }else if (record.tagName[i] == "exon_number" ) {
                exonId = toCString(record.tagValue[i]);
            } else if (record.tagName[i] == "gene_biotype") {
                bioType = toCString(record.tagValue[i]);
            }
        }

        if (bioType.length() == 0) {
            bioType = toCString(record.source);
        }

        assert(geneId.length() > 0);
        assert(transcriptId.length() > 0);

        if (geneMap.count(geneId) == 0) {
            GenePtr gene( new Gene(geneId, geneName, 0, 0) );
            gene->setBioType(bioType);
            geneMap[geneId] = gene;
        }

        GenePtr gene = geneMap[geneId];
        string mapId = useIdAsName ? transcriptId : transcriptName;

        if (transcripts.count(mapId) == 0) {
            TranscriptPtr t( new Transcript(transcriptId, transcriptName, gene) );
            transcripts[mapId] = t;
        }

        TranscriptPtr transcript = transcripts[mapId];
        if (record.type == "start_codon") {
            int pos = record.strand == '+' ? record.beginPos : record.endPos - 1;
            transcript->setCdrStart(pos);
        } else if (record.type == "stop_codon") {
            int pos = record.strand == '+' ? record.endPos -1 : record.beginPos;
            transcript->setCdrStop(pos);
        } else if (record.type == "exon" ) {
            ExonPtr exon( new Exon(transcript) );
            exon->positiveStranded = record.strand == '+';
            exon->startPos = record.beginPos;
            exon->id = exonId;
            exon->length = abs(record.endPos - record.beginPos);
            exon->seqName = toCString(record.ref);
            transcript->addExon( exon );
       }

    }

    for (auto iter = transcripts.begin(); iter != transcripts.end(); ++iter) {
        TranscriptPtr t = iter->second;
        if (!t->init()) {
            return false;
        } else {
            int startPos = t->getStartPos();
            int endPos = t->getEndPos();
            GenePtr gene = t->getGene();
            if (gene->getStartPos() == 0 || gene->getStartPos() > startPos) {
                gene->setStartPos(startPos);
            }
            if (gene->getEndPos() == 0 || gene->getEndPos() < endPos) {
                gene->setEndPos(endPos);
            }
        }
    }

    return true;
}

const TranscriptPtr AnnotationModel::getTranscript(const string &transcriptName) const
{
    TranscriptPtr res;
    auto iter = transcripts.find(transcriptName);
    if (iter != transcripts.end()) {
        res = iter->second;
    }

    return res;
}




