#include "ClusterRegistryImpl.h"
#include "GlobalContext.h"
#include "StringUtils.h"
#include "Reference.h"
#include "MiscUtils.h"
#include "PerfTimer.h"

#include <unordered_map>
#include <boost/thread.hpp>
#include <boost/filesystem.hpp>

using namespace std;

void ClusterRegistryImpl::addIntervalToTree(const BreakpointCandidatePtr& bp, int localIdx) {

    const QueryAlignment& aln = bp->alns[localIdx];
    if (clusterTreeMap.count(aln.refId) == 0 ) {
        ClusterTreePtr clusterTreePtr( new ClusterTree<IntervalInfo>() );
        clusterTreeMap.insert( std::make_pair(aln.refId, clusterTreePtr) );
    }

    ClusterTreePtr clusterTree = clusterTreeMap[aln.refId];

    IntervalInfo info(bp, localIdx);
    int start = aln.refStart;
    int end = aln.inRefEnd();
    if (bp->isOriginPaired()) {
        Cluster::Direction dir = Cluster::inferDirection(aln.positiveStrand, localIdx == 0);
        if (dir == Cluster::DirectionLeft) {
            end += config.pairedClusterOverlapTolerance;
        } else {
            start -= config.pairedClusterOverlapTolerance;
        }
    }
    clusterTree->insertInterval(start, end, info);




}

void ClusterRegistryImpl::processBreakpoints(const vector<BreakpointCandidatePtr> &bpntList)
{

    // Clustering alignments from breakpoints

    foreach_( const BreakpointCandidatePtr& bp, bpntList ) {

        addIntervalToTree(bp,0);
        addIntervalToTree(bp,1);

    }

}


class BpPtrHash {
public:
    size_t operator()(const BreakpointCandidatePtr& key) const {
        return (size_t)key.get();
    }
};


void ClusterRegistryImpl::finalizeClustering()
{
    unordered_map<BreakpointCandidatePtr, ClusterSegmentPtr, BpPtrHash> bpToSegmentMap;

    for (auto iter = clusterTreeMap.begin(); iter != clusterTreeMap.end(); ++iter) {
        ClusterTreePtr tree = iter->second;

        ClusterTree<IntervalInfo>::Node node = tree->leftMostCluster();

        while (!node.isNull()) {

            map<ClusterIndex,ClusterPtr> clusterNodeMap;

            clusternode* clusterNode = node.getClusterNode();

            vector<IntervalInfo> intervals;
            node.getIntervalItems(intervals);

            foreach_ (const IntervalInfo& info, intervals) {

                const QueryAlignment& aln = info.bp->alns[info.localIdx];
                Cluster::Direction dir = Cluster::inferDirection(aln.positiveStrand, info.localIdx == 0);

                ClusterIndex clusterIdx = std::make_pair(clusterNode,dir);

                if ( clusterNodeMap.count(clusterIdx) == 0 ) {
                    int curIdx = clusters.size();
                    ClusterPtr newCluster(  new Cluster(curIdx, aln.refId, dir) );
                    clusters.push_back(newCluster);
                    clusterNodeMap[clusterIdx] = newCluster;
                }

                ClusterPtr& cluster = clusterNodeMap.at(clusterIdx);
                ClusterSegmentPtr seg(new ClusterSegment(cluster,info.bp,info.localIdx));
                if (info.localIdx == 0) {
                    seg->setFirst();
                }

                cluster->addSegment(seg);

                if (bpToSegmentMap.count(info.bp) > 0) {
                    ClusterSegmentPtr& partnerSegment = bpToSegmentMap.at(info.bp);
                    seg->setPartner(partnerSegment);
                    partnerSegment->setPartner(seg);
                    bpToSegmentMap.erase(info.bp);
                } else {
                    bpToSegmentMap.insert( make_pair(info.bp,seg) ) ;
                }


            }

            node.stepForward();

        }





    }

    clusterTreeMap.clear();

    cerr << "Created " << clusters.size() << " clusters" << endl;

    if (config.splitClustersByBreakpoint) {
        splitClusterByBreakpoint();
    }

    createClusterGenomicOverlapper();

    if (config.pairedReads) {
        cerr << "Merging crossing over and separated by intron clusters..." << endl;
        mergeCrossingOverClusters();
    }


    foreach_ (ClusterPtr& cluster, clusters) {
        cluster->updateUniquePartners();
    }


}


static void joinClusterSeparatedByIntron(ClusterPtr destCluster, ClusterPtr toJoin) {
    if (toJoin == destCluster) {
        return;
    }

    if ( toJoin->getDirection() == destCluster->getDirection() ) {
        // merging clusters
        const vector<ClusterSegmentPtr>& segmentsToMerge = toJoin->getSegments();
        foreach_ (ClusterSegmentPtr segment , segmentsToMerge ) {
            segment->setParentCluster(destCluster);
            /*if (segment->getBreakpointCandidate()->readName == "SRR018269.8620830") {
                cout << "BINGO!" << endl;
            }*/
            segment->markSeparatedByIntron();
        }

        destCluster->insertSegments( segmentsToMerge );
        toJoin->clear();
        //destCluster->update();
        //cerr << "Joined clutser " << toJoin->getIdx() << " to " << destCluster->getIdx() << endl;

    }
}


bool ClusterRegistryImpl::clustersWithinMaxIntronLength(const ClusterPtr& prevCluster, const ClusterPtr& curCluster) {

    const SimpleInterval& iv1 = prevCluster->getEncompassingInterval();
    const SimpleInterval& iv2 = curCluster->getEncompassingInterval();

    return ( iv2.begin > iv1.end  && iv1.end + MAX_INTRON_LENTH > iv2.begin  );
}



static inline bool haveCommonPartners(const ClusterPtr& cluster1, const ClusterPtr& cluster2) {

    const Cluster::PtrSet& s1 = cluster1->getUniquePartners();
    const Cluster::PtrSet& s2 = cluster2->getUniquePartners();

    set<ClusterPtr> res;
    std::set_intersection(s1.begin(), s1.end(), s2.begin(), s2.end(), std::inserter(res,res.begin()));

    return res.size() > 0;


}


bool ClusterRegistryImpl::haveCommonPartnersWithinIntronLength(const ClusterPtr& cluster1, const ClusterPtr& cluster2) {

    const Cluster::PtrSet& partners1 = cluster1->getUniquePartners();
    const Cluster::PtrSet& partners2 = cluster2->getUniquePartners();

    foreach_ (ClusterPtr p1, partners1) {

        if ( partners2.count(p1) != 0) {
            return true;
        } else if (p1->getNumLocalAlignments() == 0 && p1->getNumSegments() > 0) {

            ClusterPtr toTest;

            const IntervalTree<ClusterPtr>& iTree = clusterOverlapDetector.getOverlapper(p1->getRefId());
            auto iv = p1->getEncompassingInterval();
            auto node = iTree.overlap(iv.begin, iv.end);
            if (!node) {
                continue;
            }

            // TODO: have to go through all clusters, not only one step
            if (p1->getDirection() == Cluster::DirectionLeft) {
                auto nextNode = iTree.stepForward(node);
                if (nextNode != NULL ) {
                    ClusterPtr nextCluster = nextNode->getValue();
                    if ( nextCluster->getSegments().size() > 0 && clustersWithinMaxIntronLength(p1, nextCluster) ) {
                        toTest = nextCluster;
                    }
                }
            } else {
                assert(p1->getDirection() == Cluster::DirectionRight);
                auto prevNode = iTree.stepBack(node);
                if (prevNode != NULL) {
                    ClusterPtr prevCluster = prevNode->getValue();
                    if ( prevCluster->getSegments().size() > 0 &&  clustersWithinMaxIntronLength(prevCluster,p1) ) {
                        toTest = prevCluster;
                    }
                }

            }

            if ( toTest && ( partners2.count(toTest) != 0 ) ) {
                return true;
            }

        }


    }

    return false;

}


inline bool ClusterRegistryImpl::canBeMerged(const ClusterPtr& cluster1 , const ClusterPtr& cluster2 ) {
    return ( cluster1->getDirection() == cluster2->getDirection() ) && haveCommonPartnersWithinIntronLength(cluster1, cluster2);



}

bool ClusterRegistryImpl::performClusterOverCrossing(const ClusterPtr &targetCluster, const ClusterPtr &adjacentCluster)
{
    // adjacent cluster is closer to possible common breakpoint

    if (targetCluster->getDirection() != adjacentCluster->getDirection()) {
        return false;
    }

    const Cluster::PtrSet& partners1 = targetCluster->getUniquePartners();
    if (partners1.size() > 1) {
        return false;
    }

    ClusterPtr targetClusterPartner = *(partners1.begin());

    const Cluster::PtrSet& partners2 = adjacentCluster->getUniquePartners();
    if (partners2.size() > 1) {
        return false;
    }

    ClusterPtr adjacentClusterPartner = *(partners2.begin());

    if (targetClusterPartner->getNumLocalAlignments() > 0 || adjacentClusterPartner->getNumLocalAlignments() > 0) {
        return false;
    }

    if (targetClusterPartner->getDirection() != adjacentClusterPartner->getDirection()
            || targetClusterPartner->getRefId() != adjacentClusterPartner->getRefId()) {
        return false;
    }

    if (targetClusterPartner->getDirection() == Cluster::DirectionRight) {
        if (! clustersWithinMaxIntronLength(targetClusterPartner, adjacentClusterPartner) ) {
            return false;
        }
    } else {
        if (! clustersWithinMaxIntronLength(adjacentClusterPartner, targetClusterPartner)) {
            return false;
        }
    }

    joinClusterSeparatedByIntron(adjacentCluster, targetCluster);
    joinClusterSeparatedByIntron(targetClusterPartner, adjacentClusterPartner);

    return true;


}

void ClusterRegistryImpl::mergeIntronSepartedClusters()
{

    if (clusterOverlapDetector.getIntervalMap().size() == 0) {
        createClusterGenomicOverlapper();
    }


    const std::map<std::string,IntervalTree<ClusterPtr> > & intervalMap = clusterOverlapDetector.getIntervalMap();

    for (auto iter = intervalMap.begin(), endIter = intervalMap.end(); iter != endIter; ++iter ) {
        const IntervalTree<ClusterPtr>& clusterITree = iter->second;

        auto node = clusterITree.leftMostNode();

        while (node != NULL) {
            ClusterPtr curCluster = node->getValue();
            //cerr << "Looking at cluster: " << curCluster << endl;
            if (curCluster->getNumLocalAlignments() == 0 && curCluster->getSegments().size() > 0) {
                if (curCluster->getDirection() == Cluster::DirectionLeft) {
                    auto nextNode = clusterITree.stepForward(node);

                    while (nextNode != NULL ) {
                        ClusterPtr nextCluster = nextNode->getValue();
                        if (nextCluster->getNumSegments() > 0) {
                            if ( clustersWithinMaxIntronLength(curCluster,nextCluster) ) {
                                if ( canBeMerged(curCluster, nextCluster) ) {
                                    joinClusterSeparatedByIntron(nextCluster,curCluster);
                                    break;
                                }
                            } else {
                                break;
                            }
                        }
                        nextNode = clusterITree.stepForward(nextNode);
                    }

                } else {
                    assert(curCluster->getDirection() == Cluster::DirectionRight);
                    auto prevNode = clusterITree.stepBack(node);
                    while (prevNode != NULL ) {
                        ClusterPtr prevCluster = prevNode->getValue();
                        if (prevCluster->getNumSegments() > 0) {
                            if ( clustersWithinMaxIntronLength(prevCluster,curCluster) ) {
                                if ( canBeMerged(curCluster, prevCluster) ) {
                                    joinClusterSeparatedByIntron(prevCluster,curCluster);
                                    break;
                                }
                            } else {
                                break;
                            }
                        }
                        prevNode = clusterITree.stepBack(prevNode);
                    }

                }

            }
            node = clusterITree.stepForward(node);
        }

    }

}

void ClusterRegistryImpl::mergeCrossingOverClusters()
{
    if (clusterOverlapDetector.getIntervalMap().size() == 0) {
        createClusterGenomicOverlapper();
    }


    const std::map<std::string,IntervalTree<ClusterPtr> > & intervalMap = clusterOverlapDetector.getIntervalMap();

    for (auto iter = intervalMap.begin(), endIter = intervalMap.end(); iter != endIter; ++iter ) {
        const IntervalTree<ClusterPtr>& clusterITree = iter->second;

        auto node = clusterITree.leftMostNode();

        while (node != NULL) {
            ClusterPtr curCluster = node->getValue();
            //cerr << "Looking at cluster: " << curCluster << endl;
            if (curCluster->getNumLocalAlignments() == 0 && curCluster->getNumSegments() > 0) {
                if (curCluster->getDirection() == Cluster::DirectionLeft) {
                    auto nextNode = clusterITree.stepForward(node);

                    while (nextNode != NULL ) {
                        ClusterPtr nextCluster = nextNode->getValue();
                        if (nextCluster->getNumSegments() > 0 && nextCluster->getNumLocalAlignments() == 0) {
                            if ( clustersWithinMaxIntronLength(curCluster,nextCluster) ) {
                                if (performClusterOverCrossing(curCluster, nextCluster)) {
                                    break;
                                }
                            } else {
                                break;
                            }
                        }
                        nextNode = clusterITree.stepForward(nextNode);
                    }

                } else {
                    assert(curCluster->getDirection() == Cluster::DirectionRight);
                    auto prevNode = clusterITree.stepBack(node);
                    while (prevNode != NULL ) {
                        ClusterPtr prevCluster = prevNode->getValue();
                        if (prevCluster->getNumSegments() > 0 && prevCluster->getNumLocalAlignments() == 0) {
                            if ( clustersWithinMaxIntronLength(prevCluster,curCluster) ) {
                                if ( performClusterOverCrossing(curCluster, prevCluster) ) {
                                    break;
                                }
                            } else {
                                break;
                            }
                        }
                        prevNode = clusterITree.stepBack(prevNode);
                    }

                }

            }
            node = clusterITree.stepForward(node);
        }

    }


}


/*static void joinIntersectingClusters(ClusterPtr destCluster, ClusterPtr clusterToJoin) {

    const vector<ClusterSegmentPtr>& destSegments = destCluster->getSegments();
    const vector<ClusterSegmentPtr>& segmentsToJoin = clusterToJoin->getSegments();


    destCluster->mergeIntersecting(clusterToJoin);

    set<BreakpointCandidatePtr> destBpSet;

    foreach_ (const ClusterSegmentPtr& s , destSegments) {
        destBpSet.insert(s->getBreakpointCandidate());
    }


    foreach_ (ClusterSegmentPtr segment , segmentsToJoin ) {

        if (destBpSet.count( segment->getBreakpointCandidate()) == 0) {
            segment->setParentCluster(destCluster);
            destCluster->addSegment(segment);
        }


    }

    destCluster->update();
    clusterToJoin->clear();

}*/

void ClusterRegistryImpl::mergeIntersectingClusters()
{
    cerr << "Merging intersecting clusters..." << endl;

    if (clusterOverlapDetector.getIntervalMap().size() == 0) {
        createClusterGenomicOverlapper();
    }


    const std::map<std::string,IntervalTree<ClusterPtr> > & intervalMap = clusterOverlapDetector.getIntervalMap();

    for (auto iter = intervalMap.begin(), endIter = intervalMap.end(); iter != endIter; ++iter ) {
        const IntervalTree<ClusterPtr>& clusterITree = iter->second;

        auto node = clusterITree.leftMostNode();

        while (node != NULL) {
            ClusterPtr curCluster = node->getValue();
            //cerr << "Looking at cluster: " << curCluster << endl;
            /*if (curCluster->getIdx() == 4287) {
                cerr << "BINGO!" << endl;
            }*/
            if (curCluster->getNumSegments() > 0) {
                   const SimpleInterval& curIv = curCluster->getEncompassingInterval();
                   auto nextNode = clusterITree.stepForward(node);

                   while (nextNode != NULL ) {
                        ClusterPtr nextCluster = nextNode->getValue();
                        const SimpleInterval& nextIv = nextCluster->getEncompassingInterval();

                        if (!curIv.intersects(nextIv)) {
                            break;
                        }

                        if (nextCluster->getNumSegments() > 0 && nextCluster->getDirection() == curCluster->getDirection()) {

                            int bpDiff = abs(nextCluster->getBreakpointPos() - curCluster->getBreakpointPos());

                            if (bpDiff <= config.fuzzyBreakpointTolerance ) {
                                Cluster::merge(curCluster, nextCluster);
                            }
                        }
                        nextNode = clusterITree.stepForward(nextNode);

                    }

              }

            node = clusterITree.stepForward(node);
        }

    }


}


void ClusterRegistryImpl::splitClusterByBreakpoint()
{

    cerr << "Splitting clusters by breakpoint..." << endl;

    vector<ClusterPtr> newClusters;
    int index = clusters.size();
    foreach_ (ClusterPtr& cluster, clusters) {
        //cerr << "Splliting cluster " << cluster << endl;
        //foreach_ (const ClusterSegmentPtr& seg, cluster->getSegments()) {
            //cerr << seg << endl;
        //}
        cluster->separateByBreakpoint(newClusters, index);
    }

    cerr << "Created additional " << newClusters.size() << " clusters" << endl;

    clusters.insert(clusters.end(), newClusters.begin(), newClusters.end() );
}



void ClusterRegistryImpl::cleanupRescuedClusters()
{
    foreach_ (ClusterPtr& cl, clusters ) {


        const vector<ClusterSegmentPtr>& segments = cl->getSegments();
        vector<ClusterSegmentPtr> toRemove;
        int numLocalNotRedeemed = 0;
        int clusterBpPos = cl->getBreakpointPos();

        foreach_ (ClusterSegmentPtr seg, segments) {

            BreakpointCandidatePtr bp = seg->getBreakpointCandidate();

            if (bp->isRedeemed()) {
                const QueryAlignment& aln = seg->getAlignment();
                int bpPos = aln.getPointClosestToBreakpoint(cl->getDirection() == Cluster::DirectionRight);
                if (abs(bpPos - clusterBpPos) < config.fuzzyBreakpointTolerance) {
                    toRemove.push_back(seg);
                }
            } else {
                if (bp->isOriginLocal()) {
                    numLocalNotRedeemed += 1;
                }
            }

        }


        if (numLocalNotRedeemed == 0) {

            foreach_ (const ClusterSegmentPtr& seg, toRemove) {
                ClusterSegmentPtr partner = seg->getPartner();
                ClusterPtr partnerParent = partner->getParentCluster();
                partnerParent->removeSegment(partner);
                cl->removeSegment(seg);

            }

        }



    }

}

void ClusterRegistryImpl::outputFusions(const string &outFileName)
{

    if (!loadedClusterSequences && config.referenceSeqAvailable) {
        bool ok = loadClusterRegionSequences( 100 );
        if (!ok) {
            cerr << "Failed to load cluster sequences" << endl;
            return;
        }

    }

    if (config.mergeIntronSeparated && config.pairedReads) {
        cerr << "Merging clusters separated by intron length..." << endl;
        mergeIntronSepartedClusters();
    }

    if (config.mergeIntersecting) {
        //cleanupRescuedClusters();

    #ifdef DEBUG
        string clustersFolder = boost::filesystem::absolute(outFileName).parent_path().string() + "/clusters_after_cleanup";
        dumpClusters(clustersFolder);
    #endif

        mergeIntersectingClusters();
    }


#ifdef DEBUG
    {
        string clustersFolder = boost::filesystem::absolute(outFileName).parent_path().string() + "/clusters_after_merging";
        dumpClusters(clustersFolder);
    }
#endif

    // Free some memory
    clusterOverlapDetector.clear();

    cerr << "Creating putative fusions..." << endl;

    ofstream out;
    out.open(outFileName, ios_base::out);

    if (!out.is_open()) {
        return;
    }

    size_t countIndex = 1;

    foreach_(ClusterPtr cluster, clusters) {
        std::map<ClusterPair,Fusion> clusterPairMap;

        foreach_(ClusterSegmentPtr segment, cluster->getSegments()) {
            if (segment->isProcessed()) {
                continue;
            }

            const ClusterSegmentPtr& partner = segment->getPartner();

            ClusterPair idx = getFusionMapIdx(segment, partner);

            if (clusterPairMap.count(idx) == 0) {
                vector<FusionInterval> intervals;
                //TODO: The breakpoint has to be recalcualted based on the segments that are taking part in it!

                FusionInterval iv1( segment->getParentCluster() );
                FusionInterval iv2( partner->getParentCluster() );

                if (segment->isFirst()) {
                    intervals.push_back(iv1);
                    intervals.push_back(iv2);
                } else {
                    intervals.push_back(iv2);
                    intervals.push_back(iv1);
                }

                if (LibraryProtocol::StrandSpecificReverse == config.protocol) {
                    reverse(intervals.begin(), intervals.end());
                }


                clusterPairMap[idx].setIntervals(intervals);
            }

            const Fusion& f = clusterPairMap.at(idx);
            segment->getBreakpointCandidate()->sortAlignmentsAccordingToInterval(f.getEncompassingInterval(0));

            clusterPairMap[idx].addBreakpoint(segment->getBreakpointCandidate());
            segment->setProcessed();
            partner->setProcessed();
        }

        for(auto iter = clusterPairMap.begin(); iter != clusterPairMap.end(); ++iter)
        {
            Fusion& fusion = iter->second;
            fusion.updateIntervals(true);
            fusion.sortSupportersByName();
            fusion.setIndex(countIndex);
            Fusion::writeSingleFusionCluster(fusion, out);
            ++countIndex;
        }

        cluster->clear();

    }

    clusters.clear();


}



void ClusterRegistryImpl::findSupporters(const vector<ReadLocalAlignment>&alns, int numAlns, const FindSupportingReadsConfig& cfg)
{

    if (clusterOverlapDetector.getIntervalMap().size() == 0) {
        createClusterGenomicOverlapper();
    }

    if (!loadedClusterSequences) {
        PerfTimer t1;
        cerr << "Loading cluster sequences..." << endl;
        bool ok = loadClusterRegionSequences( cfg.additionalReserveSize );
        if (!ok) {
            cerr << "Failed to load cluster sequences" << endl;
            return;
        }
        loadedClusterSequences = true;
        t1.drop();
        cerr << "Time loading the seuqences: " << t1.getElapsed() << endl;

    }


    findSupportersParallel(alns, numAlns, cfg);

}


void ClusterRegistryImpl::findSupportersParallel(const vector<ReadLocalAlignment> &alns, int numAlns, const FindSupportingReadsConfig &cfg)
{

    int numCores = cfg.numCores;

    if (numCores > numAlns) {
        numCores = 1;
    }

    vector<ResultStore*> resultStores;

    boost::thread_group group;

    int numTasksPerThread = numAlns / numCores;
    int taskCount = 0;
    for ( int idx  = 0; idx < numAlns; idx += numTasksPerThread) {
        int upperIdx = idx + numTasksPerThread > numAlns ? numAlns - 1 : idx + numTasksPerThread - 1;
#ifdef DEBUG
        cerr << "Task " << taskCount++ << " range: [" << idx << "," << upperIdx << "]" << endl;
#endif
        ResultStore* store = new ResultStore();
        resultStores.push_back(store);
        RescueLocalAlignmentsTask task(alns, clusterOverlapDetector, cfg, idx, upperIdx, store);
        group.create_thread(task);
    }

    group.join_all();

    set<ClusterPtr> toUpdate;

    foreach_ (ResultStore* store, resultStores) {

        for (size_t i = 0, sz = store->size(); i < sz; ++i) {

            const RescueAlignmentsTaskResult& r = store->at(i);

            ClusterSegmentPtr seg1( new ClusterSegment(r.sourceCluster, r.bp, r.sourceIdx) );
            r.sourceCluster->addSegment(seg1);
            toUpdate.insert(r.sourceCluster);

            ClusterSegmentPtr seg2( new ClusterSegment(r.partnerCluster, r.bp, r.queryIdx) );
            r.partnerCluster->addSegment(seg2);
            toUpdate.insert(r.partnerCluster);

            seg1->setPartner(seg2);
            seg2->setPartner(seg1);

            seg1->getLocalIdx() == 0 ?  seg1->setFirst() : seg2->setFirst();

        }

        store->clear();
        delete store;
    }

    // Make sure the sequences are updated in main thread
    foreach_ ( ClusterPtr c, toUpdate) {
        c->update();
    }

}


void ClusterRegistryImpl::createClusterGenomicOverlapper()
{
    clusterOverlapDetector.clear();
    foreach_ (ClusterPtr& cluster, clusters) {
        const SimpleInterval& cIv = cluster->getEncompassingInterval();
        clusterOverlapDetector.insertInterval(cluster->getRefId(), cIv.begin, cIv.end, cluster);
    }

}

bool ClusterRegistryImpl::loadClusterRegionSequences(int additionalReserveSize)
{

    seqan::CharString seq;
    string curRefName;
    int numSeqLoaded = 0;

    const std::map<std::string,IntervalTree<ClusterPtr> > & intervalTreeMap = clusterOverlapDetector.getIntervalMap();

    for(auto iter = intervalTreeMap.begin(); iter != intervalTreeMap.end(); ++iter) {
        string refName = iter->first;
        if (refName != curRefName) {
            if (!GlobalContext::getGlobalContext()->getGenomeReference()->getWholeSequence(seq, refName)) {
                cerr << "Skip loading sequences for clusters from " << refName << endl;
                continue;
            }
            curRefName = refName;
        }


        const IntervalTree<ClusterPtr>& iTree = iter->second;
        auto node = iTree.leftMostNode();

        while (node != NULL) {

            ClusterPtr cluster = node->getValue();
            if (cluster->getSegments().size() == 0) {
                node = iTree.stepForward(node);
                continue;
            }
            const SimpleInterval& clusterInterval = cluster->getEncompassingInterval();
            if (clusterInterval.begin == -1) {
                node = iTree.stepForward(node);
                continue;
            }

            int seqStart = clusterInterval.begin - additionalReserveSize;
            if (seqStart < 0) {
                seqStart = 0;
            }

            /*if (cluster->getIdx() == 4287) {
                cerr << "BINGO!" << endl;
            }*/

            // TODO: set limits to this thing, at least introns should be skipped
            size_t seqEnd = clusterInterval.end + additionalReserveSize  + 1;
            if (seqEnd > length(seq)) {
                cerr << "WARNING: cluster " << cluster->getIdx() << " is exceeding chromosome size! Skip sequence loading..." << endl;
                node = iTree.stepForward(node);
                continue;
                //seqEnd = length(seq);
            }

            auto subseqSegment = seqan::infix( seq, seqStart, seqEnd);
            seqan::CharString subseq(subseqSegment);
            assert (length(subseq) == seqEnd - seqStart);
            cluster->setSequence(subseq, clusterInterval.begin, clusterInterval.length(), seqStart);
            numSeqLoaded++;
            node = iTree.stepForward(node);
        }
    }

    cerr << "Loaded sequences for " << numSeqLoaded << " clusters." << endl;

    return true;

}

void ClusterRegistryImpl::dumpClusters(const string &pathToFolder)
{

    boost::filesystem::path dirPath = boost::filesystem::absolute(pathToFolder);
    cerr << "Folder to save clusters: " << dirPath << endl;

    if (!boost::filesystem::exists(dirPath)) {
        if (!boost::filesystem::create_directory(dirPath)) {
            cerr << "Failed to create directory " << pathToFolder << endl;
            return;
        }
    }

    const std::map<std::string,IntervalTree<ClusterPtr> > & intervalTreeMap = clusterOverlapDetector.getIntervalMap();

    for(auto iter = intervalTreeMap.begin(); iter != intervalTreeMap.end(); ++iter) {
        const string& refName = iter->first;
        string path = pathToFolder + "/" + refName + ".clusters";
        dumpClusters(path, refName, true);
    }

}

void ClusterRegistryImpl::dumpClusters(const string& fileName, const string &refName, bool dumpSegments)
{
    ofstream out;
    out.open(fileName, ios_base::out);

    out << "Clusters from chromosome " << refName << endl;

    const std::map<std::string,IntervalTree<ClusterPtr> > & intervalMap = clusterOverlapDetector.getIntervalMap();

    const IntervalTree<ClusterPtr>& clusterITree = intervalMap.at(refName);

    auto node = clusterITree.leftMostNode();
    while (node != NULL ) {
        ClusterPtr cl = node->getValue();
        auto iv = cl->getEncompassingInterval();
        out << cl->getIdx() << "\t" << iv.begin << "\t" << iv.end << "\t" << cl->getSegments().size() << endl;
        if (dumpSegments) {
            const vector<ClusterSegmentPtr>& segments = cl->getSegments();
            map<int,int> clusterIds;

            out << endl << "i ";
            for (int i = 0; i < iv.length(); ++i) {
                out << "*";
            }
            out << endl;

            vector<CharString> readNames;

            foreach_(ClusterSegmentPtr segment,  segments) {
                if (segment->getBreakpointCandidate()->isSeparatedByIntron()) {
                    continue;
                }
                clusterIds [segment->getPartner()->getParentCluster()->getIdx()] ++;
                const QueryAlignment& aln = segment->getAlignment();
                assert( iv.begin <= aln.refStart);
                int startPos = aln.refStart - iv.begin;
                readNames.push_back(segment->getBreakpointCandidate()->readName);
                if (segment->getBreakpointCandidate()->isOriginLocal()) {
                    if (segment->getBreakpointCandidate()->isRedeemed()) {
                        out << "r";
                    } else {
                        out << "l";
                    }
                } else if (segment->getBreakpointCandidate()->isOriginPaired()) {
                    out << "p ";
                } else {
                    out << "u ";
                }
                for (int i = 0; i < iv.length(); ++i) {
                    if (i >= startPos && i < startPos + aln.length) {
                        out << "*";
                    } else {
                        out << " ";
                    }
                }
                out << endl;

            }
            out << endl;

            out << "Partner clusters: ";
            for (auto iter = clusterIds.begin(); iter != clusterIds.end(); ++iter) {
                out << "{" << iter->first << ":" << iter->second << "}, ";
            }
            out << endl;

            out << "Read names :";
            foreach_ (const CharString& readName, readNames){
                out << readName << ",";
            }

            out << endl << endl;
        }

        node = clusterITree.stepForward(node);

    }
    out << "Finished clusters from chromosome " << refName << endl << endl;
}





