#include <iostream>
#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>

#include "Global.h"

using namespace std;


class Task
{
    string message;

    const vector<int>& array;
    int start,end;
    double* result;
    static boost::mutex guard;
public:
    Task(const vector<int>& a, int startIdx, int endIdx, double* r) : array(a), start(startIdx), end(endIdx), result(r)  {}

    void operator()() {
        run();
    }

    void run() {
        cout << "Hello I am working" << endl;
        double sum = 0;
        for (int i = start; i <= end; ++i) {
            float ms = 1e3;
            boost::posix_time::milliseconds workTime(ms);
            //boost::this_thread::sleep(workTime);
            sum += sqrt(array.at(i))*sqrt(array.at(i));
        }

        boost::mutex::scoped_lock scoped_lock(guard);
        *result += sum;
    }


};

boost::mutex Task::guard;

int main(int argc, char const ** argv)
{

#ifdef DEBUG

    cout << "Running in DEBUG mode" << endl;

#else
    cout << "Running in RELEASE mode" << endl;

#endif //DEBUG


    int numCores = 8;//boost::thread::hardware_concurrency() << endl;
    cout << "Number of cores: " << numCores << endl;

    vector<int> array;
    const int N = 100000;
    for (int i = 0; i < N; ++i ) {
        array.push_back( i );
    }


    cout << "Running in single thread" << endl;
    double sum = 0;
    Task singleThreadedTask(array, 0, N-1, &sum);
    singleThreadedTask.run();
    cout << "Single thread result: " << sum << endl << endl;


    cout << "Running in multiple threads" << endl;
    boost::thread_group group;

    double multiSum = 0;
    int numTasksPerThread = N / numCores;
    for ( int idx  = 0; idx < N; idx += numTasksPerThread) {
        int upperIdx = idx + numTasksPerThread > N ? N - 1 : idx + numTasksPerThread - 1;
        Task task(array, idx, upperIdx, &multiSum);
        //group.create_thread(task);
        task.run();
    }

    group.join_all();

    cout << "Threaded result: " <<  multiSum << endl;




}
