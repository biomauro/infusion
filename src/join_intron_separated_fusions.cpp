#include <iostream>
#include <vector>

#include <seqan/arg_parse.h>

#include "MergeIntronSeparatedFusionsTask.h"

using namespace seqan;
using namespace std;

#define PROGRAM_NAME    "join_intron_separated_fusions"

void setupArgumentParser( ArgumentParser& parser )
{
    setVersion(parser, INFUSION_VERSION);


    addDescription(parser, "This tool is part of the InFusion pipeline. It searchs for fusions consisting only from paired alignments \
                   and tries to join it with other clusters within max intron distance.");

    string usageLine;
    usageLine.append(" [OPTIONS] -i fusionClusters.txt -o updatedClusters.txt");
    addUsageLine(parser, usageLine);

    addSection(parser, "Options");

    addOption(parser, ArgParseOption( "i", "input", "Path to input fusion clusters. Use - to for standard input.", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "o", "output", "Path to output updated fusion clusters. Use - to for standard output", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "x", "max-intron-length", "Maximum intron size to consider. Default: 20000", ArgParseArgument::INTEGER) );
    addOption(parser, ArgParseOption( "m", "mean-insert-size", "If a joined alignment is in the range of mean insert size it is considered as a part of the cluster.\
                                      Otherwise alignment is separated by intron. Default: 25", ArgParseArgument::INTEGER) );


    addTextSection(parser, "References");
    addText(parser, "Konstantin Okonechnikov <okonechnikov@mpiib-berlin.mpg.de>");
}

int main(int argc, char const ** argv)
{

    ArgumentParser parser(PROGRAM_NAME);
    setupArgumentParser(parser);

    ArgumentParser::ParseResult res = parse(parser, argc, argv);
    if (res != ArgumentParser::PARSE_OK) {
        return 1;
    }

    MergeIntronSeparatedFusionsTaskConfig cfg;

    getOptionValue(cfg.inputPath, parser, "i");
    getOptionValue(cfg.outputPath, parser, "o");
    getOptionValue(cfg.maxIntronLength, parser, "x");
    getOptionValue(cfg.meanInsertSize, parser, "m");

#ifdef DEBUG
    cerr << "Options" << endl;
    cerr << "Input file: " << cfg.inputPath << endl;
    cerr << "Output file: " << cfg.outputPath << endl;
    cerr << "Max intron length: " << cfg.maxIntronLength << endl;
    cerr << "Mean insert size: " << cfg.meanInsertSize << endl;
#endif

    MergeIntronSeparatedFusionsTask mergeTask(cfg);

    return mergeTask.run();

}




