#include <iostream>
#include <vector>

#include <iostream>
#include <seqan/arg_parse.h>
#include <seqan/sequence.h>
#include <seqan/seq_io.h>

using namespace seqan;
using namespace std;

#define PROGRAM_NAME    "compute_read_stats"


void setupArgumentParser( ArgumentParser& parser )
{
    setVersion(parser, INFUSION_VERSION);


    addDescription(parser, "This tool is part of the InFusion pipeline. It outputs the read statistics: total number of reads, min and max read length.");

    string usageLine;
    usageLine.append("-i [INPUT_READS] -o [OUTPUT]");
    addUsageLine(parser, usageLine);

    addSection(parser, "Options");

    addOption(parser, ArgParseOption("i", "input-reads", "Path to input reads.", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "o", "output", "Path to output file with reads statistic", ArgParseArgument::STRING ));

    addTextSection(parser, "References");
    addText(parser, "Konstantin Okonechnikov <okonechnikov@mpiib-berlin.mpg.de>");
}



int performAnalysis(string inputReadsPath, string outputPath) {

    long numReads = 0;
    int minSize = INT_MAX, maxSize = 0;

    seqan::CharString id;
    seqan::Dna5String seq;
    seqan::SequenceStream seqStream(inputReadsPath.c_str());

    if (!isGood(seqStream))
    {
        std::cerr << "ERROR! Could not open the file: " << inputReadsPath << endl;
        return 1;
    }


    while (!atEnd(seqStream))
    {
        if (readRecord(id, seq, seqStream) != 0)
        {
            std::cerr << "ERROR! Could not read from file: " << inputReadsPath << endl;
            return 1;
        }

        numReads += 1;
        int sz = length(seq);

        if (sz < minSize) {
            minSize = sz;
        }

        if (sz > maxSize) {
            maxSize = sz;
        }

    }


    ofstream outStream(outputPath);

    if (!outStream.is_open()) {
        cerr << "ERROR! Failed to open file " << outputPath << " for writing" << endl;
        return 1;
    }

    outStream << "N=" << numReads << ", ";
    outStream << "min_size=" << minSize << ", ";
    outStream << "max_size=" << maxSize;

    return 0;

}



int main(int argc, char const ** argv)
{

    ArgumentParser parser(PROGRAM_NAME);
    setupArgumentParser(parser);

    ArgumentParser::ParseResult res = parse(parser, argc, argv);
    if (res != ArgumentParser::PARSE_OK) {
        return 1;
    }

    string inputPath, outputPath;

    getOptionValue(inputPath, parser, "i");
    getOptionValue(outputPath, parser, "o");

    if (outputPath.length() == 0) {
        cerr << "The output file is not set!" << endl;
        return -1;
    }

    if (inputPath.length() == 0) {
        cerr << "The input BAM file is not set!" << endl;
        return -1;
    }

#ifdef DEBUG
    cerr << "Options" << endl;
    cerr << "Input fusions file: " << inputPath << endl;
    cerr << "Output alignment file: " << outputPath << endl;
#endif

    return performAnalysis(inputPath, outputPath);


}




