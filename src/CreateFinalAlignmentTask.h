#ifndef CREATE_FINAL_ALIGNMENT_TASK_H
#define CREATE_FINAL_ALIGNMENT_TASK_H

#include <seqan/bam_io.h>

#include "Global.h"
#include "AnnotationModel.h"
#include "Fusion.h"

struct CreateFinalAlignmentTaskConfig {

    string transcriptomicAlignmentsPath;
    vector<string> genomicAlignmentsPath;
    vector<string> localAlignmentsPath;
    string fusionsPath;
    string annotationsPath;
    string headerBamPath;
    string outputPath;
    bool outputReadClusters, useSamFormat;

    CreateFinalAlignmentTaskConfig() : outputReadClusters(true), useSamFormat(false) {}

};

class CreateFinalAlignmentTask
{
    CreateFinalAlignmentTaskConfig cfg;
    AnnotationModel geneModels;
    seqan::BamStream outBamStream;
    vector<Fusion> fusions;
    map<seqan::CharString,int> fusionReadMap;
    void convertToGenomic(seqan::BamAlignmentRecord& rec, const TranscriptPtr& transcript, map<string,int>& refIdMap);
public:
    CreateFinalAlignmentTask(const CreateFinalAlignmentTaskConfig& config) : cfg(config) {}
    bool init();
    int run();
    bool writeHeader();
    bool writeTranscriptomicAlignments();
    bool writeLocalAlignments(const string& alignmentPath, const char* mateId);
};

#endif // CREATE_FINAL_ALIGNMENT_TASK_H
