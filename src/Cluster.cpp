#include "Global.h"
#include "Cluster.h"

#include <math.h>

using std::cerr;
using std::endl;

int Cluster::insertSizeTolerance = 50;
int Cluster::maxFuzzyBreakpointDistance = 20;

void Cluster::update() const
{
    vector<int> localAlnPos;

    //cerr << "Updating cluster " << idx << ", direcion: " << directionToChar(dir) << endl;
    interval.begin = -1;
    interval.end = 0;
    numLocalAlignments = 0;
    uniquePartners.clear();
    int numProcessedSegments = 0;

    foreach_(const ClusterSegmentPtr& segment, segments) {

        if (segment->isProcessed()) {
            ++numProcessedSegments;
            continue;
        }

        const QueryAlignment& aln = segment->getAlignment();
        //cerr << "Using bp: " << segment->getBreakpointCandidate()->readName << endl;

        ClusterPtr partner = segment->getPartner()->getParentCluster();
        //cerr << "Partner is " << partner->getIdx() << " " << partner.get() << endl;
        uniquePartners.insert(partner);

        if (segment->isSeparatedByIntron()) {
            continue;
        }

        if (interval.begin == -1) {
            interval.begin = aln.refStart;
            interval.end = aln.inRefEnd();
        } else {
            //assert(aln.refId == refId);
            interval.begin = aln.refStart < interval.begin ? aln.refStart : interval.begin;
            interval.end = aln.inRefEnd() > interval.end ? aln.inRefEnd() : interval.end;
        }

        assert( dir == Cluster::inferDirection( aln.positiveStrand, segment->getLocalIdx() == 0 ) );
        if (aln.refId != refId) {
            cerr << "Bad read: " << segment->getBreakpointCandidate()->readName << endl;
        }
        assert( aln.refId == refId);
        bPos = dir == DirectionRight ? interval.begin : interval.end;
        if (segment->getBreakpointCandidate()->isOriginLocal()) {
            localAlnPos.push_back( dir == DirectionRight ? aln.refStart : aln.inRefEnd() );
        }

        if (segment->getBreakpointCandidate()->isOriginLocal()) {
            numLocalAlignments += 1;
        }

    }

    if (localAlnPos.size() > 2) {
        // We use median for fuzzy breakpoint estimation
        if (dir == DirectionRight) {
            std::sort(localAlnPos.begin(), localAlnPos.end(), std::less<int>()  );
        } else {
            std::sort(localAlnPos.begin(), localAlnPos.end(), std::greater<int>()  );
        }
        bPos = localAlnPos[ localAlnPos.size() / 2 - 1 ];
    }


    if (numProcessedSegments == 0 &&  numLocalAlignments == 0 && interval.begin != -1) {
        if (dir == DirectionLeft) {
            interval.end += insertSizeTolerance;
        } else {
            interval.begin -= insertSizeTolerance;
            if (interval.begin < 0) {
                interval.begin = 0;
            }
        }
    }

    updateSequence(interval.begin, interval.length());

    uptodate = true;

}

#define FUZZY_BREAKPOINT_TOLERANCE 7

void Cluster::separateByBreakpoint(vector<ClusterPtr>& results, int& clusterIndex)
{

    update();

    if (segments.size() == 1 || numLocalAlignments == 0) {
        return;
    }

    int newBreakpointPos = -1, minDistanceToBreakpoint = interval.length();
    int intervalBreakpointPos = dir == DirectionRight ? interval.begin : interval.end;

    foreach_(const ClusterSegmentPtr& segment, segments) {

        //cerr << "Using bp: " << segment->getBreakpointCandidate()->readName << endl;

        if (!segment->getBreakpointCandidate()->isOriginLocal() ) {
            continue;
        }

        const QueryAlignment& aln = segment->getAlignment();
        int curBPos = dir == DirectionRight ? aln.refStart : aln.inRefEnd();
        int distanceToBreakpoint = abs(curBPos - intervalBreakpointPos);
        if (distanceToBreakpoint > maxFuzzyBreakpointDistance &&  distanceToBreakpoint < minDistanceToBreakpoint ) {
            newBreakpointPos = curBPos;
            minDistanceToBreakpoint = distanceToBreakpoint;
        }

    }

    if (newBreakpointPos != -1) {

        ClusterPtr newCluster( new Cluster(clusterIndex++,refId, dir) );

        vector<ClusterSegmentPtr> newSegmentList;

        foreach_(const ClusterSegmentPtr& segment, segments) {

            const QueryAlignment& aln = segment->getAlignment();

            if ( ( dir == DirectionRight && ( (newBreakpointPos - FUZZY_BREAKPOINT_TOLERANCE) <= aln.refStart) ) ||
                    (dir == DirectionLeft && ( newBreakpointPos + FUZZY_BREAKPOINT_TOLERANCE >= aln.inRefEnd() )) )
            {
                segment->setParentCluster(newCluster);
                newCluster->addSegment(segment);
            } else {
                newSegmentList.push_back(segment);
            }


        }

        segments = newSegmentList;
        uptodate = false;
        results.push_back(newCluster);
        assert(newCluster->getNumSegments() > 0);
        /*if (idx == 34 || idx == 6) {
            foreach_ (const ClusterSegmentPtr& seg, segments) {
                cerr << seg << endl;
            }
        }*/
        newCluster->separateByBreakpoint(results,clusterIndex);

    }



}

void Cluster::merge(ClusterPtr &dest, ClusterPtr& other)
{
    using namespace seqan;

    const SimpleInterval& iv1 = dest->getEncompassingInterval();
    const SimpleInterval& iv2 = other->getEncompassingInterval();

    if ( !iv1.intersects(iv2) ) {
        return;
    }

    const vector<ClusterSegmentPtr>& destSegments = dest->getSegments();
    std::set<CharString> destReadSet;

    foreach_ (const ClusterSegmentPtr& s , destSegments) {
        destReadSet.insert(s->getBreakpointCandidate()->readName);
    }

    const vector<ClusterSegmentPtr>& segmentsToJoin = other->getSegments();

    foreach_ (ClusterSegmentPtr segment , segmentsToJoin ) {
        //cerr << segment << endl;
        if (destReadSet.count(segment->getBreakpointCandidate()->readName) == 0) {
            segment->setParentCluster(dest);
            dest->addSegment(segment);
        } else {
            ClusterSegmentPtr partner = segment->getPartner();
            ClusterPtr partnerParent = partner->getParentCluster();
            partnerParent->removeSegment(partner);
        }
    }

    if (iv1.contains(iv2)) {
        // do nothing
    } else if (iv2.contains(iv1)) {
        dest->sequenceData = other->sequenceData;
        dest->sequenceOffset = other->sequenceOffset;
    } else {


        //TODO: test this code! see at playgrnd/test_sequence2

        int seq1Start = dest->sequenceOffset;
        int seq1End = dest->sequenceOffset + length(dest->sequenceData);
        int seq2Start = other->sequenceOffset;
        int seq2End = other->sequenceOffset + length(other->sequenceData);

        CharString leftSubSequence;  // includes common part
        //int commonPartLen = std::min(iv1.end, iv2.end) - std::max(iv1.begin, iv2.begin)  +1;
        int commonPartLen = std::min(seq1End, seq2End) - std::max(seq1Start, seq2Start)  +1;

        /*if (iv1.begin < iv2.begin) {
            leftSubSequence = prefix(dest->sequenceData,iv2.begin - iv1.begin + commonPartLen);
        } else {
            leftSubSequence = prefix(other->sequenceData, iv1.begin - iv2.begin + commonPartLen);
            dest->sequenceOffset = other->sequenceOffset;
        }*/

        if (seq1Start < seq2Start) {
            leftSubSequence = prefix(dest->sequenceData, seq2Start - seq1Start + commonPartLen);
        } else {
            leftSubSequence = prefix(other->sequenceData, seq1Start - seq2Start + commonPartLen);
            dest->sequenceOffset = other->sequenceOffset;
        }


        CharString rightSubSequence;
        if (seq1End > seq2End) {
            rightSubSequence = suffix(dest->sequenceData, seq2End - seq1Start );
        } else {
            rightSubSequence = suffix(other->sequenceData, seq1End - seq2Start );
        }

        dest->sequenceData = leftSubSequence;
        dest->sequenceData += rightSubSequence;

    }

    dest->update();

    other->clear();


}

void Cluster::updateSequence(int intervalStart, int intervalLen) const
{

    if (intervalStart == -1) {
        seqan::clear(clusterSubseq);
        return;
    }

    /*if (idx == 1 || idx == 476159 || idx == 476160 || idx == 476162) {
        cerr << endl <<"Cluster " << idx << "before updating sequence" << endl;
        cerr << "[start=" << interval.begin << ",";
        cerr << "end=" << interval.end << ",";
        cerr << "n=" << segments.size() << ",";
        cerr <<  "]" << endl;
        cerr << "seq=" << clusterSubseq << endl;
        cerr << "intervalStart=" << intervalStart << ",intervalLen="<< intervalLen << endl << endl;
    }*/


    int seqLen = length(sequenceData);
    if ( seqLen > 0) {
        int startPos = intervalStart - sequenceOffset;
        //assert(startPos >= 0 && startPos + intervalLen < seqLen);
        if (startPos < 0 ) {
            cerr << "ERROR! Failed to update sequence: start position is less than zero. clusterId=" <<
                    idx << ",startPos=" << startPos << endl;
            seqan::clear(clusterSubseq);
            return;
        }
        if (startPos + intervalLen >= seqLen) {
            cerr << "ERROR! Failed to update sequence: end pos is larger than sequence end. clusterId=" <<
                    idx << ",seqLen=" << seqLen << ",endPos=" << startPos + intervalLen << endl;
            //cerr << "Cluster: " << interval.begin << "-" << interval.end << endl;
            //debugOutput(this);
            //exit(-1);
            seqan::clear(clusterSubseq);
            return;
        }
        auto seq = infix(sequenceData, startPos, startPos + intervalLen );
        clusterSubseq = CharString(seq);
        assert( length(clusterSubseq) == intervalLen );
    }


}

bool Cluster::canAddSegment(const ClusterSegmentPtr &segment)
{
    foreach_(const ClusterSegmentPtr& segment, segments) {

        const QueryAlignment& aln = segment->getAlignment();
        if (aln.refId != refId) {
            return false;
        }
        if ( dir != Cluster::inferDirection( aln.positiveStrand, segment->getLocalIdx() == 0 ) ) {
            return false;
        }
    }

    return true;

}


void Cluster::setSequence(const CharString& seq, int intervalStart, int intervalLen, int offset) {
    sequenceData = seq;
    sequenceOffset = offset;
    updateSequence(intervalStart, intervalLen);

}

void Cluster::updateUniquePartners()
{
    uniquePartners.clear();

    foreach_(const ClusterSegmentPtr& segment, segments) {
        ClusterPtr partner = segment->getPartner()->getParentCluster();
        uniquePartners.insert(partner);
    }

}




bool Cluster::alignmentConcordantWithCluster(const QueryAlignment &aln, bool firstAlignment, const Cluster *cluster)
{
    if (cluster->dir == DirectionLeft) {
        if ( (firstAlignment && aln.positiveStrand) || (!firstAlignment && !aln.positiveStrand) ) {
            return true;
        } else {
            return false;
        }
    } else if (cluster->dir == DirectionRight) {
        if ( (firstAlignment && !aln.positiveStrand) || (!firstAlignment && aln.positiveStrand) ) {
            return true;
        } else {
            return false;
        }
    } else {
        assert(cluster->dir == DirectionUnknown);
        return false;
    }



}

Cluster::Direction Cluster::inferDirection(bool isPositiveStrand, bool firstAlignment)
{
    if (firstAlignment) {
        return isPositiveStrand ? DirectionLeft : DirectionRight;
    } else {
        return isPositiveStrand ? DirectionRight : DirectionLeft;
    }
}




