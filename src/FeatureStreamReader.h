#ifndef FEATURE_STREAM_READER_H
#define FEATURE_STREAM_READER_H

#include "Feature.h"
#include <fstream>

class FeatureStreamReader
{
protected:
    string errorMessage;
    std::ifstream infile;
    vector<FeaturePtr> features;
    void setError(const string& message) { errorMessage = message; }
    virtual FeaturePtr parseLine(const string& line) = 0;
public:
    FeatureStreamReader();
    bool open(const string& filePath);
    bool hasError() { return errorMessage.length() > 0; }
    const string getError() { return errorMessage; }
    bool readAll(FeatureSet& featureSet, const string& filePath);
    static bool load(FeatureSet& featureSet, const string& filePath );
};

class GffStreamReader : public FeatureStreamReader {
    virtual FeaturePtr parseLine(const string& line);
};


#endif // FEATURE_STREAM_READER_H
