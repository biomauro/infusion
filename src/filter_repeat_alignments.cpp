#include <iostream>
#include <algorithm>

#include <boost/algorithm/string.hpp>
#include <seqan/bam_io.h>
#include <seqan/arg_parse.h>

#include "Global.h"
#include "GenomicOverlapDetector.h"
#include "StringUtils.h"
#include "SimpleInterval.h"
#include "PerfTimer.h"

using namespace std;
using namespace seqan;


#define PROGRAM_NAME                    "filter_repeat_alignments"


/*struct RepeatInterval {
    int begin, end;
    const string repeatType;
    RepeatInterval() : begin(-1), end(-1) {}
    RepeatInterval(int startPos, int endPos, const string& type )
        : begin(startPos), end(endPos), repeatType(type) {}

    bool contains
};*/

void setupArgumentParser( ArgumentParser& parser )
{
    setVersion(parser, INFUSION_VERSION);

    addDescription(parser, "This tool is part of InFusion pipeline. It removes from input BAM alignment file\
                   all records aligned to repeat regions. Repeat regions should be provided in UCSC track format.");

    string usageLine;
    usageLine.append(PROGRAM_NAME).append(" [OPTIONS]");
    addUsageLine(parser, usageLine);

    //addArgument(parser, ArgParseArgument(ArgParseArgument::INPUTFILE, "input"));

    addSection(parser, "Options");

    ArgParseOption inputOpt( "i", "input", "Path to input file.", ArgParseArgument::STRING );
    inputOpt._isRequired = true;
    addOption(parser, inputOpt);
    addOption(parser, ArgParseOption( "o", "output", "Path to output file.", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "r", "repeats-annotation", "Path to repeat annotations.", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "a", "gene-annotation", "Path to gene annotations in GTF format", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "s", "sam", "Path output in SAM format, default is BAM." ));


    addTextSection(parser, "References");
    addText(parser, "Konstantin Okonechnikov <okonechnikov@mpiib-berlin.mpg.de>");
}


bool loadRepeatIntervals(GenomicOverlapDetector<SimpleInterval>& intervalOverlapper, const string& path) {

    ifstream inFile(path);

    if (!inFile.is_open()) {
        cerr << "Failed to open input file: " << path << endl;
        return false;
    }

    string line;

    PerfTimer t0;

    while (getline(inFile,line)) {

        if (StringUtils::startsWith(line, "#")) {
            continue;
        }

        int indexCount = 0;

        string chrName, startPosStr, endPosStr, repeatType;
        for (size_t i = 0; i < line.length(); ++i ) {
            char c = line[i];
            if (c == '\t') {
                indexCount++;
                continue;
            }

            switch(indexCount) {
                case 5:
                    chrName += c;
                    break;
                case 6:
                    startPosStr += c;
                    break;
                case 7:
                    endPosStr += c;
                    break;
                case 11:
                    repeatType += c;
                    break;
            }

        }

        int startPos = boost::lexical_cast<int>(startPosStr);
        int endPos = boost::lexical_cast<int>(endPosStr);


        if (chrName == "chrM") {
            chrName = "MT";
        } else {
            boost::replace_first( chrName, "chr", "");
        }

        SimpleInterval iv(startPos, endPos);
        intervalOverlapper.insertInterval(chrName, startPos, endPos, iv);

    }

    t0.drop();

#ifdef DEBUG
    /*cout << "Time checking for comment: " << t1.getElapsed() << endl;
    cout << "Time parsing string: " << t2.getElapsed() << endl;
    cout << "Time splitting string: " << t4.getElapsed() << endl;
    cout << "Time modifying chromosome: " << t5.getElapsed() << endl;
    cout << "Time parsing postions: " << t6.getElapsed() << endl;
    cout << "Time inserting interval: " << t3.getElapsed() << endl;*/

    cout << "\nOverall reading time: " << t0.getElapsed() << endl;

#endif

    return true;
}


int filterReadAlignments(BamStream& bamOutput, vector<BamAlignmentRecord>& readAlignments, const vector<string>& chrNames,
                                                GenomicOverlapDetector<SimpleInterval>& repeatIntervals, GenomicOverlapDetector<string>& geneIntervals) {

    vector<BamAlignmentRecord> passed;

    foreach_ (BamAlignmentRecord& record, readAlignments) {
        vector<SimpleInterval> results;

        const string& refId = chrNames.at(record.rID);
        SimpleInterval alnInterval(record.beginPos, record.beginPos + getAlignmentLengthInRef(record));
        repeatIntervals.findIntervals(results, refId, alnInterval.begin, alnInterval.end );
        bool pass = true;
        if (results.size() > 0) {
            foreach_ (const SimpleInterval& rInterval, results) {
                if (rInterval.contains(alnInterval)) {
                    pass = false;
                    break;
                }
            }
        }
        if (pass) {
            passed.push_back(record);
        }
    }

    foreach_ (BamAlignmentRecord& record, passed ) {
        if (writeRecord(bamOutput, record) != 0) {
            return -1;
        }
    }

    return passed.size();
}


int performAnalysis(const string& inFile, const string& outFile, const string& pathToRepeats, const string& pathToGtf, bool samFormat) {


    //BamStream bamInput(inFile.c_str(), BamStream::READ);

    BamStream bamInput(inFile.c_str(), BamStream::READ, BamStream::SAM);

    if (!isGood(bamInput)) {
        cerr << "Problem with input BAM file " << inFile << endl;
        return -1;
    }

    BamStream bamOutput(outFile.c_str(), BamStream::WRITE, samFormat ? BamStream::SAM : BamStream::BAM);

    if (!isGood(bamOutput)) {
        cerr << "Problem with output BAM file " << outFile << endl;
        return -1;
    }

    cerr << "Started loading repeat intervals..." << endl;
    GenomicOverlapDetector<SimpleInterval> repeatIntervals;
    if (!loadRepeatIntervals(repeatIntervals, pathToRepeats)) {
        return -1;
    }
    cerr << "Finished loading repeat intervals..." << endl;

    // TODO: use gene annotations?
    GenomicOverlapDetector<string> geneIntervals;
    if (pathToGtf.length() > 0) {
        cerr << "Started loading gene annotations..." << endl;

        cerr << "Finished loading gene annotations..." << endl;
    }

    vector<string> chrNames;
    StringUtils::convertStringSetToVector(bamInput._nameStore, chrNames);

    bamOutput.header = bamInput.header;

    cerr << "Started filtering" << endl;

    BamAlignmentRecord record;
    vector<BamAlignmentRecord> readAlignments;
    CharString curReadName;

    while (!atEnd(bamInput))
    {
        if (readRecord(record, bamInput) != 0)
        {
            std::cerr << "Could not read alignment from " << inFile << std::endl;
            return 1;
        }

        if (hasFlagUnmapped(record)) {
            continue;
        }

        if (record.qName != curReadName) {
            int res = filterReadAlignments(bamOutput, readAlignments, chrNames, repeatIntervals, geneIntervals);
            if (res == -1) {
                std::cerr << "Could not write filtered alignment to " << outFile << std::endl;
                return 1;
            }
            curReadName = record.qName;
            readAlignments.clear();
        }

        readAlignments.push_back(record);

    }


    if (readAlignments.size() > 0) {
        int res = filterReadAlignments(bamOutput, readAlignments, chrNames, repeatIntervals, geneIntervals);
        if (res == -1) {
            std::cerr << "Could not write filtered alignment to " << outFile << std::endl;
            return 1;
        }
    }

    cerr << "Finished filtering" << endl;


    return 0;

}


int main(int argc, char const ** argv)
{

    ArgumentParser parser(PROGRAM_NAME);
    setupArgumentParser(parser);

    ArgumentParser::ParseResult res = parse(parser, argc, argv);
    if (res != ArgumentParser::PARSE_OK) {
        return 1;
    }

    string inFile, outFile, repeatsFile, gtfFile;
    bool samFormat = false;

    getOptionValue(inFile, parser, "i");
    getOptionValue(outFile, parser, "o");
    getOptionValue(repeatsFile, parser, "r");
    getOptionValue(gtfFile, parser, "a");
    getOptionValue(samFormat, parser, "s");



#ifdef DEBUG
    cerr << "Options" << endl;
    cerr << "Input BAM file: " << inFile << endl;
    cerr << "Repeats annotation file: " << repeatsFile << endl;
    cerr << "Genome annotation file: " << gtfFile << endl;
    cerr << "Output BAM file: " << outFile << endl;

#endif

    return performAnalysis(inFile, outFile, repeatsFile, gtfFile, samFormat);
}




