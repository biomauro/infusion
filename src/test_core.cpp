#include <cassert>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <regex.h>

#include <boost/shared_ptr.hpp>
#include <boost/scoped_ptr.hpp>

#include <seqan/sequence.h>
#include <seqan/misc/misc_interval_tree.h>
#include <seqan/find.h>

#include "Global.h"
#include "FeatureStreamReader.h"
#include "AnnotationModel.h"
#include "Reference.h"
#include "StringUtils.h"
#include "SimpleInterval.h"
#include "ClusterTree.h"
#include "IntervalTree.h"
#include "AlignmentUtils.h"
#include "FindSupportingMatesTask.h"
#include "ChunkBamReader.h"
#include "LocalAlignmentStore.h"
#include "ClusterTree.h"
#include "PerfTimer.h"
#include "MiscUtils.h"
#include "Fusion.h"

using namespace std;
using boost::scoped_ptr;
using seqan::String;

static string COMMON_DATA_FOLDER("../../test/common_data");

void testGffReader() {

    FeatureSet transcripts;
    bool ok = FeatureStreamReader::load(transcripts, "/home/non_existing_file");
    assert( ok == false);

    /*ok = FeatureStreamReader::load(transcripts, COMMON_DATA_FOLDER + "/test_region_01.gff");

    assert( ok );


    Feature f = transcripts["gene1"];

    assert( f );

    assert( f->pos == 125);
    assert( f->seqName == "chr1" );*/

}

void testAnnotationModel() {

    {
    AnnotationModel model1;
    bool ok = model1.loadTranscripts(COMMON_DATA_FOLDER + "/test001.gtf");
    assert(ok);
    const TranscriptSet& tSet1 = model1.getTranscripts();
    assert(tSet1.size() == 1);
    const TranscriptPtr& f1 = tSet1.begin()->second;
    assert(f1->getId() == "transcriptA");
    assert(f1->getName() == "t1");
    assert(f1->getRefName() == "ref1");
    assert(f1->getGeneId() == "geneA");
    assert(f1->getGeneName() == "g1");
    assert(f1->getGeneBioType() == "protein_coding");
    assert(f1->getStartPos() == 6);
    assert(f1->getLength() == 10);
    assert(f1->isPositiveStranded() == true);

    const GenePtr& gene = f1->getGene();
    assert(gene->getStartPos() == 6);
    assert(gene->getEndPos() == 25);
    assert(!f1->geneContainsGenomicPoint(0));
    assert(f1->geneContainsGenomicPoint(15));


    const vector<ExonPtr>& exons = f1->getExons();
    assert(exons.size() == 2);
    foreach_ ( const ExonPtr& e, exons) {
        assert( e->getTranscript()->getId() == "transcriptA");
    }

    assert(exons[0]->id == "1");
    assert(exons[1]->id == "2");

    assert(f1->getGenomicPos(0) == 6);
    assert(f1->getGenomicPos(5) == 21);
    assert(f1->getGenomicPos(9) == 25);

    vector<GenomicInterval> iv1 = f1->getGenomicIntervals(0,10);
    assert(iv1.size() == 2);
    assert(iv1[0].begin == 6);
    assert(iv1[0].end == 10);
    assert(iv1[1].begin == 21);
    assert(iv1[1].end == 25);

    vector<GenomicInterval> iv2 = f1->getGenomicIntervals(1,3);
    assert(iv2.size() == 1);
    assert(iv2[0].begin == 7);
    assert(iv2[0].end == 9);

    vector<GenomicInterval> iv3 = f1->getGenomicIntervals(6,3);
    assert(iv3.size() == 1);
    assert(iv3[0].begin == 22);
    assert(iv3[0].end == 24);

    vector<GenomicInterval> iv4 = f1->getGenomicIntervals(1,8);
    assert(iv4.size() == 2);
    assert(iv4[0].begin == 7);
    assert(iv4[0].end == 10);
    assert(iv4[1].begin == 21);
    assert(iv4[1].end == 24);

    };



    {
    AnnotationModel model2;
    bool ok = model2.loadTranscripts(COMMON_DATA_FOLDER + "/test002.gtf");
    assert(ok);
    const TranscriptSet& tSet2 = model2.getTranscripts();
    assert(tSet2.size() == 1);
    const TranscriptPtr& f2 = tSet2.begin()->second;
    assert(f2->getId()== "ENSMUST00000105216");
    assert(f2->getName() == "AC007307.1-201" );
    assert(f2->getGeneId() == "ENSMUSG00000000702");
    assert(f2->getGeneName() == "AC007307.1" );
    assert(f2->getGeneBioType() == "protein_coding");
    assert(f2->getRefName() == "NT_166433");
    assert(f2->getStartPos() == 11954);
    assert(f2->getLength() == 1037);
    assert(f2->isPositiveStranded() == true);
    assert(f2->getCdrStart() == 12025);
    assert(f2->getCdrStop() == 17788);
    }

    {
    AnnotationModel model;
    bool ok = model.loadTranscripts(COMMON_DATA_FOLDER + "/test003.gtf");
    assert(ok);
    const TranscriptSet& tSet = model.getTranscripts();
    assert(tSet.size() == 1);
    const TranscriptPtr& f1 = tSet.begin()->second;
    assert(f1->getId() == "transcriptA");
    assert(f1->getRefName() == "ref1");
    assert(f1->getStartPos() == 6);
    assert(f1->getLength() == 10);
    assert(f1->isPositiveStranded() == false);
    assert(f1->getGenomicPos(0) == 25);
    assert(f1->getGenomicPos(5) == 10);
    assert(f1->getGenomicPos(9) == 6);

    vector<GenomicInterval> iv1 = f1->getGenomicIntervals(0,10);
    assert(iv1.size() == 2);
    assert(iv1[0].begin == 6);
    assert(iv1[0].end == 10);
    assert(iv1[1].begin == 21);
    assert(iv1[1].end == 25);

    vector<GenomicInterval> iv2 = f1->getGenomicIntervals(0,3);
    assert(iv2.size() == 1);
    assert(iv2[0].begin == 23);
    assert(iv2[0].end == 25);

    vector<GenomicInterval> iv3 = f1->getGenomicIntervals(6,4);
    assert(iv3.size() == 1);
    assert(iv3[0].begin == 6);
    assert(iv3[0].end == 9);

    vector<GenomicInterval> iv4 = f1->getGenomicIntervals(1,7);
    assert(iv4.size() == 2);
    assert(iv4[0].begin == 8);
    assert(iv4[0].end == 10);
    assert(iv4[1].begin == 21);
    assert(iv4[1].end == 24);

    }

    {
    AnnotationModel model;
    bool ok = model.loadTranscripts(COMMON_DATA_FOLDER + "/test004.gtf");
    assert(ok);
    const TranscriptSet& tSet = model.getTranscripts();
    const TranscriptPtr& t = tSet.begin()->second;
    assert(t->getName() == "TMPRSS2-001" );
    assert(t->getGeneName() == "TMPRSS2" );
    assert(t->getGeneBioType() == "protein_coding");
    assert(t->getStartPos() == 42836477 );
    assert(t->getEndPos() == 42880085 );
    assert(t->isPositiveStranded() == false);
    assert(t->getCdrStart() == 42870059);
    assert(t->getCdrStop() == 42838068);
    assert(t->getRefName() == "21");

    ExonPtr exon = t->getExons().back();
    assert(exon->seqName == "21");
    assert(exon->startPos == 42880007 );
    assert(exon->endPos() == 42880085 );
    assert(!exon->positiveStranded);
    assert(exon->id == "1");
    }


    {
    AnnotationModel model;
    bool ok = model.loadTranscripts(COMMON_DATA_FOLDER + "/test005.gtf");
    assert(ok);
    const TranscriptSet& tSet = model.getTranscripts();
    assert(tSet.size() == 3);
    string id1 = "ENST00000314423";
    const TranscriptPtr& t = tSet.find(id1)->second;
    assert(t->getName() == "CIRH1A-001" );
    assert(t->getGeneName() == "CIRH1A" );
    assert(t->getGeneBioType() == "protein_coding");
    assert(t->getGene()->getStartPos() == 69166417 );
    assert(t->getGene()->getEndPos() ==  69202923);
    assert(t->isPositiveStranded() == true);
    assert(t->getStartPos() == 69166417);
    assert(t->getEndPos() ==  69202923);
    assert(t->getCdrStart() ==  69167362);
    assert(t->getCdrStop() == 69202839);
    ExonPtr exon = t->getExons().front();
    assert(exon->seqName == "16");
    assert(exon->startPos == 69166417 );
    assert(exon->endPos() == 69166591 );
    assert(exon->id == "1");
    }



    {
    AnnotationModel model;
    bool ok = model.loadTranscripts(COMMON_DATA_FOLDER + "/simple.gtf");
    assert(ok);
    const TranscriptSet& tSet = model.getTranscripts();
    assert(tSet.size() == 4);
    assert(tSet.count("tC1") == 1);
    const TranscriptPtr& f1 = tSet.find("tC1")->second;
    assert(f1->getRefName() == "ref1");
    assert(f1->getStartPos() == 20);
    assert(f1->isPositiveStranded() == true);

    const GenePtr& gene = f1->getGene();
    assert(gene);
    assert(gene->getName() == "C");
    assert(gene->getId() == "geneC");
    assert(gene->getStartPos() == 20);
    assert(gene->getEndPos() == 399);

    }



}


void testFeature() {

    vector<FeaturePtr> features;

    FeaturePtr f1( new FeatureData() );
    f1->startPos = 25;
    features.push_back(f1);

    FeaturePtr f2( new FeatureData() );
    f2->startPos = 5;
    features.push_back(f2);

    FeaturePtr f3( new FeatureData() );
    f3->startPos = 15;
    features.push_back(f3);


    std::sort(features.begin(), features.end(), FeatureCompare());

    assert(features[0]->startPos == 5);
    assert(features[2]->startPos == 25);



}

#define S1 "CGGAGCGGGCAGCGGCCAAGTCAGGGCCGTCCGGGGGCGCGGCCGGCGATGCCCGCAGCCCCCGCCGCGC\
CCCGCCGGGCCTGCTGAGCCGCCCCCGGGCCGGGGTCGCGCCGGGCCGGGCCGCGCCCGGGGCGGGGCGG\
CGCTGCCTGCATGACCCTCCGGCGGCGCGGGGAGAAGGCGACCATCAGCATCCAGGAGCATATGGCCATC\
GACGTGTGCCCCGGCCCCATCCGTCCCATCAAGCAGATCTCCGACTACTTCCCCCGCTTCCCGCGGGGCC\
TGCCCCCGGACGCCGGGCCCCGAGCCGCTGCACCCCCGGACGCCCCCGCGCGCCCGGCTGTGGCCGGTGC\
CGGCCGCCGCAGCCCCTCCGACGGCGCCCGCGAGGACGACGAGGATGTGGACCAGCTCTTCGGAGCCTAC"



void testReferenceImpl(boost::scoped_ptr<ReferenceSeq>& ref) {

    bool refLoaded = ref->load(COMMON_DATA_FOLDER + "/test.fa");
    assert(refLoaded);

    string subseq = toCString(ref->getSubSequence("s1",0,10));
    assert(subseq.size() == 10);
    assert(subseq == "CGGAGCGGGC");

    subseq = toCString(ref->getSubSequence("s1",0,10, true));
    assert(subseq.size() == 10);
    assert(subseq == "GCCCGCTCCG");

    subseq = toCString(ref->getSubSequence("s2", 70, 10 ));
    assert(subseq.size() == 10);
    assert(subseq == "CGGACGCCGA");

    subseq = toCString(ref->getSubSequence("s25",0,10));
    assert(subseq.size() == 0);

    subseq = toCString(ref->getSubSequence("s2",1000, 1200));
    assert(subseq.size() == 0);

    seqan::CharString seq;
    seqan::CharString expected(S1);
    ref->getWholeSequence(seq,"s1");
    assert( expected == seq);


}



void testReference() {

    scoped_ptr<ReferenceSeq> ref1( new ReferenceSeqIndexed() );
    testReferenceImpl(ref1);


    scoped_ptr<ReferenceSeq> ref2( new ReferenceSeqInMemory() );
    testReferenceImpl(ref2);


}



void generateRandomSequences(seqan::StringSet<seqan::CharString>& stringSet, int minLength, int maxLength, int numSequences) {
    using namespace seqan;
    //  for the sake of reproducibility
        Rng<MersenneTwister> rng;

        // create StringSet of 1000 sequences
        reserve(stringSet, numSequences);
        for (int seqNo = 0; seqNo < numSequences; ++seqNo)
        {
            DnaString tmp;
            int len = pickRandomNumber(rng) % maxLength + minLength;
            for (int i = 0; i < len; ++i)
                appendValue(tmp, Dna(pickRandomNumber(rng) % 4));
            appendValue(stringSet, tmp);
            //std::cout << ">Seq" << seqNo << std::endl << tmp << std::endl;
        }

}


void findPatternPerfTest() {

    using namespace seqan;

    // generate sources 1000 source with length [50, 550]

    StringSet<CharString> sources, queries;
    generateRandomSequences(sources, 50, 500, 1000);
    generateRandomSequences(queries, 3, 10, 100);


    cout << "Starting perf test" << endl;

    size_t numSources = length(sources);
    size_t numQueries = length(queries);
    size_t numResults = 0;

    PerfTimer t1;

    for (size_t i = 0; i < numSources; ++i) {
        for (size_t j=0; j < numQueries; ++j) {
            vector<SearchResult> results = StringUtils::findDnaPattern(queries[j],sources[i],-1,true);
            numResults += results.size();
        }
    }

    t1.drop();

    cout << "Number of results: " << numResults << endl;
    cout << "Processing time: " << t1.getElapsed() << endl;
    //assert(numResults == 1828130);

}


void testStringUtils() {

    CharString hell("ACGACGACGATTA");
    auto sub1 = infix(hell, 1, 3);
    CharString subSeq(sub1);
    assert(subSeq == "CG");

    CharString stupid("AAAAAAAAAAAAAAAAAAAAAAFAFAFAFAFAFAFAFAFAAFAFAFAFAFAFAFAFAFAFAFAF");
    clear(stupid);
    assert(length(stupid) == 0);

    string source("AAACCCCCCCCCCCCCCCCCCCCCCCGGG");

    {
        string p("AAA");
        size_t res = StringUtils::findFlankingPattern(p,source,false,0);
        assert(res == 0);
    }

    {
        string p("CGGG");
        size_t res = StringUtils::findFlankingPattern(p,source,true,0);
        assert(res == 25);
    }

    {
        using namespace seqan;

        CharString haystack("AAACCCCCCCCCCCCCCCCGGG");
        CharString needle("ATT");
        Finder<CharString> finder(haystack);
        Pattern<CharString, DPSearch<SimpleScore> > pattern(needle, SimpleScore(0, -2, -1));
        while (seqan::find(finder, pattern, -1))
            while (seqan::findBegin(finder, pattern, getScore(pattern)))
                std::cout << '[' << beginPosition(finder) << ',' << endPosition(finder) << ")\t" << infix(finder) << "\t" << std::endl;

    }

    {
        CharString s(source.c_str());
        CharString p1("AAA");
        auto res1 = StringUtils::findDnaPattern(p1,s,0);
        assert(res1.size() == 1);
        assert( res1.at(0).pos == 0);
        CharString p2("CGGG");
        auto res2 = StringUtils::findDnaPattern(p2,s,0);
        assert( res2.size() == 2);
        assert( res2.at(0).pos == 25);
        assert( res2.at(1).pos == 23);

    }
\
    {
        int x = 500;
        string xStr = StringUtils::numToStr(x);
        assert(xStr == "500");
    }


    {
        string readName1("17:1247566-1303672C:ENST00000466227:2:1378:551:714/1");
        StringUtils::removeReadMateIdentiferInPlace(readName1);
        string readName2("17:1247566-1303672C:ENST00000466227:2:1378:551:714/2");
        StringUtils::removeReadMateIdentiferInPlace(readName2);
        assert(readName1 == readName2);

    }

    {




        /*regex_t regex;
                int reti;
                char msgbuf[100];

                reti = regcomp(&regex, "^a[[:alnum:]]", 0);
                if( reti ){ fprintf(stderr, "Could not compile regex\n"); exit(1); }

                reti = regexec(&regex, "abc", 0, NULL, 0);
                if( !reti ){
                        puts("Match");
                }
                else if( reti == REG_NOMATCH ){
                        puts("No match");
                }
                else{
                        regerror(reti, &regex, msgbuf, sizeof(msgbuf));
                        fprintf(stderr, "Regex match failed: %s\n", msgbuf);
                        exit(1);
                }

            regfree(&regex);
            //std::string s ("subject");
            //std::regex e ("(sub)(.*)");*/



    }

    {
        string readName1("R_1:1");
        StringUtils::removeReadMateIdentiferInPlace(readName1);
        string readName2("R_1:2");
        StringUtils::removeReadMateIdentiferInPlace(readName2);
        assert(readName1 == readName2);

    }

    {
        CharString s("AAACCCAAA");
        CharString p1("CCC"), p2("CCT");
        vector<SearchResult> r1 = StringUtils::findDnaPattern(p1, s, -1, false);
        assert(r1.size() == 5);
        sort(r1.begin(), r1.end(), CompareSearchResults());
        vector<SearchResult> r2 = StringUtils::findDnaPattern(p2, s, -1, false);
        assert(r2.size() == 4);
        vector<SearchResult> r3 = StringUtils::findDnaPattern(p1, s, 0, false);
        assert(r3.size() == 1);
        vector<SearchResult> r4 = StringUtils::findDnaPattern(p2, s, 0, false);
        assert(r4.size() == 0);





    }

    findPatternPerfTest();

}



void testAlignmentUtils() {
    string source("AAACCCCCCCCCCCCCCCCCCCCCCCGGG");

    {
        string p("AAA");
        int score = AlignmentUtils::alignFlankingPattern(p,source,false,2);
        assert(score == -2);
    }

    /*{
        string p("CGGG");
        int score = AlignmentUtils::alignFlankingPattern(p,source,true,2);
        assert(score == -2);
    }

    {
        string p("ACA");
        int score = AlignmentUtils::alignFlankingPattern(p,source,false,2);
        assert(score == -3);
    }

    {
        string p("CGTG");
        int score = AlignmentUtils::alignFlankingPattern(p,source,true,2);
        assert(score == -3);
    }
    {
        string p("CAAA");
        int score = AlignmentUtils::alignFlankingPattern(p,source,false,2);
        assert(score == -3);
    }*/


    CharString host("IAMWITHSTUPIDWITHSTUPID");
    CharString query1("IAMW");
    CharString query2("IAW");
    CharString query3("IAXWITH");
    CharString query4("IAXWIXH");


    {
        AlignmentResult res = AlignmentUtils::performLocalAlignment( host, query1);
        assert(res.score == 8 );
    }

    {
        AlignmentResult res = AlignmentUtils::performLocalAlignment( host, query2);
        assert(res.score == 5 );
    }

    {

        AlignmentResult res = AlignmentUtils::performLocalAlignment( host, query3);
        assert(res.score == 11);

    }

    {

        AlignmentResult res = AlignmentUtils::performLocalAlignment( host, query4);
        assert(res.score == 8);

    }



}


void generateRandomIntervals(int low, int high, int numIntervals, vector<SimpleInterval>& results) {

    for (int i = 0; i < numIntervals; ++i) {
        int range = high - low + 1;
        int start = rand() % range + low;
        int end = start - 1;
        while (end < start ) {
            end = rand()  % range + low;
        }
        results.push_back(SimpleInterval(start, end));
    }
}

void debugOutputIntervals(const char* filename, vector<SimpleInterval>& intervals) {
    ofstream out;
    out.open(filename, ios_base::out);

    foreach_ (const SimpleInterval& iv, intervals) {
        out << iv.begin << " " << iv.end << endl;
    }


}


bool intervalListsMatch(vector<SimpleInterval>& list1, vector<SimpleInterval>& list2) {

    std::sort(list1.begin(), list1.end());
    std::sort(list2.begin(), list2.end());

    size_t count = list1.size();
    if (list1.size() != list2.size()) {
        debugOutputIntervals("/tmp/iv1.txt", list1);
        debugOutputIntervals("/tmp/iv2.txt", list2);
        return false;
    }


    for (size_t i = 0; i < count; ++i) {
        if (list1[i] != list2[i]) {
            return false;
        }
    }

    return true;
}

void testIntervalTree() {

    {
        IntervalTree<SimpleInterval> redBlackTree;

        redBlackTree.insertInterval( SimpleInterval(10,10));
        redBlackTree.insertInterval( SimpleInterval(15,15));
        redBlackTree.insertInterval( SimpleInterval(20,20));
        cout << "\nTree:\n";
        redBlackTree.print();
        assert(redBlackTree.getRoot()->getStart() == 15);
    }


    {
        IntervalTree<SimpleInterval> redBlackTree;

        redBlackTree.insertInterval( SimpleInterval(20,20));
        redBlackTree.insertInterval( SimpleInterval(15,15));
        redBlackTree.insertInterval( SimpleInterval(10,10));
        cout << "\nTree:\n";
        redBlackTree.print();
        assert(redBlackTree.getRoot()->getStart() == 15);
    }


    {
        IntervalTree<SimpleInterval> redBlackTree;

        redBlackTree.insertInterval( SimpleInterval(41,41));
        redBlackTree.insertInterval( SimpleInterval(38,38));
        redBlackTree.insertInterval( SimpleInterval(31,31));
        redBlackTree.insertInterval( SimpleInterval(12,12));
        redBlackTree.insertInterval( SimpleInterval(19,19));
        redBlackTree.insertInterval( SimpleInterval(8,8));
        cout << "\nTree:\n";
        redBlackTree.print();
        auto node = redBlackTree.getRoot();
        assert(node->getStart() == 38);

        node = redBlackTree.stepBack(node);
        assert(node->getStart() == 31);
        assert(node->isRed() == false);

        node = redBlackTree.stepBack(node);
        assert(node->getStart() == 19);
        assert(node->isRed() == true);

        node = redBlackTree.stepBack(node);
        assert(node->getStart() == 12);
        assert(node->isRed() == false);

        node = redBlackTree.stepBack(node);
        assert(node->getStart() == 8);
        assert(node->isRed() == true);

        node = redBlackTree.stepForward(redBlackTree.getRoot());
        assert(node->getStart() == 41);
        assert(node->isRed() == false);

    }


    {
        IntervalTree<SimpleInterval> redBlackTree;

        redBlackTree.insertInterval( SimpleInterval(17,19));
        redBlackTree.insertInterval( SimpleInterval(5,11));
        redBlackTree.insertInterval( SimpleInterval(22,23));
        redBlackTree.insertInterval( SimpleInterval(15,18));
        redBlackTree.insertInterval( SimpleInterval(4,8));
        redBlackTree.insertInterval( SimpleInterval(7,10));

        cout << "\nTree:\n";
        redBlackTree.print();

        {
            vector<SimpleInterval> results;
            SimpleInterval q(1,3);
            redBlackTree.findOverlapers(results,q);
            assert(results.size() == 0);
        }

        {
            vector<SimpleInterval> results;
            SimpleInterval q(25,27);
            redBlackTree.findOverlapers(results,q);
            assert(results.size() == 0);
        }

        {
            vector<SimpleInterval> results;
            SimpleInterval q(14,16);
            redBlackTree.findOverlapers(results,q);
            assert(results.size() == 1);
            assert(results[0] == SimpleInterval(15,18));
        }

        {
            vector<SimpleInterval> results;
            SimpleInterval q(12,14);
            redBlackTree.findOverlapers(results,q);
            assert(results.size() == 0);
        }

        {
            vector<SimpleInterval> results;
            SimpleInterval q(5,10);
            redBlackTree.findOverlapers(results,q);
            assert(results.size() == 3);
            assert(results[0] == SimpleInterval(4,8));
            assert(results[1] == SimpleInterval(5,11));
            assert(results[2] == SimpleInterval(7,10));

        }


        {
            vector<SimpleInterval> results;
            SimpleInterval q(4,23);
            redBlackTree.findOverlapers(results,q);
            assert(results.size() == redBlackTree.getSize() );
         }


    }

    // RANDOM INTERVALS TEST

    {
        IntervalTree<SimpleInterval> intervalTree;

        vector<SimpleInterval> source, queries;
        srand(0);
        generateRandomIntervals(1, 1000, 25, source );
        generateRandomIntervals(1, 1000, 1, queries );

        //queries.push_back( Interval(300, 900));
        vector<SimpleInterval> list1, list2;

        // TEST1
        foreach_ (SimpleInterval& i1, queries) {
            foreach_ (SimpleInterval& i2,  source) {
                if (i2.intersects(i1)) {
                    list1.push_back(i2);
                }
            }
        }


        // TEST2
        foreach_ ( SimpleInterval& iv,  source) {
            intervalTree.insertInterval(iv);
        }

        foreach_ ( SimpleInterval& iv,  queries) {
            intervalTree.findOverlapers(list2,iv);
        }


        ofstream out;
        out.open("/tmp/tree.dot", ios_base::out);
        assert(out.is_open());

        intervalTree.outputDot(out);


        // COMPARE
        assert(intervalListsMatch(list1, list2));
    };

    {
        IntervalTree<SimpleInterval> intervalTree;

        vector<SimpleInterval> source, queries;
        srand(1);
        generateRandomIntervals(1, 10000, 10000, source );
        generateRandomIntervals(1, 10000, 1000, queries );

        foreach_ ( SimpleInterval& iv, source) {
            intervalTree.insertInterval(iv);
        }

        // TODO: add iterator
        auto node = intervalTree.leftMostNode();
        int prevPos = -1;
        size_t numIntervals = 0;
        while (node != NULL) {
            ++numIntervals;
            assert( prevPos <= node->getStart());
            prevPos = node->getStart();
            node = intervalTree.stepForward(node);
        }
        assert(numIntervals == source.size());

        foreach_(SimpleInterval& query, queries) {

            vector<SimpleInterval> list1, list2;

            foreach_(SimpleInterval& iv, source) {
                if (iv.intersects(query)) {
                    list1.push_back(iv);
                }
            }


            intervalTree.findOverlapers(list2,query);
            assert(intervalListsMatch(list1, list2));

        }



    }

    typedef seqan::IntervalAndCargo<int,size_t> SInterval;
    typedef seqan::IntervalTree<int,size_t> SIntervalTree;

    
    {


         vector<SimpleInterval> source, queries;
         srand(1);
         //generateRandomIntervals(1, 10000, 100000, source );
         //generateRandomIntervals(1, 10000, 1000, queries );

         source.push_back(SimpleInterval(11,12));
         queries.push_back(SimpleInterval(12,15));

         debugOutputIntervals("/tmp/source.txt", source);


         String<SInterval> intervals;
         seqan::resize(intervals, source.size());

         for ( size_t idx = 0, count = source.size(); idx < count; ++idx) {
             SimpleInterval iv = source.at(idx);
             SInterval siv;
             siv.i1 = iv.begin;
             siv.i2 = iv.end + 1;
             siv.cargo = idx;
             intervals[idx] = siv;
         }

         SIntervalTree seqanTree(intervals);

         foreach_(SimpleInterval& query, queries) {

             vector<SimpleInterval> list1, list2;

             foreach_ (SimpleInterval& iv, source) {
                 if (iv.intersects(query)) {
                     list1.push_back(iv);
                 }
             }


             String<size_t> res;
             seqan::findIntervals(seqanTree, query.begin, query.end , res);

             for (size_t i = 0; i < length(res); ++i) {
                 size_t idx = res[i];
                 list2.push_back(source.at(idx));
             }



             assert(intervalListsMatch(list1, list2));

         }




    }







}

struct T {
    int x;
    T() : x(1) { cout << "I am born!" << endl; }

};


void testBoost() {

    boost::shared_ptr<int> empty;
    if (empty) {
        assert(0);
    }

    int* v = empty.get();
    assert( v == NULL);

    empty.reset(new int(25) );

    if (!empty) {
        assert(0);
    }

      vector<T> data;
    data.push_back(T());
    data.push_back(T());

    int count = 0;
    foreach_(const T& t, data) {
        count += t.x;
    }

    assert(count == 2);

}


void testFindMates(const AnnotationModel& model, string pathGenomicAlignment, bool firstMates, string pathTranscriptomicAlignment)
{
    std::map<string, QueryAlignment> alnMap;
    boost::scoped_ptr<ReadMateAlignmentChecker> mateChecker(new InsertSizeBasedMateChecker(1000));

    ChunkBamReaderConfig cfg;
    cfg.reverseAlignmentStrand = !firstMates;
    ChunkBamReader genomicReader(pathGenomicAlignment.c_str(), cfg);

    assert(genomicReader.init());

    while (!genomicReader.isEof()) {
        vector<BamAlignmentRecord> records;
        genomicReader.loadNextChunk(records);

        foreach_ (const BamAlignmentRecord& rec, records) {
            QueryAlignment aln = LocalAlignmentStore::getLocalAlignment(rec, genomicReader.getRefNames(), 5);
            alnMap[toCString(rec.qName)] = aln;
        }
    }

    ChunkBamReaderConfig cfgT;
    cfgT.reverseAlignmentStrand = firstMates;
    ChunkBamReader trReader(pathTranscriptomicAlignment.c_str(), cfgT);

    assert(trReader.init());

    while (!trReader.isEof()) {
        vector<BamAlignmentRecord> records;
        trReader.loadNextChunk(records);

        foreach_ (const BamAlignmentRecord& rec, records) {
            string readName = toCString(rec.qName);
            assert(alnMap.count(readName) > 0);
            const QueryAlignment& aln = alnMap[readName];
            string transcriptName = toCString(trReader.getRefNames()[rec.rID]);
            const TranscriptPtr transcript = model.getTranscript(transcriptName);
            assert(transcript);
            assert(mateChecker->genomicReadHasMateInTranscript(transcript, aln, firstMates, rec));
        }

    }

}


void testClusterTree() {
    // Simple test

    {
        ClusterTree<int> clusterTree(0);

        clusterTree.insertInterval(3,4,0);
        clusterTree.insertInterval(6,7,1);
        clusterTree.insertInterval(9,10,2);
        clusterTree.insertInterval(1,2,3);
        clusterTree.insertInterval(3,8,4);

        ClusterTree<int>::Node cluster = clusterTree.leftMostCluster();

        vector<int> iv1;
        cluster.getIntervalItems(iv1);
        assert( iv1.size() == 1);
        assert( MiscUtils::contains(iv1, 3) );

        cluster.stepForward();
        assert(!cluster.isNull());
        vector<int> iv2;
        cluster.getIntervalItems(iv2);
        assert (iv2.size() == 3);
        assert( MiscUtils::contains(iv2, 0) );
        assert( MiscUtils::contains(iv2, 1) );
        assert( MiscUtils::contains(iv2, 4) );


        cluster.stepForward();
        assert(!cluster.isNull());
        vector<int> iv3;
        cluster.getIntervalItems(iv3);
        assert (iv3.size() == 1);
        assert( MiscUtils::contains(iv3, 2) );

        cluster.stepForward();
        assert(cluster.isNull());
    }


    {
        ClusterTree<string> clusterTree(0);

        clusterTree.insertInterval(150,175,"one");
        clusterTree.insertInterval(120,140,"two");
        clusterTree.insertInterval(135,155,"three");
        clusterTree.insertInterval(135,155,"four");

        ClusterTree<string>::Node cluster = clusterTree.leftMostCluster();

        vector<string> iv1;
        cluster.getIntervalItems(iv1);
        assert( iv1.size() == 4);
        assert( MiscUtils::contains(iv1, "four") );

        cluster.stepForward();
        assert(cluster.isNull());
    }

    {
        ClusterTree<string> clusterTree(100);

        clusterTree.insertInterval(150,175,"one");
        clusterTree.insertInterval(200,240,"two");

        ClusterTree<string>::Node cluster = clusterTree.leftMostCluster();

        vector<string> iv1;
        cluster.getIntervalItems(iv1);
        assert( iv1.size() == 2);

        cluster.stepForward();
        assert(cluster.isNull());
    }



}

void testLocalAlignmentParsing() {

    // This is a test for SAM local alignment processing

    using namespace seqan;
    string path(COMMON_DATA_FOLDER + "/local_aln.sam");
    BamStream bamStreamIn( path.c_str() , BamStream::READ);
    BamAlignmentRecord record;

    assert(isGood(bamStreamIn));

    while (!atEnd(bamStreamIn))
    {

        assert (readRecord(record, bamStreamIn) == 0);
        cout << record.qName << endl;

        QueryAlignment aln = LocalAlignmentStore::getLocalAlignment(record, bamStreamIn._nameStore, 5);
        if (record.qName == "r0") {
            assert(aln.refStart == 13);
            assert(aln.refId == "ref1");
            assert(aln.length == 61);
            assert(aln.queryStart == 14);
            assert(aln.positiveStrand == false);
        }else if (record.qName == "r1") {
            assert(aln.refStart == 48);
            assert(aln.length == 51);
        } else if (record.qName == "r2") {
            assert(aln.refStart == 99);
            assert(aln.length == 30);
        } else if (record.qName == "r3") {
            assert(aln.refStart == 99);
            assert(aln.length == 55);
            assert(aln.queryStart == 16);
        } else {
            // max additional clipped regions size is more than 5
            assert(aln.refStart == -1);
        }

    }


}

void testFusionsWriting() {

    vector<Fusion> fusions;
    string pathToFusions(COMMON_DATA_FOLDER + "/../annotate_fusions/clusters_004.txt");
    cerr << pathToFusions;
    bool ok = Fusion::readFusionClusters(fusions, pathToFusions);
    assert(ok);
    assert(fusions.size() == 5);

    string outPath("/tmp/fusion_seqs.fa");
    bool ok2 = Fusion::writeFusionSeqsAsFasta(fusions, outPath, 10);
    assert(ok2);

    using namespace seqan;

    CharString id;
    Dna5String seq;
    SequenceStream seqStream(outPath.c_str());
    readRecord(id, seq, seqStream);
    assert(id == "1/1");
    assert(seq == "CCCTAGCGGA");
    readRecord(id, seq, seqStream);
    assert(id == "1/2");
    assert(seq == "CCAGGTGCCT" );
    readRecord(id, seq, seqStream);
    assert(id == "16/1");
    assert(seq == "CAACACCATT" );
    readRecord(id, seq, seqStream);
    assert(id == "16/2");
    assert(seq == "CTGCAGGGTG" );
    readRecord(id, seq, seqStream);
    assert(id == "20/1");
    assert(seq == "CATCTTTTTC" );
    readRecord(id, seq, seqStream);
    assert(id == "20/2");
    assert(seq == "TCCCCATTGG" );





}


void testFindMatesTask() {


    AnnotationModel model;
    assert(model.loadTranscripts(COMMON_DATA_FOLDER + "/simple.gtf"));

    // Searching for second mates
    testFindMates(model,
                  COMMON_DATA_FOLDER + "/genomic_aln_1.sam",
                  true,
                  COMMON_DATA_FOLDER + "/transcript_aln_2.sam");

    // Searching for first mates
    testFindMates(model,
                  COMMON_DATA_FOLDER + "/genomic_aln_2.sam",
                  false,
                  COMMON_DATA_FOLDER + "/transcript_aln_1.sam");


}

int main(int argc, char const ** argv)
{

    if (argc == 2) {
        COMMON_DATA_FOLDER = argv[1];
    }

#ifdef DEBUG

    cout << "Running in DEBUG mode" << endl;
    cout << "Infusion v" << INFUSION_VERSION << endl;

#else

    cout << "Running in RELEASE mode: fail!" << endl;
    assert(0);

    /*AnnotationModel m1;

    PerfTimer t1;
    t1.reset();
    m1.loadTranscripts("/data/infusion.ens65/Homo_sapiens.GRCh37.65.gtf");
    t1.drop();

    cout << "Num transcripts (method 1): " << m1.getTranscripts().size() << endl;
    cout << "Time taken (ms): " << t1.getElapsed() << endl;


    PerfTimer t2;
    m2.loadTranscriptsNew("/data/infusion.ens65/Homo_sapiens.GRCh37.65.gtf");
    t2.drop();
    cout << "Num transcripts (method 2): " << m2.getTranscripts().size() << endl;
    cout << "Time taken (ms): " << t2.getElapsed() << endl;*/

    return -1;

#endif //DEBUG

    testFeature();
    testGffReader();
    testFusionsWriting();
    testAnnotationModel();
    testReference();
    testStringUtils();
    testAlignmentUtils();
    testLocalAlignmentParsing();
    testBoost();
    testFindMatesTask();
    testClusterTree();
    testIntervalTree();


    cout << "\nALL TESTS PASSED!" << endl;

    return 0;
}



