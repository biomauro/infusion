#include <iostream>
#include <boost/algorithm/string.hpp>

#include "FilterFusionsTask.h"
#include "StringUtils.h"

using namespace std;

bool FilterFusionsTask::init()
{

    cerr << "Loading fusions..." << endl;

    if ( !Fusion::readFusionClusters(fusions, inputPath.c_str()) ) {
        cerr << "Failed to read fusions from " << inputPath << endl;
        return false;
    }


    return true;
}


void FilterFusionsTask::constructFilters() {


    NonCodingRegionFilterConfig regionFilterCfg;
    regionFilterCfg.allowIntergenic = settings.allowIntergenic;
    regionFilterCfg.allowIntronic = settings.allowIntronic;
    regionFilterCfg.allowNonCoding = settings.allowNonCoding;
    {
        vector<string> biotypesList = StringUtils::split(settings.excludedBioTypes,',');

        foreach_ (const string& biotype, biotypesList) {
            regionFilterCfg.excludedBiotypes.insert(biotype);
        }
    }

    FusionFilterPtr nonCodingRegionFilter( new NonCodingRegionFilter(regionFilterCfg,fusionJunctionMap) );
    fusionFilters.push_back(nonCodingRegionFilter);

    FusionFilterPtr minSupportFilter( new MinSupportFilter(fusionJunctionMap, settings.minSupportConfig, settings.isPaired) );
    fusionFilters.push_back(minSupportFilter);

    FusionFilterPtr homologyFilter( new HomologyFilter );
    fusionFilters.push_back(homologyFilter);

    FusionFilterPtr insertSizeFilter( new InsertSizeFilter(settings.minProperlyAlignedPairsRate));
    fusionFilters.push_back(insertSizeFilter);

    FusionFilterPtr splitAligmentsFilter( new SplitCoverageFilter(settings.minUniqueAlignmentRate, settings.minUniqueSplitReads));
    fusionFilters.push_back(splitAligmentsFilter);

    if (settings.paralogsPath.length() > 0) {
        FusionFilterPtr paralogsFilter( new ParalogsFilter(settings.paralogsPath));
        fusionFilters.push_back(paralogsFilter);
    }

    FusionFilterPtr meanSplitPosFilter( new SplitPositionFilter(settings.splitPosDev) );
    fusionFilters.push_back(meanSplitPosFilter);

    FusionFilterPtr homogeneityFilter( new HomogeneityFilter(settings.minHomogeneityWeight, settings.requiredHomogeneityWeight) );
    fusionFilters.push_back(homogeneityFilter);

    FusionFilterPtr coverageContextFilter( new CoverageContextFilter(settings.minBckgReads) );
    fusionFilters.push_back(coverageContextFilter);

}



static void addFilterAnnotation(Fusion& fusion, const string& filterName ) {
    string filterStr = fusion.getAnnotationValue(ANNOTATION_FILTERS, "");
    if (filterStr.length() > 0) {
        filterStr += ",";
    }
    filterStr += filterName;
    fusion.setAnnotationValue( ANNOTATION_FILTERS, filterStr );
}


int FilterFusionsTask::run()
{
    if (!init()) {
        return -1;
    }

    constructFilters();

    cerr << "Filtering fusions..." << endl;

    size_t numFusions = fusions.size();

    for (size_t i = 0; i < numFusions; ++i) {

        Fusion& fusion = fusions[i];

        // apply fitlers
        foreach_ (FusionFilterPtr filter, fusionFilters) {
            FusionFilter::Result res = filter->process(fusion);
            if (!res.passed) {
                string filterMessage = res.message.size() > 0 ? res.message : filter->getName();
                addFilterAnnotation(fusion, filterMessage );
            }
        }

        fusion.calcScore(1.0, 1.0);
    }

    // sort by fusion accumulative score
    sort(fusions.begin(), fusions.end(), FusionCompare() );

    Fusion::writeFusionsReport(fusions, settings.outputPath, true );
    string detailedReportPath = boost::replace_first_copy(settings.outputPath,".txt", ".detailed.txt");
    Fusion::writeDetailedFusionsReport(fusions, detailedReportPath, true );

    string detailedFullReportPath = boost::replace_first_copy(settings.outputPath, ".txt", ".detailed.full.txt");
    Fusion::writeDetailedFusionsReport(fusions, detailedFullReportPath,false );

    //string htmlReportPath = boost::replace_first_copy(settings.outputPath, ".txt", ".detailed.html");
    //Fusion::writeHtmlReport(fusions, htmlReportPath, true);

    return 0;

}
