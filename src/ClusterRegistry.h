#ifndef CLUSTER_REGISTRY_H
#define CLUSTER_REGISTRY_H

#define DEFAULT_IMPL "impl-default"
#define STANDARD_IMPL "impl-std"

#include "PairedAlignmentsStore.h"
#include "RescueLocalAlignmentsTask.h"
#include "Fusion.h"

struct ClusterRegistryConfig {
    LibraryProtocol protocol;
    int pairedClusterOverlapTolerance;
    int insertSizeTolerance;
    int fuzzyBreakpointTolerance;
    bool pairedReads;
    bool mergeIntronSeparated;
    bool mergeIntersecting;
    bool splitClustersByBreakpoint;
    bool referenceSeqAvailable;

    ClusterRegistryConfig() {
        protocol = LibraryProtocol::NonStrandSpecific;
        pairedClusterOverlapTolerance = 10;
        insertSizeTolerance = 50;
        fuzzyBreakpointTolerance = 5;
        pairedReads = true;
        mergeIntronSeparated = false;
        mergeIntersecting = false;
        splitClustersByBreakpoint = true;
        referenceSeqAvailable = false;

    }

};


class ClusterRegistry
{
protected:
    std::vector<Fusion> fusions;
    ClusterRegistryConfig config;
public:
    static const int MAX_INTRON_LENTH = 20000;
public:
    ClusterRegistry() {  }
    /* Analyze breakpoints to see if they form new clusters or belong to existing one */
    virtual void processBreakpoints(const std::vector<BreakpointCandidatePtr>& bpntList) = 0;
    /* This method should be called when all breakpoints are processed*/
    virtual void finalizeClustering() = 0;
    /* Analyze local alignments to see if they support any existing fusion */
    virtual void findSupporters(const vector<ReadLocalAlignment>&alns, int count, const FindSupportingReadsConfig& cfg) = 0;
    /* This function creates and outputs fusions */
    virtual void outputFusions(const string& outFileName) = 0;
    virtual ~ClusterRegistry() {}
    virtual void dumpClusters(const string& pathToOuputDir) = 0;
    void writeFusionsGff(const char* path);
    void setConfiguration(const ClusterRegistryConfig& cfg) { config = cfg; }

    const std::vector<Fusion>& getFusions() const { return fusions; }

    static ClusterRegistry* createInstance(const std::string& implName = DEFAULT_IMPL);

};


#endif // CLUSTER_REGISTRY_H
