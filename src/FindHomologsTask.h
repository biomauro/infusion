#ifndef FIND_HOMOLOGS_TASK_H
#define FIND_HOMOLOGS_TASK_H

#include "AnnotationModel.h"
#include "GenomicOverlapDetector.h"
#include "Fusion.h"

struct FindHomologsTaskConfig {
    string transcriptomicAlignmentPath;
    string clusterAlignmentPath;
    string genomicAlignmentPath;
    string inputFusionsPath;
    string outputFusionsPath;
    string gtfPath;
    int meanIntronLength;
    FindHomologsTaskConfig() : meanIntronLength(10000) {}
};

class FindHomologsTask
{
    map<int,int> fusionIdxMap;
    vector<Fusion> fusions;
    AnnotationModel geneModels;
    GenomicOverlapDetector<ExonPtr> geneOverlapper;
    FindHomologsTaskConfig cfg;
    bool analyzeTranscriptomicAlignments();
    bool analyzeGenomicAlignments();
    void overlapFusionIntervalWithGenes(Fusion& fusion, int idx);
    void createGeneOverlapper();
    bool foundHomolog(Fusion& f, int intervalIdx, const string& transcriptId);
    bool foundHomolog(Fusion&f, int intervalIdx, const string& refId, int pos);
public:
    FindHomologsTask(const FindHomologsTaskConfig& config) : cfg(config) {}
    int run();
};

#endif // FIND_HOMOLOGS_TASK_H
