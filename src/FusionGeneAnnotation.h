#ifndef FUSION_GENE_ANNOTATION_H
#define FUSION_GENE_ANNOTATION_H

#include "AnnotationModel.h"
#include "Global.h"

struct RegionDesc {
    string bioType;
    string geneName;
    char transcriptStrand;
    string transcriptName;
    string exonId;
    string secondExonId; // this is valid only for introns
};


struct RegionDescCompare {
    bool operator()(const RegionDesc& l, const RegionDesc& r) {
        return l.transcriptName < r.transcriptName;
    }
};

struct GenomicAnnotation {

    const static string INTRON;
    const static string EXON;
    const static string INTERGENIC_REGION;

    string featureType;
    vector<RegionDesc> features;
    bool breakOnExon;

    string getTranscriptsStr() const;
    string getGenesStr() const;
    string getBioTypesStr() const;
    char getStrand() const;

    bool isProteinCoding() const;
    bool isIntergenic() const {
        return featureType == INTERGENIC_REGION;
    }
    bool isIntronic() const {
        return featureType == INTRON;
    }

    GenomicAnnotation() : breakOnExon(false) {}

    static GenomicAnnotation parseAnnotation(const string& str);
    static string exonToString(const ExonSet& exons, bool breakOnExonBoundary);
    static string intronToString(const vector<RegionDesc>& introns);

};

#endif // FUSION_GENE_ANNOTATION_H
