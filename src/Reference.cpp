#include <boost/algorithm/string/trim.hpp>

#include "Reference.h"

using std::cerr;
using std::endl;
using seqan::CharString;

bool ReferenceSeqIndexed::load(const string &path)
{
    string indexPath(path + ".fai");
    int loadRes = seqan::read( refIndex, path.c_str(), indexPath.c_str() );
    if (loadRes != 0) {
        std::cerr << "Reference index is not found, building one..." << std::endl;
        int buildRes = seqan::build(refIndex, path.c_str());
        if (buildRes != 0) {
            std::cerr << "ERROR: Could not build the index!\n";
            return false;
        }
    }

    std::cerr << "Loaded reference index." << std::endl;

    return true;

}

seqan::CharString ReferenceSeqIndexed::getSubSequence(const string &refId, int start, int length, bool revCompl)
{
    unsigned idx = 0;

    if (!seqan::getIdByName(refIndex, refId, idx)) {
        std::cerr << "ERROR: reference index has no entry for " << refId << std::endl;
        return "";
    }

    seqan::CharString subseq;
    if (seqan::readRegion(subseq, refIndex, idx, start, start + length) != 0) {
        std::cerr << "ERROR: Could not load region from " << refId << std::endl;
        return "";
    }

    if (revCompl) {
        seqan::reverseComplement(subseq);
    }

    return seqan::toCString(subseq);
}

bool ReferenceSeqIndexed::getWholeSequence(seqan::CharString &sequence, const string &refId)
{
    unsigned idx = 0;

    if (!seqan::getIdByName(refIndex, refId, idx)) {
        std::cerr << "ERROR: reference index has no entry for " << refId << std::endl;
        return false;
    }

    int res = seqan::readSequence(sequence, refIndex, idx);

    return res == 0;
}

bool ReferenceSeqIndexed::includesSequence(const string &refId)
{
    unsigned idx = 0;
    return seqan::getIdByName(refIndex, refId, idx);
}


bool ReferenceSeqInMemory::load(const string &path)
{
    seqan::SequenceStream seqStream( path.c_str() );
    seqan::CharString id;
    seqan::CharString seq;

    if (!isGood(seqStream))
    {
        std::cerr << "Failed to open file " << path << endl;
        return false;
    }

    while (!atEnd(seqStream))
    {
        if (readRecord(id, seq, seqStream) != 0)
        {
            std::cerr << "Failed to read sequence from " << path << endl;
            return false;
        }
        string idStr( toCString(id) );
        // SeqAn is not trimming!
        boost::algorithm::trim(idStr);

        seqMap[idStr] = seq;
    }

    cerr << "Loaded " << seqMap.size() << " sequences" << endl;

    return true;
}

seqan::CharString ReferenceSeqInMemory::getSubSequence(const string &refId, int start, int len, bool revCompl)
{
    if (seqMap.count(refId) == 0) {
        cerr << "Reference entry " << refId << " is not found." << endl;
        return CharString();
    }

    CharString& sequence = seqMap[refId];
    size_t end = start + len;
    if (end > length(sequence)) {
        cerr << "Can not get sub sequence of reference: end is more than sequence size" << endl;
        return CharString();
    }

    if (start < 0) {
        cerr << "Can not get sub sequence of reference: start is less than zero" << endl;
        return CharString();
    }

    auto subseqSegment = seqan::infix( sequence, start, end);
    seqan::CharString subseq(subseqSegment);

    if (revCompl) {
        seqan::reverseComplement(subseq);
    }


    return subseq;

}

bool ReferenceSeqInMemory::getWholeSequence(seqan::CharString &sequence, const string &refId)
{
    if (seqMap.count(refId) > 0) {
        sequence = seqMap[refId];
        return true;
    } else {
        cerr << "Reference entry " << refId << " is not found." << endl;
        return false;
    }
}

bool ReferenceSeqInMemory::includesSequence(const string &refId)
{
    if (seqMap.count(refId) == 0) {
        return false;
    } else {
        return true;
    }
}

