#include "InsertSizeDistr.h"
#include "PairedAlignmentsStore.h"
#include "AlignmentUtils.h"

using namespace seqan;
using namespace std;

#define NON_EXONIC_REGION "non_exonic_region"

PairedAlignmentsStore::PairedAlignmentsStore( const TranscriptSet& transcripts, const StringSet<CharString>& refNameSet,
                                              const PairedAlignmentStoreConfig& config )
    : cfg(config)
{
    ctx.transcripts = transcripts;
    ctx.refNameSet = refNameSet;
    insertSize.resize(cfg.maxInsertSize + 1, 0);
    if (cfg.computeCounts) {
        for ( auto iter = transcripts.begin(); iter != transcripts.end(); ++ iter) {
            countsData[iter->first] = 0;
        }
    }
}



void PairedAlignmentsStore::addAlignment(BamAlignmentRecord &record, bool upstream)
{

    string readName(toCString(record.qName));

    PairedAlignment& aln = alignmentMap[readName];


    BamTagsDict tags(record.tags);
    uint idx = -1;
    bool genomicAln = findTagKey(idx, tags, CharString("ZG"));

    bool ok = true;
    SplicedAlignment splicedAln = genomicAln ? getSplicedAlignmentFromGenomic(record) : getSplicedAlignment(record, ok);

    if (!ok) {
        return;
    }

    if (upstream) {
        aln.addUpstreamAlignment(splicedAln);
    } else {
        aln.addDownstreamAlignment(splicedAln);
    }

}

void PairedAlignmentsStore::findBreakpointCandidates(vector<BreakpointCandidatePtr> &result, const BreakpointConstraints& bc)
{

    for (auto iter = alignmentMap.begin(); iter != alignmentMap.end(); ++iter) {
        
        PairedAlignment& aln =iter->second;

        const unordered_set<SplicedAlignment>& uniqFirstMates = aln.getFirstMates();
        const unordered_set<SplicedAlignment>& uniqSecondMates =aln.getSecondMates();

        vector<BreakpointCandidatePtr> candidatesForOneRead;

        foreach_ (const SplicedAlignment& firstMateAln, uniqFirstMates) {
            QueryAlignment aln1 = toQueryAlignment(firstMateAln);
            foreach_ (const SplicedAlignment& secondMateAln, uniqSecondMates) {
                QueryAlignment aln2 = toQueryAlignment(secondMateAln);

                if (!BreakpointCandidate::passConstraints(aln1, aln2, bc)) {

                    if (cfg.computeCounts) {
                        countsData[ firstMateAln.transcriptId ]++;
                        countsData[ secondMateAln.transcriptId ]++;
                    }

                    if (firstMateAln.transcriptId.length() > 0 && secondMateAln.transcriptId.length() > 0) {
                        string geneName1 = ctx.transcripts[firstMateAln.transcriptId]->getGeneName();
                        string geneName2 = ctx.transcripts[secondMateAln.transcriptId]->getGeneName();
                        if (geneName1 == geneName2) {
                            collectInsertSize(aln1, aln2);
                        }
                    }

                    continue;
                }

                if (aln.getOrientation() == PairOrientation::FR) {
                    QueryAlignment::revertStrand(aln2);
                } else if (aln.getOrientation() == PairOrientation::RF) {
                    QueryAlignment::revertStrand(aln1);
                }

                BreakpointCandidatePtr candidate( new BreakpointCandidate() );
                candidate->readName = iter->first;
                candidate->flags |= BreakpointFlags_OriginPaired;
                candidate->alns.push_back(aln1);
                candidate->alns.push_back(aln2);

                candidatesForOneRead.push_back(candidate);

            }
        }

        if (candidatesForOneRead.size() > 1) {
            foreach_ (BreakpointCandidatePtr bp, candidatesForOneRead) {
                bp->flags |= BreakpointFlags_Multimapped;
            }
        }

        result.insert(result.end(), candidatesForOneRead.begin(), candidatesForOneRead.end());

    }



}

/*QueryAlignment PairedAlignmentsStore::toQueryAlignment(const BamAlignmentRecord &r, string& geneName, bool& ok)
{
    QueryAlignment aln;

    // TODO: toCString() conversion occurs too often, this could result in performance problems
    string transcriptName = toCString(ctx.refNameSet[ r.rID ]);

    TranscriptSet::iterator it = ctx.transcripts.find(transcriptName);
    if ( it  == ctx.transcripts.end()) {
        cerr << "Failed to find transcript sequence " << transcriptName << ", ignoring read " << r.qName << endl;
        ok = false;
        return aln;
    }

    const TranscriptPtr& transcript = it->second;
    geneName = transcript->getGeneName();

    int rLen = getAlignmentLengthInRef(r);

    aln.queryStart = 0;
    aln.refId = transcript->getRefName();

    vector<GenomicInterval> intervals = transcript->getGenomicIntervals(r.beginPos, rLen);
    assert(intervals.size() > 0);

    bool firstOfPair = hasFlagFirst(r);

    const GenomicInterval& lastInterval = pickNearestToBreakpointInterval(intervals, firstOfPair, transcript->isPositiveStranded());
    aln.refStart =  lastInterval.begin;
    aln.length = lastInterval.length();

    //aln.refStart = transcript.getGenomicPos( transcript.isPositiveStranded() ? r.pos : r.pos + rLen - 1 );

    //aln.length = rLen;
    bool positiveStrand = !hasFlagRC(r);
    aln.positiveStrand = transcript->isPositiveStranded() ? positiveStrand : !positiveStrand;

    return aln;
}*/

QueryAlignment PairedAlignmentsStore::toQueryAlignment(const SplicedAlignment &splicedAln)
{
    QueryAlignment qAln;

    qAln.refStart = splicedAln.interval.begin;
    qAln.length = splicedAln.interval.length();
    // TODO: is this always true?
    qAln.queryStart = 0;
    qAln.refId = splicedAln.interval.refId;
    qAln.positiveStrand = splicedAln.interval.isPositiveStrand;
    qAln.score = splicedAln.score;

    return qAln;
}

bool PairedAlignmentsStore::initBreakpointCandidatesOutput(const string& path)
{
    outfile.open(path, ios_base::out);

    // header
    outfile << "#name\tseq1\tpos1\tstrand1\tlocal_pos1\tlength1\tseq2\tpos2\tstrand2\tlocal_pos2\tlength2\torigin";
    outfile << endl;

    return outfile.is_open();

}

void PairedAlignmentsStore::writeBreakpointCandidates(const vector<BreakpointCandidatePtr> &candidates)
{

    // TODO: get rid of this copy-paste by creating object to load and write breakpoint candidates
    assert(outfile.is_open());

    // contents
    for (size_t i = 0, count = candidates.size(); i < count; ++i ) {
        const BreakpointCandidatePtr& c = candidates.at(i);
        BreakpointCandidate::writeCandidate(c, outfile);
    }

}

void PairedAlignmentsStore::writeInsertSizeDistribution(const string& path)
{
    std::ofstream outstream;

    outstream.open(path, ios_base::out);

    assert(outstream.is_open());

    for (size_t i = 0; i < insertSize.size(); ++i ) {
        outstream << insertSize.at(i) << endl;
    }


}




void PairedAlignmentsStore::writeInsertSizeDistributionParams(const string& path)
{

    DistrParams originalParams, modifiedParams;

    vector<int> originalDistr;
    for ( size_t is = 0; is < insertSize.size(); ++is) {
        int freq = insertSize.at(is);
        for (int j = 0; j < freq; ++j) {
            originalDistr.push_back(is);
        }
    }

    DistrParams::calcParamsFromSortedDistr(originalParams, originalDistr);

    vector<int> modifiedDistr;
    foreach_(int val, originalDistr) {
        if (val <= originalParams.median*2) {
            modifiedDistr.push_back(val);
        } else {
            break;
        }
    }

    DistrParams::calcParamsFromSortedDistr(modifiedParams, modifiedDistr);

    std::ofstream outstream;

    outstream.open(path, ios_base::out);

    assert(outstream.is_open());

    outstream << "Original distribution: " << originalParams << endl;
    outstream << "Modified distribution: " << modifiedParams << endl;


}



void PairedAlignmentsStore::writeGeneCounts(const string& path)
{
    std::ofstream outstream;

    outstream.open(path, ios_base::out);

    assert(outstream.is_open());

    outstream << "#transcript_id\tgene_name\tlength\tread_count\tRPKM\n";

    for (auto iter = countsData.begin(); iter != countsData.end(); ++iter) {
        const string& tID = iter->first;
        if (tID.length() == 0) {
            continue;
        }
        int readCount = iter->second;
        const TranscriptPtr& t = ctx.transcripts[tID];

        outstream << tID << "\t" << t->getGeneName();
        outstream << "\t" << t->getLength() << "\t" << readCount;
        double k = 1000000000. / (double) cfg.numReads;
        if (cfg.numReads > 0) {
            double rpkm = (readCount*k) / t->getLength();
            assert(rpkm >= 0);
            outstream << "\t" << rpkm;
        }
        outstream << endl;
    }
}

void PairedAlignmentsStore::collectInsertSize(const QueryAlignment &aln1, const QueryAlignment &aln2)
{
    int dist = aln1.refStart < aln2.refStart ? aln2.refStart - aln1.inRefEnd() : aln1.refStart - aln2.inRefEnd();

    if (dist >= 0 && dist <= cfg.maxInsertSize) {
        insertSize[dist] += 1;
    }
#ifdef DEBUG
    else {
        //cerr << "Insert size less than zero in record" << endl;
    }
#endif // DEBUG

}

int PairedAlignmentsStore::getAlignmentScore(const BamAlignmentRecord &r)
{

    int score = length(r.seq)*cfg.matchBonus;
    BamTagsDict tags;
    setHost(tags, r.tags);
    unsigned tagIdx = 0;
    bool ok = false;
    int numMismatches = 0;
    if (findTagKey(tagIdx, tags, "XM")) {
        ok = extractTagValue(numMismatches, tags, tagIdx);
        if (ok) {
            score -= cfg.mismatchScore*numMismatches;
        }

    }

    return score;
}

SplicedAlignment PairedAlignmentsStore::getSplicedAlignment(const BamAlignmentRecord &rec, bool& alnOk)
{

    SplicedAlignment splicedAln;

    // TODO: toCString() conversion occurs too often, this could result in performance problems
    string transcriptId = toCString(ctx.refNameSet[ rec.rID ]);

    TranscriptSet::iterator it = ctx.transcripts.find(transcriptId);
    if ( it  == ctx.transcripts.end()) {
        cerr << "Failed to find transcript sequence " << transcriptId << ", ignoring read " << rec.qName << endl;
        alnOk = false;
        return splicedAln;
    }

    /*cerr << rec.qName << endl;
    if (rec.qName == "ZFYVE16-NDUFS4.fa.fastq.000000003") {
        cerr << "BINGO!" << endl;
    }*/


    const TranscriptPtr& transcript = it->second;
    splicedAln.transcriptId = transcriptId;

    int rLen = getAlignmentLengthInRef(rec);

    vector<GenomicInterval> intervals = transcript->getGenomicIntervals(rec.beginPos, rLen);
    assert(intervals.size() > 0);

    bool firstOfPair = hasFlagFirst(rec);
    bool positiveStrand = !hasFlagRC(rec);
    bool genomicStrandIsPositive = positiveStrand == transcript->isPositiveStranded();


    splicedAln.interval = AlignmentUtils::pickNearestToBreakpointInterval(intervals, firstOfPair, genomicStrandIsPositive/*transcript->isPositiveStranded()*/);

    // TODO: Very small alignments do not make sense and could be misleasing.
    // One has to keep the big alignment, and try to keep it to the cluster.
    if (splicedAln.interval.length() < cfg.minAnchorSize) {
        alnOk = false;
        return splicedAln;
    }
    splicedAln.interval.refId = transcript->getRefName();

    splicedAln.interval.isPositiveStrand = transcript->isPositiveStranded() ? positiveStrand : !positiveStrand;

    splicedAln.score = getAlignmentScore(rec);

    alnOk = true;

    return splicedAln;

}


SplicedAlignment PairedAlignmentsStore::getSplicedAlignmentFromGenomic(const BamAlignmentRecord &rec)
{
    SplicedAlignment splicedAln;

    int rLen = getAlignmentLengthInRef(rec);

    splicedAln.interval.isPositiveStrand = !hasFlagRC(rec);

    splicedAln.interval.refId = toCString(ctx.refNameSet[ rec.rID ]);
    splicedAln.interval.begin = rec.beginPos;
    splicedAln.interval.end = rec.beginPos + rLen - 1;
    splicedAln.score = getAlignmentScore(rec);


    return splicedAln;
}


