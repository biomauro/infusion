#ifndef BREAKPOINT_CANDIDATE_H
#define BREAKPOINT_CANDIDATE_H

#include <vector>
#include <ostream>
#include <boost/shared_ptr.hpp>

#include <seqan/sequence.h>

#include "QueryAlignment.h"

using seqan::CharString;
using seqan::StringSet;

struct BreakpointConstraints {
    bool alignmentsCanIntersectLocally;
    int minDistBetweenAlignInRef;
    int maxDistBetweenAlignInRead;
    int maxIntersectionSizeInRead;
    std::vector<std::string> ignoredSequences;
    BreakpointConstraints() : alignmentsCanIntersectLocally(false) {
        maxDistBetweenAlignInRead = 1;
        maxIntersectionSizeInRead = 1;
        minDistBetweenAlignInRef = 10000;
    }
};

class BreakpointCandidate;
typedef boost::shared_ptr<BreakpointCandidate> BreakpointCandidatePtr;

enum BreakpointOrigin {
    OriginUnknown,
    OriginLocal,
    OriginLocalMateOne,
    OriginLocalMateTwo,
    OriginPaired,
    OriginLocalRedeemed,
    OriginLocalRedeemedMateOne,
    OriginLocalRedeemedMateTwo
};


enum BreakpointFlags {
    BreakpointFlags_Empty = 0x0,
    BreakpointFlags_OriginLocalFirst = 0x1,
    BreakpointFlags_OriginLocalSecond = 0x2,
    BreakpointFlags_OriginLocal = 0x3,
    BreakpointFlags_OriginPaired = 0x4,
    BreakpointFlags_RedeemedReadAlignment = 0x8,
    BreakpointFlags_HasProperPair = 0x10,
    BreakpointFlags_Multimapped = 0x20,
    BreakpointFlags_FirstSeparatedByIntron = 0x40,
    BreakpointFlags_SecondSeparatedByIntron = 0x80,
    BreakpointFlags_SeparatedByIntron = 0xc0

};


struct BreakpointCandidate {

    static const std::string ORIGIN_LOCAL;
    static const std::string ORIGIN_LOCAL_MATE_1;
    static const std::string ORIGIN_LOCAL_MATE_2;
    static const std::string ORIGIN_PAIRED;
    static const std::string ORIGIN_LOCAL_REDEEMED;
    static const std::string ORIGIN_LOCAL_REDEEMED_MATE_1;
    static const std::string ORIGIN_LOCAL_REDEEMED_MATE_2;
    static const std::string ORIGIN_UNKNOWN;
    static const std::string TAG_ALN_SCORE;

    const std::string& originToStr() const {
        if (isOriginLocal()) {
            if ( isRedeemed() ) {
                return ORIGIN_LOCAL_REDEEMED;
            } else {
                return ORIGIN_LOCAL;
            }
        } else if (isOriginPaired()) {
            return ORIGIN_PAIRED;
        } else {
            assert(0);
        }

    }

    void setOriginFromStr(const std::string& orignStr) {
        if (orignStr == ORIGIN_LOCAL) {
            flags |= BreakpointFlags_OriginLocal;
        } else if (orignStr == ORIGIN_PAIRED) {
            flags |= BreakpointFlags_OriginPaired;
        } else if (orignStr == ORIGIN_LOCAL_REDEEMED) {
            flags |= (BreakpointFlags_OriginLocal | BreakpointFlags_RedeemedReadAlignment);
        }

    }

    BreakpointCandidate() {
        flags = BreakpointFlags_Empty;
    }

    CharString readName;
    uint flags;
    std::vector<QueryAlignment> alns;


    static char strandToChar(bool positiveStrand) {
        return positiveStrand ? '+' : '-';
    }

    void sortAlignments() {
        // TODO: what about flags?
        std::sort(alns.begin(), alns.end());
    }

    void swapAlignments() {
        std::swap(alns[0],alns[1]);
    }

    void sortAlignmentsAccordingToInterval(const GenomicInterval& firstInterval);

    bool isOriginLocalFirstMate() const {
        return (flags & BreakpointFlags_OriginLocalFirst) == BreakpointFlags_OriginLocalFirst;
    }

    void setOriginLocalFirstMate() {
        flags |= BreakpointFlags_OriginLocalFirst;
    }

    bool isOriginLocalSecondMate() const {
        return (flags & BreakpointFlags_OriginLocalSecond) == BreakpointFlags_OriginLocalSecond;
    }

    void setOriginLocalSecondMate() {
        flags |= BreakpointFlags_OriginLocalSecond;
    }

    bool isOriginLocal() const {
        return (flags & BreakpointFlags_OriginLocal) > 0;
    }

    bool isOriginPaired() const {
        return (flags & BreakpointFlags_OriginPaired) == BreakpointFlags_OriginPaired;
    }

    bool isRedeemed() const {
        return (flags & BreakpointFlags_RedeemedReadAlignment) == BreakpointFlags_RedeemedReadAlignment;
    }

    bool isMultimapped() const {
        return (flags & BreakpointFlags_Multimapped) == BreakpointFlags_Multimapped;
    }

    bool isFirstSeparatedByIntron() const {
        return (flags & BreakpointFlags_FirstSeparatedByIntron) == BreakpointFlags_FirstSeparatedByIntron;
    }

    bool isSecondSeparatedByIntron() const {
        return (flags & BreakpointFlags_SecondSeparatedByIntron) == BreakpointFlags_SecondSeparatedByIntron;
    }

    bool hasProperPair() const {
        return (flags & BreakpointFlags_HasProperPair) == BreakpointFlags_HasProperPair;
    }

    bool isSeparatedByIntron() const {
        return (flags & BreakpointFlags_SeparatedByIntron ) > 0;
    }

    void setSeparatedByIntron(int localIdx) {
        flags |= (BreakpointFlags_FirstSeparatedByIntron << localIdx);
    }


    int getDownstreamAlnIndex() const {
        return alns[0].queryStart < alns[1].queryStart ? 1 : 0;
    }

    int getUpstreamAlnIndex() const {
        return alns[0].queryStart < alns[1].queryStart ? 0 : 1;
    }

    static bool readCandidates(std::vector<BreakpointCandidatePtr>& candidates, const char* path );

    static void writeCandidates(const std::vector<BreakpointCandidatePtr>& candidates, const char* path );

    static BreakpointCandidatePtr parseCandidateRecord(const std::string& line);

    static void writeCandidate(const BreakpointCandidatePtr& c, std::ostream& outfile );

    static bool passConstraints(const QueryAlignment& aln1, const QueryAlignment& aln2, const BreakpointConstraints& bc);

};



#endif // BREAKPOINT_CANDIDATE_H
