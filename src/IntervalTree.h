#ifndef INTERVAL_TREE_H
#define INTERVAL_TREE_H

#include <iostream>
#include <sstream>
#include <vector>
#include <cassert>


template <typename num_t>
std::string num_to_string( num_t num ) {
     std::ostringstream ss;
     ss << num;
     return ss.str();
}

/*
 * An augmented interval tree based on RBK-Tree
 * Implementation inspired by Cormen et. al "Alogrithms: design & analysis"
 *
 * All intervals are treated as closed intervals.
 * Interval [a,b] starts at a and ends at b and has size of b-a + 1
 * Intervals with b < a are not supported!
 *
 */

template<typename TValue>
class IntervalTree {
public:

    class Node {
        friend class IntervalTree;
    private:
        Node *left, *right, *parent;
        int maxEnd;
        bool isBlack;
        TValue value;
        size_t index;
        int begin, end;
    public:
        Node(size_t idx, int beginPos, int endPos, const TValue& val)
            : left(NULL),right(NULL),parent(NULL),isBlack(false),index(idx)
        {
            value = val;
            begin = beginPos;
            end = endPos;
        }

        int getStart() const { return begin; }
        int getEnd() const { return end; }
        int getMaxEnd() const { return maxEnd; }
        bool isRed() const { return !isBlack;}
        size_t getIndex() const { return index; }
        const TValue& getValue() const { return value; }
        std::string colorToStr() const { return ( isBlack ? "black" : "red" ); }
        void print() const {
            std::string leftNode = left == NULL ? "null" : num_to_string(left->index);
            std::string rightNode = right == NULL ? "null" : num_to_string(right->index);

            std::cout << "Node "<< index << ": color=" << colorToStr() << ", value=" << ", start=" << getStart();
            std::cout << ", end = " << getEnd() << ", maxEnd = " << maxEnd << ", kids = (" << leftNode << "," << rightNode << ")" << std::endl;
        }
        void outputDot(std::ostream &os, int& nullCounter) const {

        }
    };

private:
    size_t currentIndex;
    Node* root;
    Node* nullNode;

    bool overlaps(int begin1, int end1, int begin2, int end2) const {
        int sd = begin1 - begin2;
        return (sd >= 0) ? sd < (end2 - begin2 + 1) : -sd < (end1 - begin1 + 1);
    }

    void leftRotate(Node *x)
    {

        Node* y = x->right;
        if (y == nullNode) {
            return;
        }

        x->right = y->left;
        if (y->left != nullNode) {
            y->left->parent = x;
        }

        y->parent = x->parent;
        if (x->parent == nullNode) {
            root = y;
        } else {
            if ( x == x->parent->left) {
                x->parent->left = y;
            } else {
                x->parent->right = y;
            }
        }

        y->left = x;
        x->parent = y;

        setMaxEnd(x);
        setMaxEnd(y);

    }

    void rightRotate(Node *y)
    {
        auto x = y->left;
        if (x == nullNode) {
            return;
        }

        y->left = x->right;
        if (x->right != nullNode) {
            x->right->parent = y;
        }

        x->parent = y->parent;
        if (y->parent == nullNode) {
            root = x;
        } else {
            if (y == y->parent->left) {
                y->parent->left = x;
            } else {
                y->parent->right = x;
            }
        }

        x->right = y;
        y->parent = x;

        setMaxEnd(y);
        setMaxEnd(x);


    }

    void fixInsert(Node *node)
    {
        fixMaxEnd(node);
        while (node->parent != nullNode && !node->parent->isBlack) {
            auto grandFather = node->parent->parent;
            if (node->parent == grandFather->left) {
                Node* uncle = grandFather->right;
                if ( uncle != nullNode && !uncle->isBlack ) {
                    node->parent->isBlack = true;
                    uncle->isBlack = true;
                    grandFather->isBlack = false;
                    node = grandFather;
                } else {
                    if (node == node->parent->right) {
                        node=node->parent;
                        leftRotate(node);
                    }
                    node->parent->isBlack = true;
                    node->parent->parent->isBlack = false;
                    rightRotate(grandFather);
                }
            } else {
                Node* uncle = grandFather->left;
                if (uncle != nullNode && !uncle->isBlack) {
                    node->parent->isBlack = true;
                    uncle->isBlack = true;
                    grandFather->isBlack = false;
                    node = grandFather;
                } else {
                    if (node == node->parent->left) {
                        node = node->parent;
                        rightRotate(node);
                    }
                    node->parent->isBlack = true;
                    node->parent->parent->isBlack = false;
                    leftRotate(grandFather);
                }
            }
        }

        root->isBlack = true;

    }

    void setMaxEnd(Node* node)
    {
        node->maxEnd = node->getEnd();
        if (node->left != nullNode) {
            node->maxEnd = std::max(node->left->maxEnd, node->maxEnd);
        }

        if (node->right != nullNode) {
            node->maxEnd = std::max(node->right->maxEnd, node->maxEnd);
        }
    }

    void fixMaxEnd(Node *node)
    {
        do  {
            setMaxEnd(node);
        } while ( (node = node->parent) != nullNode );
    }


    void print(Node *node) const
    {
        node->print();
        if (node->left != nullNode) {
            print(node->left);
        }

        if (node->right != nullNode) {
            print(node->right);
        }
    }

    void deleteNode(Node* node) {
        if (node->left != nullNode) {
            deleteNode(node->left);
            node->left = nullNode;
        }

        if (node->right != nullNode) {
            deleteNode(node->right);
            node->right = nullNode;
        }

        delete node;
        --currentIndex;
    }


    void outputDot(std::ostream &os, Node *node, int& nullCounter) const {
        node->outputDot(os,nullCounter);
        if (node->left != nullNode) {
            outputDot(os,node->left,nullCounter);
        }

        if (node->right != nullNode) {
            outputDot(os,node->right,nullCounter);
        }

    }


public:

    IntervalTree() {
        nullNode = NULL;
        root = nullNode;
        currentIndex = 0;
    }

    ~IntervalTree() {
        if (root != nullNode) {
            deleteNode(root);
            root = nullNode;
        }
        assert(currentIndex == 0);
    }

    Node* getRoot() { return root; }

    size_t getSize()  const { return currentIndex; }

    void insertInterval(int beginPos, int endPos, const TValue &val)
    {

        assert(beginPos <= endPos);

        // TODO: where is it deleted? Clean up!
        Node* newNode = new Node(++currentIndex, beginPos, endPos, val);

        Node* node = root;
        Node* nodeParent = nullNode;

        while (node != nullNode) {
            nodeParent = node;
            if (node->getStart() > beginPos) {
                node = node->left;
                nodeParent->maxEnd = std::max(nodeParent->maxEnd, newNode->maxEnd);
            } else {
                node = node->right;
                newNode->maxEnd = std::max(nodeParent->maxEnd, newNode->maxEnd);
            }
        }

        if (nodeParent == nullNode) {
            root = newNode;
        } else {
            if ( newNode->getStart() < nodeParent->getStart() ) {
                nodeParent->left = newNode;
            } else {
                nodeParent->right = newNode;
            }
        }

        newNode->isBlack = false;
        newNode->left = nullNode;
        newNode->right = nullNode;
        newNode->parent = nodeParent;

        fixInsert(newNode);

    }

    // Specialization when value is interval itself

    void insertInterval(const TValue& val) {
        insertInterval(val.begin, val.end, val);
    }

    Node* overlap(int startPos, int endPos) const {
        Node* node = root;
        Node* result = nullNode;
        while (node != nullNode && node->getMaxEnd() >= startPos) {
            if ( overlaps(node->getStart(), node->getEnd(), startPos, endPos ) ) {
                result = node;
                node = node->left;
            } else {
                if (node->left != nullNode && node->left->getMaxEnd() >=  startPos) {
                    node = node->left;
                }   else {
                    if ( node->getStart() > endPos ) {
                        break;
                    }
                    node = node->right;
                }
            }

        }

        return result;
    }

    // Note: begin and end are both included into the interval
    // For example interval [x,x] consists of single point x

    void findOverlapers(std::vector<TValue>& results, int startPos, int endPos) const
    {
        const Node* node = root;
        const Node* result = nullNode;
        while (node != nullNode && node->getMaxEnd() >= startPos) {
            if ( overlaps(node->getStart(), node->getEnd(), startPos, endPos ) ) {
                result = node;
                node = node->left;
            } else {
                if (node->left != nullNode && node->left->getMaxEnd() >=  startPos) {
                    node = node->left;
                }   else {
                    if ( node->getStart() > endPos ) {
                        break;
                    }
                    node = node->right;
                }
            }

        }

        if (result == nullNode) {
            return;
        } else {
            results.push_back(result->value);
        }

        node = result;
        while (true) {
            const Node* nextNode = node->right;
            if ( nextNode != nullNode && nextNode->getMaxEnd() >= startPos )  {
                node = nextNode;
                while ( (nextNode = node->left) != nullNode && nextNode->getMaxEnd() >=  startPos ) {
                    node = nextNode;
                }
            } else  {
                nextNode = node;
                while ( (node = nextNode->parent) != nullNode && node->right == nextNode ) {
                    nextNode = node;
                }
            }

            if ( node == nullNode || node->getStart() >  endPos ) {
                break;
            }

            if (node->getStart() <=  endPos &&  startPos <= node->getEnd()) {
                results.push_back(node->value);
            }
        }

    }

    // Specialization when value is interval itself

    void findOverlapers(std::vector<TValue>& results, const TValue& interval) const {
        findOverlapers(results, interval.begin, interval.end);
    }


    Node* stepForward(Node *node) const
    {
        Node* result = nullNode;

        if ( (result = node->right) != nullNode) {
            while (result->left != nullNode ) {
                result = result->left;
            }
        } else {
            result = node->parent;
            if (result == NULL) {
                return result;
            }
            while (node == result->right) {
                if (result == root) {
                    return nullNode;
                }
                node = result;
                result = result->parent;
            }
        }

        return result;
    }



    Node* stepBack(Node *node) const
    {
        Node* result = nullNode;

        if ( (result = node->left) != nullNode)  {
            while (result->right != nullNode ) {
                result = result->right;
            }
        } else {
            result = node->parent;
            if (result == NULL) {
                return result;
            }
            while (node == result->left) {
                if (result == root) {
                    return nullNode;
                }
                node = result;
                result = result->parent;
            }
        }

        return result;

    }

    // TODO: add iterators

    Node* leftMostNode() const {
        Node* node = root;
        Node* res = nullNode;

        while (node != nullNode) {
            res = node;
            node = stepBack(node);
        }

        return res;
    }

    void print() const
    {
        print(root);
    }

    void outputDot(std::ostream &os) const
    {
        int nullCounter = 0;
        os << "digraph IntervalTree {" << std::endl;
        outputDot(os, root, nullCounter);
        os << "}" << std::endl;
    }



};


#endif // INTERVAL_TREE_H
