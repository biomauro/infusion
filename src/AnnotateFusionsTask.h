#ifndef ANNOTATE_FUSIONS_TASK_H
#define ANNOTATE_FUSIONS_TASK_H

#include <boost/scoped_ptr.hpp>

#include "AnnotationModel.h"
#include "GenomicOverlapDetector.h"
#include "AlignmentUtils.h"
#include "Fusion.h"
#include "FusionGeneAnnotation.h"
#include "InsertSizeDistr.h"
#include "Reference.h"


class FusionAnnotator {
public:
    virtual ~FusionAnnotator() {}
    virtual void annotate(Fusion& f) = 0;
};

class ReferenceSeq;


class HomologyAnnotator : public FusionAnnotator {
    boost::scoped_ptr<ReferenceSeq> transcripts;
    AlignmentResult testFusionIntervalForHomology(const Fusion& f, int intervalIndex);
    bool loadTranscripts(const CharString& path);
    CharString getSubsequence(const string& transcriptName, int queryStart, int queryEnd );
public:
    HomologyAnnotator(const string& pathToTranscripts);
    virtual void annotate(Fusion &f);
};

typedef std::pair<int,int> FusionIntervalIdx;

class HomologousGenesAnnotator : public FusionAnnotator {
    std::map<FusionIntervalIdx, std::set<string> > alignmentMap;
    bool hasHomologousAlignments(const Fusion& f, int intervalIndex);
    bool loadAlignments(const string& pathToBamFile);
    bool loadAlignmentsOk;
public:
    HomologousGenesAnnotator(const string& pathToAlignmentFile);
    virtual void annotate(Fusion &f);
};

class CoverageAnnotator : public FusionAnnotator {
    bool loadedInsertSizeParams;
    DistrParams expectedParams;
    int windowSize,minAnchorSize;
    float lowerBound, upperBound;
public:
    CoverageAnnotator(const string& pathToInsertSizeDistrParams);
    virtual void annotate(Fusion &f);
};

class SpliceCiteAnnotator : public FusionAnnotator {
    std::unique_ptr<ReferenceSeqIndexed> ref;
public:
    SpliceCiteAnnotator() {}
    bool loadReference(const string& path);
    virtual void annotate(Fusion &f);
};

class StrandnessAnnotator : public FusionAnnotator {
    float annotateStrandness(const Fusion& f, int idx);
public:
    StrandnessAnnotator() {}
    virtual void annotate(Fusion &f);
};

struct AnnotateFusionsTaskConfig {
    string inputPath, outputPath, geneExpressionPath;
    string pathToGeneModels, pathToTranscripts;
    string pathToFusionSeqAlignments;
    string pathToInsertSizeDistrParams;
    string pathToGenomeFile;

    bool performCoverageAnalysis, analyzeStrandness, writeGeneAnnotations;

    string filteredAnnotation;

    AnnotateFusionsTaskConfig() : performCoverageAnalysis(true), analyzeStrandness(true), writeGeneAnnotations(true) {
    }
};


class AnnotateFusionsTask
{
    const string inputPath;
    const AnnotateFusionsTaskConfig config;

    vector<Fusion> fusions;
    boost::scoped_ptr<FusionAnnotator> homologyAnnotator;
    boost::scoped_ptr<HomologousGenesAnnotator> homologousGenesAnnotator;
    boost::scoped_ptr<CoverageAnnotator> coverageAnnotator;
    boost::scoped_ptr<StrandnessAnnotator> strandnessAnnotator;
    boost::scoped_ptr<SpliceCiteAnnotator> spliceCiteAnnotator;
    AnnotationModel geneModels;
    GenomicOverlapDetector<ExonPtr> exonOverlapper;
    GenomicOverlapDetector<RegionDesc> intronOverlapper;
    GenomicOverlapDetector<TranscriptPtr> transcriptOverlapper;
    map<string,float> geneExpression;

    bool init();
    bool loadGeneExpression();
    bool overlapFusionIntervalWithGenes(Fusion& f, int intervalIndex);
    void inferFusionType(Fusion& f);
    void setExpressionValue(Fusion& f, const string& geneName, const string& annId);
    void detectFusionGenesAdjacency(Fusion& f);
    void createGeneOverlapper();    
public:
    AnnotateFusionsTask(const AnnotateFusionsTaskConfig& cfg)
        : config(cfg) {}
    int run();
};

#endif // ANNOTATE_FUSIONS_TASK_H
