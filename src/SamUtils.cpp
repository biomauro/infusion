#include "SamUtils.h"


using namespace seqan;

int SamUtils::getNumNonMatches(const BamAlignmentRecord &rec)
{

    int res = 0;
    for (size_t i = 0, count = length(rec.cigar); i< count; ++i ) {
        auto cigarElement = rec.cigar[i];
        if (cigarElement.operation == 'M' || cigarElement.operation == '=' ) {
            continue;
        }

        res+= cigarElement.count;

    }

    return res;

}
