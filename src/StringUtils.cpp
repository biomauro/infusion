#include <seqan/find.h>
#include "StringUtils.h"

using namespace std;

vector<string> StringUtils::split(const string& str, char sep)
{
    vector<string> elems;

    std::stringstream ss(str);
    std::string item;
    while(std::getline(ss, item, sep)) {
        elems.push_back(item);
    }


    return elems;
}

string StringUtils::substrAfterPattern(const string &str, const string &pattern)
{
    size_t pos = str.find(pattern);
    if (pos != string::npos) {
        return str.substr(pos + pattern.length());
    } else {
        return "";
    }
}

string StringUtils::substrBeforePattern(const string &str, const string &pattern)
{
    size_t pos = str.find(pattern);
    if (pos != string::npos) {
        return str.substr(0, pos);
    } else {
        return "";
    }
}

bool StringUtils::startsWith(const string &line, const string &prefix)
{
    return (line.compare(0, prefix.length(), prefix) == 0);
}


string StringUtils::trim(const string &line) {
    std::string s(line);
    s.erase(s.find_last_not_of(" \n\r\t")+1);
    return s;
}

vector<string> StringUtils::splitFileName(const string &str)
{
    vector<string> result;
    size_t found = str.find_last_of("/\\");
    result.push_back(str.substr(0,found));
    result.push_back(str.substr(0,found + 1));

    return result;

}

size_t StringUtils::findFlankingPattern(const string &pattern, const string &source, bool searchDownstream, int tolerance)
{
    if (searchDownstream) {
        // find the closest to end region
        size_t localPos = source.find(pattern, source.length() - (pattern.length() + tolerance + 1)  );
        if (localPos != string::npos) {
            return localPos;
        }

    } else {
        // find the closest to start of the region
        size_t localPos = source.rfind(pattern, pattern.length() + tolerance  );
        if (localPos != string::npos) {
            return localPos;
        }
    }

    return string::npos;
}

vector<SearchResult> StringUtils::findDnaPattern(const CharString& query, const CharString &source, int minScore)
{
    using namespace seqan;
    vector<SearchResult> results;

    // TODO:Why do we have to copy to be able perform search!?
    CharString sourceCopy(source);
    CharString queryCopy(query);

    Finder<CharString> directFinder(sourceCopy);
    Pattern<CharString, DPSearch<SimpleScore> > pattern(queryCopy, SimpleScore(0, -2, -1));
    while (seqan::find(directFinder, pattern, minScore)) {
        while (seqan::findBegin(directFinder, pattern, getScore(pattern))) {
            // Question: how do I get the score of the alignment?
            SearchResult r(beginPosition(directFinder),  true);
            results.push_back(r);
        }
    }

    seqan::reverseComplement(queryCopy);
    Pattern<CharString, DPSearch<SimpleScore> > revPattern(queryCopy, SimpleScore(0, -2, -1));

    Finder<CharString> revFinder(sourceCopy);
    while (seqan::find(revFinder, revPattern, minScore)) {
        while (seqan::findBegin(revFinder, revPattern, getScore(revPattern))) {
            SearchResult r(beginPosition(revFinder),  false);
            results.push_back(r);
        }
    }

    return results;
}

vector<SearchResult> StringUtils::findDnaPattern(CharString& query, CharString &source, int minScore,bool revCompl)
{
    using namespace seqan;
    vector<SearchResult> results;

    if (revCompl) {
        reverseComplement(query);
    }

    Finder<CharString> directFinder(source);
    Pattern<CharString,  Myers<> > pattern(query);
    while (seqan::find(directFinder, pattern, minScore)) {
        int score = getScore(pattern);
        while (seqan::findBegin(directFinder, pattern, score)) {
            // Question: how do I get the score of the alignment?
            SearchResult r(beginPosition(directFinder),  !revCompl, score);
            results.push_back(r);
        }
    }

    return results;
}



string StringUtils::removeReadMateIdentifer(const CharString &oldName)
{
    // TODO: find a better way to remove mate identifier
    string readName(toCString(oldName));
    removeReadMateIdentiferInPlace(readName);

    return readName;
}

void StringUtils::removeReadMateIdentiferInPlace(string &readName)
{

    int len = readName.size();
    if (len > 2) {
        char c1 = readName.at(len - 2);
        char c2 = readName.at(len - 1);
        if ( (c1 == ':' || c1 == '/') && (c2 == '1' || c2 == '2')) {
            readName.resize(len-2);
        }
    }

}

void StringUtils::addReadMateIdentifier(CharString& readName, char id)
{
    int len = length(readName);
    if (len > 2) {
        char c2 = readName[len - 2];
        char c1 = readName[len - 1];
        if (c1 == id && (c2 == ':' || c2 == '/')) {
           return;
        }
    }

    append(readName, '/');
    append(readName, id);

}

void StringUtils::convertStringSetToVector(const StringSet<CharString> &stringSet, vector<string> &array)
{
    array.clear();
    array.reserve(length(stringSet));
    for (size_t i = 0, count = length(stringSet); i < count; ++i) {
        array.push_back(toCString(stringSet[i]));
    }
}

