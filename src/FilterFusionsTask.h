#ifndef FILTER_FUSIONS_TASK_H
#define FILTER_FUSIONS_TASK_H

#include <seqan/sequence.h>

#include "AnnotationModel.h"
#include "GenomicOverlapDetector.h"
#include "Fusion.h"
#include "FusionFilter.h"

using seqan::CharString;

struct FilterFusionsTaskSettings {

    MinSupportFilterConfig minSupportConfig;
    int minUniqueSplitReads, minBckgReads;
    float minProperlyAlignedPairsRate, minUniqueAlignmentRate, minHomogeneityWeight,
        requiredHomogeneityWeight, splitPosDev; //minBckgCoverageProp;
    string outputPath, paralogsPath, excludedBioTypes;
    bool printFiltered, allowNonCoding, allowIntergenic, allowIntronic, isPaired;

    FilterFusionsTaskSettings() {
        printFiltered = false;
        allowNonCoding = false;
        allowIntergenic = false;
        allowIntronic = true;
        isPaired = true;
        minProperlyAlignedPairsRate = 0.5;
        minUniqueAlignmentRate = 0.5;
        minUniqueSplitReads = 0;
        minHomogeneityWeight = 0.1;
        requiredHomogeneityWeight = 0.4;
        splitPosDev = 0.3;
        minBckgReads = 1;
    }
};


class FilterFusionsTask
{

    const string inputPath;
    const FilterFusionsTaskSettings settings;

    vector<Fusion> fusions;
    map<int,FusionJunctionType> fusionJunctionMap;
    vector<FusionFilterPtr> fusionFilters;
    AnnotationModel geneModels;
    GenomicOverlapDetector<ExonPtr> geneOverlapper;

    bool init();
    void constructFilters();
    void overlapFusionIntervalWithGenes(Fusion& f, int intervalIndex);
public:
    FilterFusionsTask(const string& pathToFusions, FilterFusionsTaskSettings& config)
        : inputPath(pathToFusions), settings(config) {}
    int run();
};

struct FusionCompare {
    bool operator()(const Fusion& l, const Fusion& r) {
        return l.getScore() > r.getScore();
    }
};


#endif // FILTER_FUSIONS_TASK_H
