#include "QueryAlignment.h"


std::ostream& operator<<(std::ostream& os, const QueryAlignment& aln)
{

   os << "{ref=" << aln.refId << ",start=" << aln.refStart << ",len=" << aln.length;
   os << ",strand=" << aln.strandAsChar() << "}";

   return os;
}


